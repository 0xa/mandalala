//BZR stuff
//http://www.13thparallel.org/archive/bezier-curves/
function B1(t) { return t*t*t }
function B2(t) { return 3*t*t*(1-t) }
function B3(t) { return 3*t*(1-t)*(1-t) }
function B4(t) { return (1-t)*(1-t)*(1-t) }

var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var SVG_DEBUG = 0;
var WIRE_FRAME = 0;

if (typeof mandalala_width !== 'undefined') {
    // the variable is defined
    canvas.width = mandalala_width;
    canvas.height = mandalala_width;
} else {
    canvas.width = 640;
    canvas.height = 640;

}

var RADIUS = (canvas.width/2)*0.97;

//console.log(mandalala_width);

if(SVG_DEBUG) {
    $("#mandalala_container").css("width", (canvas.width+10)*2);
    $("#mandalala_container").css("height", canvas.height+10);
} else {
    $("#mandalala_container").css("width", canvas.width+10);
    $("#mandalala_container").css("height", canvas.height+10);
}

$("#text_container").css("width", canvas.width);
$("#text_container").css("height", canvas.height);
$('#texted').css("width", canvas.width);
$('#texted').css("height", canvas.height);

$('.controler').css("width", canvas.width);
$('.controler').css("top", canvas.height-200);
$('.controler2').css("width", canvas.width);

//console.log(canvas.width);
//$('#texted').css("height", canvas.height);


var export_format = "PNG";
var seg_w = canvas.width/2;
var seg_h = canvas.width/2;

var CUR_LAYER = 0;
var snap_inc = 0;
//svg
var SVG_HEAD = "<svg xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' width='"+canvas.width+"' height='"+canvas.height+"' style='stroke:#000000;stroke-width:1;fill:none;'>";
SVG_HEAD +=  "<rect x='0' y='0' width='"+canvas.width+"' height='"+canvas.height+"' style='fill:none;stroke:#000000;stroke-width:1;'/>";

var SVG_TAIL = "</svg>";

function get_bzr_coords(cw,ch,swap, brush, reflect) {
    var x_off = seg_w*bzr.ox;
    var y_off = seg_h*bzr.oy;
    var start_x = (cw*bzr.sx)+x_off;
    var SY = bzr.sy*reflect;
//    var start_y = (ch*bzr.sy)+y_off;
    var start_y = (ch*SY)+y_off;

    if(!brush) {
	if(!swap) {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-bzr.c1x):Math.abs(swap-bzr.c1x)*-1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-bzr.c2x):Math.abs(swap-bzr.c2x)*-1;
	} else {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-bzr.c1x):swap+Math.abs(bzr.c1x)*1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-bzr.c2x):swap+Math.abs(bzr.c2x)*1;
	}
	var c1_x = (cw*swap_c1x)+x_off;
	var c1_y = (ch*(SY+(bzr.c1y*reflect)))+y_off;
	var c2_x = (cw*swap_c2x)+x_off;
	var c2_y = (ch*(SY+(bzr.c2y*reflect)))+y_off;
    } else {
	if(!swap) {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-(bzr.c1x*bzr.brush.c1x)):Math.abs(swap-(bzr.c1x*bzr.brush.c1x))*-1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-(bzr.c2x*bzr.brush.c2x)):Math.abs(swap-(bzr.c2x*bzr.brush.c2x))*-1;
	} else {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-(bzr.c1x*bzr.brush.c1x)):swap+Math.abs(bzr.c1x*bzr.brush.c1x)*1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-(bzr.c2x*bzr.brush.c2x)):swap+Math.abs(bzr.c2x*bzr.brush.c2x)*1;
	}
	var c1_x = (cw*swap_c1x)+x_off;
	var c1_y = (ch*(SY+((bzr.c1y*bzr.brush.c1y)*reflect)))+y_off;
	var c2_x = (cw*swap_c2x)+x_off;
	var c2_y = (ch*(SY+((bzr.c2y*bzr.brush.c2y)*reflect)))+y_off;

    }

    
    var end_x = (cw*bzr.ex)+x_off;
    var end_y = ((ch*bzr.ey)+y_off);
    return [start_x, start_y, c1_x, c1_y, c2_x, c2_y, end_x, end_y];
}

var bzr = {
    line_width: 1,
    sx: 0.2,
    sy: 0.125,
    c1x: 0.3,
    c1y: 0.1,
    c2x: 0.4,
    c2y: 0.2,
    ex: 0.5,
    ey: 0,
    ox: 0,
    oy: 0,
    segs: 8,
    flip: 1,
    scale: 1,
    brush: {
	on:1,
	c1y:0.46,
	c1x: 0.77,
	c2x:0.91,
	c2y: 1.7,
    }
}


var BZR_RES = 25;

function man_calc(canvas_w, canvas_h, count) {
    var man3D = [];
    bzr_res = BZR_RES;

    var bzr_rot = 0;
    
    for(i=0; i<bzr.segs; i++) {

	man3D[i] = [];
	
	if(count%2) {
	    bzr_rot = (((i/bzr.segs)*360)-90)*Math.PI/180;
	} else {
//	    bzr_rot = (((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180;
	    bzr_rot = (((i/bzr.segs)*360)-90)*Math.PI/180;
	}

	for(n=0;n<2;n++) {
	    man3D[i][n]=[];
	    
	    if(i%bzr.flip) {
		var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
	    } else {
		var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
	    }

	    if(i%bzr.flip) {
		var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,1,1):get_bzr_coords(canvas_w, canvas_h, 1,1,-1)
		b_coords = [b_coords[0],b_coords[1],b_coords[4],b_coords[5],b_coords[2],b_coords[3],b_coords[6],b_coords[7]];
	    } else {
		var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,1,1):get_bzr_coords(canvas_w, canvas_h, 0,1,-1)
	    }

	    var bzr_prev = [];
	    if(bzr.brush.on) {

		for(var inc=0; inc<bzr_res; inc++) {

		    var t = inc/(bzr_res-1);
		    bzr_x = coords[0] * B1(t) + coords[2] * B2(t) + coords[4] * B3(t) + coords[6] * B4(t);
		    bzr_y = coords[1] * B1(t) + coords[3] * B2(t) + coords[5] * B3(t) + coords[7] * B4(t);

		    b_bzr_x = b_coords[0] * B1(t) + b_coords[2] * B2(t) + b_coords[4] * B3(t) + b_coords[6] * B4(t);
		    b_bzr_y = b_coords[1] * B1(t) + b_coords[3] * B2(t) + b_coords[5] * B3(t) + b_coords[7] * B4(t);

		    var rot_2d = rotate_2d((bzr_x)*bzr.scale, (bzr_y)*bzr.scale, (bzr_rot+bzr.XY_rot2D));
		    man3D[i][n][inc] = rot_2d;

		}
		
	    }

	    
	}
    }

    mandala_3D(canvas_w, canvas_h, count, man3D);

}

function to_sphere(xy) {
    var t = 0;
//    console.log(xy);
    var sphere = xy2sphere(xy[0], xy[1], t);
    var rot = rotateY(sphere[0],sphere[1],sphere[2],0.5+bzr.rotY);
    var rot2 = rotateX(rot[0],rot[1],rot[2],bzr.rotX);

    var result = [rot2[0]*RADIUS,rot2[1]*RADIUS,rot2[2]*RADIUS];
    
    var pz = (rot2[2]-4)*-1;
    var d = 1;
    var perspective = [((d*rot2[0])/pz)*canvas.width*1.825,
      		       ((((d*(rot2[1]))/pz))+0.07) *canvas.width*1.825, 
      		       -1];

    
//    return result;
    return perspective;
}

function perpen(pts, d, off) {
    //		vector AB = (Bx-Ax, By-Ay) = (BAx, BAy)

    var vec = [];
    vec[0] = pts[1][0]-pts[0][0];
    vec[1] = pts[1][1]-pts[0][1];
    //		var len = Math.sqrt( Math.pow(vec[0],2) + Math.pow(vec[1]) );
    var len = Math.sqrt(Math.pow(vec[0],2)+Math.pow(vec[1],2))
    //         		(BAx, BAy)
    // unit vector AB = ------------------,  where length = sqrt(BAx^2 + BAy^2)
    //                        length
    var unit = [];
    unit[0] = vec[0]/len;
    unit[1] = vec[1]/len;


    //                                   (-BAy, BAx)
    // unit vector perpendicular to AB = -------------
    //                                     length

    var per = [];
    per[0] = (-1*vec[1])/len;
    per[1] = (vec[0])/len;

    //		coordinate at t1 = (Bx, By) + t1 * (unit vector perpendicular to AB)
    
    var per_t1 = [];
    if(d == 1) {
	per_t1[0] = pts[1][0]-off*per[0];
	per_t1[1] = pts[1][1]-off*per[1];
    } else {
	per_t1[0] = pts[1][0]+off*per[0];
	per_t1[1] = pts[1][1]+off*per[1];

    }
    return per_t1;
}

function dist(p1, p2) {
    var dist = Math.sqrt(Math.pow(p2[0]-p1[0],2)+Math.pow(p2[1]-p1[1],2));
    return dist;
}

var zseg = [];
function mandala_3D(canvas_w, canvas_h, count, man3D) {
    if(bzr.scale == 0) return;
    var seg = [];
    for(var i = 0; i<man3D.length;i++) {
	seg = [];
	var z_xyz = to_sphere(man3D[i][0][0]);
	for(var b = 0; b<man3D[i][0].length;b++) {
	    var p1 = man3D[i][0][b];
	    var p2 = man3D[i][1][b];
	    
	    var p1_n = man3D[i][0][Math.min(b+1,man3D[i][0].length-1)];
	    var p2_n = man3D[i][1][Math.min(b+1,man3D[i][0].length-1)];
	    
	    var pm_x = (p1[0]+p2[0])/2;
	    var pm_y = (p1[1]+p2[1])/2;

	    var pm_x_n = (p1_n[0]+p2_n[0])/2;
	    var pm_y_n = (p1_n[1]+p2_n[1])/2;

	    var tri1 = [
		[p1,p1_n,[pm_x,pm_y]],
		[p2,p2_n,[pm_x,pm_y]],
		[[pm_x,pm_y], [pm_x_n,pm_y_n],p1_n],
		[[pm_x,pm_y],[pm_x_n,pm_y_n],p2_n]
	    ];
	    
	    for(var t1=0;t1<4;t1++) {
	    	var t_tmp = tri1[t1];

	    	var po1 = to_sphere([t_tmp[0][0],t_tmp[0][1]]);
	    	var po2 = to_sphere([t_tmp[1][0],t_tmp[1][1]]);
	    	var po3 = to_sphere([t_tmp[2][0],t_tmp[2][1]]);

		var pt1 = [];
		var pt2 = [];
		
		var center_x = (t_tmp[0][0]+t_tmp[1][0]+t_tmp[2][0])/3;
		var center_y = (t_tmp[0][1]+t_tmp[1][1]+t_tmp[2][1])/3;
		var center_z = (t_tmp[0][2]+t_tmp[1][2]+t_tmp[2][2])/3;

		var c_xyz = to_sphere([center_x, center_y]);
		var line_width = 10*(Math.pow(((c_xyz[2]+RADIUS)/(RADIUS*2)),1)+0.05);

		var ms_x = 1.09
		if(t_tmp[0][0] == 0) ms_x = 1;

		var po1_2 = to_sphere([t_tmp[0][0]*ms_x,t_tmp[0][1]*ms_x]);
	    	var po2_2 = to_sphere([t_tmp[1][0]*ms_x,t_tmp[1][1]*ms_x]);
	    	var po3_2 = to_sphere([t_tmp[2][0]*ms_x,t_tmp[2][1]*ms_x]);
		seg.push([c_xyz[2],"TRI",po1,po2,po3,t1,po1_2,po2_2,po3_2,pt1,pt2,b]);

	    }

	}
	zseg.push([z_xyz[2],seg]);
    }

}

function draw_zseg(canvas_w, canvas_h, zseg) {
    context.save();
    context.translate(canvas_w/1,canvas_w/1);

    for(var zs=0;zs<zseg.length;zs++) {
	zdep = zseg[zs][1];
	for(var z=0;z<zdep.length;z++) {
	    var z_pos = zdep[z][0];
	    var what = zdep[z][1];

	    var step = zdep[z][11];

	    var po1 = zdep[z][2];
	    var po2 = zdep[z][3];
	    var po3 = zdep[z][4];

	    var po11 = zdep[z][6];
	    var po21 = zdep[z][7];
	    var po31 = zdep[z][8];

	    var p1 = po1;
	    var p2 = po2;
	    var p3 = po3;
	    
	    context.beginPath();

	    context.moveTo(p1[0], p1[1]);
	    context.lineTo(p2[0], p2[1]);
	    context.lineTo(p3[0], p3[1]);
	    context.lineTo(p1[0], p1[1]);

	    if(WIRE_FRAME) {
		var fill_col = "rgba(255, 255, 255, 1)";
		var stroke_col = "rgba(255, 0, 0, 1)";
	    } else {
		var grey = "255";
		var fill_col = "rgba("+grey+", "+grey+", "+grey+", 1)";
		var stroke_col = "rgba("+grey+", "+grey+", "+grey+", 1)";
	    }
	    
	    context.fillStyle = fill_col;
	    context.fill();
	    context.lineWidth =1;
	    context.strokeStyle = stroke_col;
	    context.closePath();
	    context.stroke();

	    
	    if(zdep[z][5] <2) {
		var po1_1 = zdep[z][2];
		var po2_1 = zdep[z][3];
		var po3_1 = zdep[z][4];

		var po1 = zdep[z][6];
		var po2 = zdep[z][7];
		var po3 = zdep[z][8];

		var po4 = zdep[z][9];
		var po5 = zdep[z][10];

		var line_width = 5*(Math.pow(((zdep[z][0]+RADIUS)/(RADIUS*2)),2)+0.05);

		context.beginPath();
		
		context.moveTo(po1_1[0], po1_1[1]);
		context.lineTo(po1[0], po1[1]);
		context.lineTo(po2[0], po2[1]);
		context.lineTo(po2_1[0], po2_1[1]);
		context.lineTo(po1_1[0], po1_1[1]);

		if(WIRE_FRAME) {
		    var stroke_fill = "rgba(0, 0, 0, 0)";
		    var stroke_style = "rgba(0, 0, 0, 1)";
		} else {
		    var stroke_fill = "rgba(0, 0, 0, 1)";
		    var stroke_style = "rgba(0, 0, 0, 1)";

		}
		
		context.fillStyle = stroke_fill;
		context.fill();
		context.lineWidth = 1;
		context.strokeStyle = stroke_style;
		context.closePath();
		context.stroke();

		
	    }
	}


    }    
    context.restore();    
}


function sortFunction(a, b) {
    if (a[0] === b[0]) {return 0;} else {return (a[0] < b[0]) ? -1 : 1;}
}

function load_bzr_params(idx) {

    bzr.line_width = bzrs.line_width[idx];
//    console.log(bzr.line_width);
    bzr.sx = bzrs.sx[idx];
    bzr.sy = bzrs.sy[idx];

    bzr.c1x = bzrs.c1x[idx];
    bzr.c1y = bzrs.c1y[idx];

    bzr.c2x = bzrs.c2x[idx];
    bzr.c2y = bzrs.c2y[idx];

    bzr.ex = bzrs.ex[idx];
    bzr.ey = bzrs.ey[idx];

    bzr.ox = bzrs.ox[idx];
    bzr.oy = bzrs.oy[idx];

    bzr.segs = bzrs.segs[idx];
    bzr.flip = bzrs.flip[idx];
    bzr.scale = bzrs.scale[idx];

    bzr.brush.on = bzrs.brush.on[idx];
    bzr.brush.c1y = bzrs.brush.c1y[idx];
    bzr.brush.c1x = bzrs.brush.c1x[idx];

    //weird hack to get svg render correctly!?
    if(bzrs.brush.c1x[idx] == 1) {bzr.brush.c1x = 0.98;}
    
    bzr.brush.c2y = bzrs.brush.c2y[idx];
    bzr.brush.c2x = bzrs.brush.c2x[idx];

    if(bzrs.brush.c2x[idx] == 1) {bzr.brush.c2x = 0.98;}

    bzr.rotX = bzrs.rotX[idx];
    bzr.rotY = bzrs.rotY[idx];
    bzr.XY_rot2D = bzrs.XY_rot2D[idx];
}


// Given a "mapping sphere" of radius R,
// the Mercator projection (x,y) of a given latitude and longitude is:
// x = R * longitude
// y = R * log( tan( (latitude + pi/2)/2 ) )

// and the inverse mapping of a given map location (x,y) is:
// longitude = x / R
// latitude = 2 * atan(exp(y/R)) - pi/2
// To get the 3D coordinates from the result of the inverse mapping:

// Given longitude and latitude on a sphere of radius S,
// the 3D coordinates P = (P.x, P.y, P.z) are:
// P.x = S * cos(latitude) * cos(longitude)
// P.y = S * cos(latitude) * sin(longitude)
// P.z = S * sin(latitude)


function xy2sphere(x,y,t) {
    var R = RADIUS;
//    R =1;
    var longitude = x / R;
    var latitude = 2 * Math.atan(Math.exp(y/R)) - Math.PI/2;

    var inc = (latitude+Math.PI)/(Math.PI*2);

    var R2 = RADIUS;
    R2 =1;
    var sx = R2 * Math.cos(latitude) * Math.cos(longitude);
    var sy = R2 * Math.cos(latitude) * Math.sin(longitude);
    var sz = R2 * Math.sin(latitude);
//    sz -= sz*(t*0.5);

    return [sx, sy, sz];
    
}

function rotateY(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+0+z*Math.sin(rot);
    var roty = y;
    var rotz = -1*x*Math.sin(rot)+0+z*Math.cos(rot);
    return [rotx, roty, rotz];
}

function rotateX(x,y,z,deg) {
    var rot = 3.14*deg;

    var rotx = x;
    var roty = 0+y*Math.cos(rot)-z*Math.sin(rot);
    var rotz = 0+y*Math.sin(rot)+z*Math.sin(rot);

    return [rotx, roty, rotz];
}


function rotate_2d(x,y,rot) {
    //rotate X
    var rotx = x*Math.cos(rot)-y*Math.sin(rot);
    var roty = x*Math.sin(rot)+y*Math.cos(rot);
    return [rotx, roty];
}

function draw_sphere_dots() {
    context.save();
    

    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    var radius = 1;
    var azimuth_num = 50;
    for(var ii=0;ii<azimuth_num;ii++) {

	var ii_inc = ii/(azimuth_num-1);
	var azimuth = (ii_inc*3.14*2*1)+0;
	var inclination_num = 50;
	for(var i = 0;i<inclination_num;i++) {
	    var inc = i/(inclination_num-1);
	    var inclination = (inc*(3.14*1))+0;
	    var r = RADIUS;
	    
	    var x = r*Math.sin(inclination)*Math.cos(azimuth);
	    var y = r*Math.sin(inclination)*Math.sin(azimuth);
	    var z = r*Math.cos(inclination);
	    
	    var rotz = rotateX(x,y,z,(bzr.rotX+0));

	    var rotx = rotz[0];
	    var roty = rotz[1];
	    var rotz = rotz[2];

	    
	    context.beginPath();
	    context.arc(canvas_w+rotx, canvas_w+roty, radius, 0, 2 * Math.PI, false);
	    context.fillStyle = '#ff0000';
	    context.fill();
	    context.lineWidth = 0;
	    context.strokeStyle = 'none';
	    context.stroke();
	}

    }

    context.restore();

}

function draw(){
    SVG = "";
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    //context.globalAlpha=0.1;
    context.rect(0, 0, canvas.width, canvas.height);
    context.fillStyle = "rgba(255, 255, 255, 255)";
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.stroke();

    
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;

    
//    draw_sphere_dots();

    var stacks = bzrs.order;
    for(c=0; c<6;c++) {
	var stack = stacks.indexOf(c);
	zseg = [];
    	load_bzr_params(stack);
	man_calc(canvas_w, canvas_h, stack);
	
	zseg.sort(sortFunction);
	//	console.log(zseg[0].le);
	var z_min = 0;
	var z_max = 0;
	if(zseg[0] !== undefined) {var z_min = zseg[0][0];}
	if(zseg[zseg.length-1] !== undefined) {var z_max = zseg[zseg.length-1][0];}

	draw_zseg(canvas_w, canvas_h,zseg);
	
    }
    
}

//UI
$(function() {
    $( "#radio" ).buttonset();
    
    $( "#eq2 > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "brush-c1y" || this.id == "brush-c2y") {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: 0,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	    
	}});
    
    $( "#eq > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "c1y" || this.id == "c2y") {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else if(this.id == "segs") {
	    $(this).empty().slider({
		value: bzr.segs,
		range: "min",
		animate: false,
		min: 1,
		max: 20,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr
		
	    });
	} else if(this.id == "flip") {
	    $(this).empty().slider({
		value: bzr.flip,
		range: "min",
		animate: false,
		min: 1,
		max: 4,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr,

	    });
	} else if(this.id == "scale") {
	    $(this).empty().slider({
		value: bzr.scale,
		range: "min",
		animate: true,
		min: 0,
		max: 5,
		step:0.1,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw,
		change: function (event) {if (event.originalEvent) {svg_draw();}}

	    });
	} else {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -1,
		max: 1,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	}
    });
    $( "#format" ).button();
    $( "#more" ).button();
    $( "#burst" ).button();
    $( "#download" ).button();
})


function downloadInnerHtml(link,filename, elId, mimeType) {
    mimeType = mimeType || 'text/plain';
    link.setAttribute('download', filename);
    link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(SVG_HEAD+SVG+SVG_TAIL));
}

function save() {$('#download')[0].click();}

function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function load_bzr() {
    for(var k in bzr) {
	if(k !== "brush") {
	    var val = bzrs[k][CUR_LAYER];
	    $('#'+k).slider('value',val);
	    $('#'+k+'_val').text(val);
//	    console.log(bzr.flip);
	} else {
	    for(var n in bzr.brush) {
		if(n == 'on') continue;
		var val = bzrs.brush[n][CUR_LAYER];
		$('#'+k+"-"+n).slider('value',val);
		$('#'+k+"-"+n+'_val').text(val);
	    }

	}
    }
    
}



function snap() {
    var inc = 1/5;
    for(var i =0; i<5; i++) {
	snap_inc = (i+1);
	draw();
	svg_draw();
	bzr.ex = bzr.ex-inc;
	bzr.sy = bzr.sy+inc;
	bzr.ey = bzr.ey+inc;
	$('#download')[0].click();
    }
    snap_inc = 0;
}

function get_rand_val(scale, off) {
    var val = ((parseInt(Math.random()*1000)/1000)*scale)+off;
    return parseInt((val*100))/100;
}

function random_gen() {
    bzr.sx = get_rand_val(2, -1);
    bzr.ex = get_rand_val(2, -1);
    bzr.c1x = get_rand_val(2, -1);
    bzr.c1y = get_rand_val(4, -2);
    bzr.c2x = get_rand_val(2, -1);
    bzr.c2y = get_rand_val(4, -2);
    bzr.segs = parseInt(get_rand_val(8, 0)+2)*2;
    bzr.flip = parseInt(get_rand_val(2, 0)+1);

    if(bzr.brush.on) {
	bzr.brush.c1x = get_rand_val(1, 0.5);
	bzr.brush.c1y = get_rand_val(1, 0.5);
	bzr.brush.c2x = get_rand_val(1, 0.5);
	bzr.brush.c2y = get_rand_val(1, 0.5);
    }
    load_bzr();
}


var isRunning = false;
var RUN = "";

function execute(x) {
    setTimeout("eval(" + x + ")", 0);
    // Or: $.globalEval(x);
}

var text_toggle = 0;

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
	if(elem.createTextRange) {
	    var range = elem.createTextRange();
	    range.move('character', caretPos);
	    range.select();
	}
	else {
	    if(elem.selectionStart) {
		elem.focus();
		elem.setSelectionRange(caretPos, caretPos);
	    }
	    else
		elem.focus();
	}
    }
}

$(document).ready(function(){

    if($('#texted').length) {
	
	document.getElementById('texted').onkeydown = function(){

//	    console.log(event.keyCode);
	    
	    if(event.ctrlKey===true && event.keyCode==88){
		var text = $("#texted").val()
		eval(text);
		draw();
		svg_draw();
	    }
	    if(event.ctrlKey===true && event.keyCode==69 && event.shiftKey){
		text_toggle = text_toggle?0:1;
		if(text_toggle) {
	    	    $("#texted").css("color","rgba(255,0,0,0)");
		    $("#texted").attr("readonly","readonly"); 	
		} else {
	    	    $("#texted").css("color","rgba(255,0,0,1)");
		    $("#texted").removeAttr("readonly"); 	
		}
		
	    }

	    if((event.ctrlKey===true && event.keyCode==38) || (event.ctrlKey===true && event.keyCode==40)) {
		var stopCharacters = [' ', '\n', '\r', '\t']
		var text = $('#texted').html();
		var start = $('#texted')[0].selectionStart;
		var end = $('#texted')[0].selectionEnd;
		while (start > 0) {
		    if (stopCharacters.indexOf(text[start]) == -1) {--start;} else {break;}
		};
		++start;
		while (end < text.length) {
		    if (stopCharacters.indexOf(text[end]) == -1) {++end;} else {break;}
		}
		var word = text.substr(start, end - start);

		var fsb = word.search(/\[/);
		var bsb = word.search(/\]/);

		if(fsb >= 0) word = word.substr(fsb+1,word.length);
		if(bsb >= 0) word = word.substr(0, bsb);
		
		var inc = 0.05;
		var next_val = word;
		if(event.keyCode==38) {next_val = parseFloat(word)+0.05;}
		if(event.keyCode==40) {next_val = parseFloat(word)-0.05;}

		if(next_val>0) {next_val = '+'+next_val.toFixed(2);} else {next_val = next_val.toFixed(2);}
		
		var first_half = text.substr(0, start);
		var last_half = text.substr(end, text.length);

		if(fsb >= 0) {
		    $('#texted').html(first_half+'['+next_val+','+last_half);
		} else if(bsb >= 0) {
		    $('#texted').html(first_half+next_val+'],'+last_half);
		} else {
		    $('#texted').html(first_half+next_val+','+last_half);
		}
		
		setCaretPosition("texted", start);

		var new_text = $("#texted").val()
		eval(new_text);
		draw();
		svg_draw();
		
		event.preventDefault();
		event.stopPropagation();
		
	    }
	    
	}
    }
    
    $(".controler").toggle();
    $(".controler2").toggle();
    $("#myCanvas").click(function() {
	$(".controler").toggle();
	if($(".controler2").is(":visible")) {
	    $(".controler2").toggle();
	}else if(bzr.brush.on) {
	    $(".controler2").toggle();
	}
	draw();
	svg_draw();
    });


    $(".l_sel").click(function() {
	var tmp = this.id.split("_");
	var layer = tmp[1];
	//	console.log(layer);
	CUR_LAYER = layer;
	load_bzr();
    });

    
    // $("#format").click(function() {
    // 	var text = $("#format_lab").text();
    // 	if(text == 'PNG') {$("#format_lab").text("SVG");export_format = "SVG";} else {$("#format_lab").text("PNG");export_format = "PNG";}
    // });


    $("#burst").click(function() {
	if(!isRunning) {
	    RUN = setInterval(function () {isRunning = true; random_gen()}, 2000);
	    $("#burst").text("---");
	} else {
	    clearInterval(RUN); isRunning = false;
	    $("#burst").text(">>>");
	}
    });

    $("#more").click(function() {
	$(".controler2").toggle();
	bzr.brush.on = (bzr.brush.on+1)%2;
	draw();
	svg_draw();

    });

    if(!bzr.brush.on) $(".controler2").toggle();
    

    document.getElementById('download').addEventListener('click', function() {

    	var fname = "bzr_";
    	var ftmp = [];
    	for(var k in bzr) {
    	    if(k !== "brush") {
    		var val = bzr[k];
    		if(k == 'line_width') continue;
    		if(k == 'segs' || k == 'flip' || k == 'scale') {
    		    val = val;
    		} else {
    		    val = parseInt(val*100);
    		}
    		ftmp.push(val);
    	    } else {
    		for(var n in bzr.brush) {
    		    if(n == 'on') continue;
    		    var val = parseInt(bzr.brush[n]*100);
    		    ftmp.push(val);
    		}
    	    }
    	}

//    	fname = fname+ftmp.join('_');
	fname = "mandalala_plus";
	
    	if(snap_inc) fname = snap_inc+'_'+fname;
	    
    	if(export_format == 'SVG') {
    	    downloadInnerHtml(this, fname+'.svg','svg');
    	} else {
//	    console.log(this);
    	    downloadCanvas(this, 'myCanvas', fname+'.png');
    	}

    }, false);


    
    load_bzr();
    
    if(SVG_DEBUG) {
	$( "body" ).append("<div id=\"svg\"></div>");
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
	$(".container").css("float","left");
    } else {
	$(".container").css("float","none");
    }

    $(".mandalala_container").toggle();
    
});

function refreshbzr(event,ui) {
    if (event.originalEvent) {
	var id = this.id;
	var id_split = id.split("-");
	var val = $(this).slider("value");
	if(id_split[0] !== "brush") {
	    // if(id == "sy") {
	    // 	bzrs["ey"][CUR_LAYER] = val;
	    // 	$("#ey_val").text(val);
	    // }
	    bzrs[id][CUR_LAYER] = val;
	} else {
//	    console.log([id_split[0],id_split[1]]);
	    bzrs[id_split[0]][id_split[1]][CUR_LAYER] = val;
	}
	$("#"+id+"_val").text(val);
    }
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}

function refreshbzr_brush(event,ui) {
    $( "#eq2 > span" ).each(function() {
	if(this.id == 'brush-c1x') {bzr.brush.c1x = $(this).slider("value");$("#brush-c1x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c1y') {bzr.brush.c1y = $(this).slider("value");$("#brush-c1y_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2x') {bzr.brush.c2x = $(this).slider("value");$("#brush-c2x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2y') {bzr.brush.c2y = $(this).slider("value");$("#brush-c2y_val").text($(this).slider("value"));}

    });
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}


function svg_draw() {

    if(SVG_DEBUG && $('#svg').length) {
	document.getElementById('svg').innerHTML = "";
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg'));

    }
}

  
draw();
