#!/bin/bash

MANDALALAS=(mandalala mandalala_plus mandalala_hatch)

for i in "${MANDALALAS[@]}"
do
    :
    cd $i && minify mandalala.js && cd ../
done
