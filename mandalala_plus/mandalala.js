var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var SVG_DEBUG = 0;
if(SVG_DEBUG) {$("#mandalala_container").css("width", 1300);}
canvas.width = 640;
canvas.height = 640;
var export_format = "PNG";
var seg_w = canvas.width/2;
var seg_h = canvas.width/2;
var CUR_LAYER = 0;
var snap_inc = 0;
//svg
var SVG_HEAD = "<svg xmlns='http://www.w3.org/2000/svg' width='640' height='640' style='stroke:#000000;stroke-width:1;fill:none;'>";
SVG_HEAD +=  "<rect x='0' y='0' width='640' height='640' style='fill:none;stroke:#000000;stroke-width:1;'/>";
var SVG_TAIL = "</svg>";

function get_bzr_coords(cw,ch,swap, brush, reflect) {
    var x_off = seg_w*bzr.ox;
    var y_off = seg_h*bzr.oy;
    var start_x = (cw*bzr.sx)+x_off;
    var SY = bzr.sy*reflect;
//    var start_y = (ch*bzr.sy)+y_off;
    var start_y = (ch*SY)+y_off;

    if(!brush) {
	if(!swap) {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-bzr.c1x):Math.abs(swap-bzr.c1x)*-1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-bzr.c2x):Math.abs(swap-bzr.c2x)*-1;
	} else {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-bzr.c1x):swap+Math.abs(bzr.c1x)*1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-bzr.c2x):swap+Math.abs(bzr.c2x)*1;
	}
	var c1_x = (cw*swap_c1x)+x_off;
	var c1_y = (ch*(SY+(bzr.c1y*reflect)))+y_off;
	var c2_x = (cw*swap_c2x)+x_off;
	var c2_y = (ch*(SY+(bzr.c2y*reflect)))+y_off;
	    } else {
	if(!swap) {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-(bzr.c1x*bzr.brush.c1x)):Math.abs(swap-(bzr.c1x*bzr.brush.c1x))*-1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-(bzr.c2x*bzr.brush.c2x)):Math.abs(swap-(bzr.c2x*bzr.brush.c2x))*-1;
	} else {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-(bzr.c1x*bzr.brush.c1x)):swap+Math.abs(bzr.c1x*bzr.brush.c1x)*1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-(bzr.c2x*bzr.brush.c2x)):swap+Math.abs(bzr.c2x*bzr.brush.c2x)*1;
	}
	var c1_x = (cw*swap_c1x)+x_off;
	var c1_y = (ch*(SY+((bzr.c1y*bzr.brush.c1y)*reflect)))+y_off;
	var c2_x = (cw*swap_c2x)+x_off;
	var c2_y = (ch*(SY+((bzr.c2y*bzr.brush.c2y)*reflect)))+y_off;

    }

    
    var end_x = (cw*bzr.ex)+x_off;
    var end_y = ((ch*bzr.ey)+y_off);
    return [start_x, start_y, c1_x, c1_y, c2_x, c2_y, end_x, end_y];
}

var bzr = {
    line_width: 1,
    sx: 0.2,
    sy: 0.125,
    c1x: 0.3,
    c1y: 0.1,
    c2x: 0.4,
    c2y: 0.2,
    ex: 0.5,
    ey: 0,
    ox: 0,
    oy: 0,
    segs: 8,
    flip: 1,
    scale: 1,
    brush: {
	on:1,
	c1y:0.46,
	c1x: 0.77,
	c2x:0.91,
	c2y: 1.7,
    }
}

function draw_loop(canvas_w, canvas_h, count) {
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		sides[n] = coords;

		context.beginPath();
		
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];


		if(n) {

		    inside_path += 
		    	" c "+ (svg_sx_max-coords[4])*-1+","+(svg_sy_max-coords[5])*-1+
		    	" "+(svg_sx_max-coords[2])*-1+","+(svg_sy_max-coords[3])*-1+
		    	" "+(svg_sx_max-coords[0])*-1+","+(svg_sy_max-coords[1])*-1;
		    inside_path += " z' style='fill:#ffffff;stroke:#000000;stroke-width:0;' id='' />";		    
		} else {
		    inside_path = "<path d='m ";
		    inside_path += coords[0]+","+coords[1]+
		    	" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
		    	" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
		    	" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min);
		}


		if(bzr.brush.on) {
		    context.moveTo(coords[0], coords[1]);
		    context.bezierCurveTo((coords[2])*1, coords[3],coords[4],coords[5],  coords[6], coords[7]);
		
		    if(i%bzr.flip) {
			var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,1,1):get_bzr_coords(canvas_w, canvas_h, 1,1,-1)
			b_coords = [b_coords[0],b_coords[1],b_coords[4],b_coords[5],b_coords[2],b_coords[3],b_coords[6],b_coords[7]];
		    } else {
			var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,1,1):get_bzr_coords(canvas_w, canvas_h, 0,1,-1)
		    }
		    
		    context.moveTo(b_coords[6], b_coords[7]);
		    context.bezierCurveTo(b_coords[4], b_coords[5], b_coords[2], b_coords[3],  b_coords[0], b_coords[1]);
		    
		    var svg_path =
			"<path d='m "+coords[0]+","+coords[1]+
			" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
			" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
			" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min)+
			" c "+(svg_sx_max-b_coords[4])*-1+","+(svg_sy_max-b_coords[5])*-1+
			" "+(svg_sx_max-b_coords[2])*-1+","+((svg_sy_max-b_coords[3])*-1)+
			" "+(svg_sx_max-b_coords[0])*-1+","+(svg_sy_max-b_coords[1])*-1+
//			" z' fill='#000000' id='' />";
			" '  id='' />";

		    
		} else {
		    context.moveTo(coords[0], coords[1]);
		    context.bezierCurveTo((coords[2]), coords[3], coords[4], coords[5], coords[6], coords[7]);

		    var svg_path =
			"<path d='m "+coords[0]+","+coords[1]+
			" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
			" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
			" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min)+"' id='' />";
		}
		
		outside_path += svg_path;
		context.moveTo(coords[6], coords[7]);
		context.closePath();

//		if(bzr.brush.on) {context.fillStyle = '#000000'; context.fill();}
		
		context.lineWidth = bzr.line_width;
		context.lineCap = 'round';
		context.strokeStyle = 'black';
		context.stroke();

	    }

//	SVG +=inside_path;
	SVG += outside_path;
	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}

var contour = 8;
function draw_contour_loop(canvas_w, canvas_h, count) {
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		sides[n] = coords;

		context.beginPath();
		
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];

		if(i%bzr.flip) {
		    var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,1,1):get_bzr_coords(canvas_w, canvas_h, 1,1,-1)
		    b_coords = [b_coords[0],b_coords[1],b_coords[4],b_coords[5],b_coords[2],b_coords[3],b_coords[6],b_coords[7]];
		} else {
		    var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,1,1):get_bzr_coords(canvas_w, canvas_h, 0,1,-1)
		}

		var svg_path = "";
		for(cc=0;cc<contour;cc++ ) {
//		    console.log(cc);
		    var contour_percent = cc/(contour-1);
		    if(contour_percent == 0 || contour_percent == 1) continue;

		    context.beginPath();
		    
		    context.moveTo(coords[0], coords[1]);
		    context.bezierCurveTo(
		    	coords[2]-((coords[2]-b_coords[2])*contour_percent), 
		    	coords[3]-((coords[3]-b_coords[3])*contour_percent),
		    	coords[4]-((coords[4]-b_coords[4])*contour_percent),
		    	coords[5]-((coords[5]-b_coords[5])*contour_percent), 
		    	coords[6], coords[7]);

		    var svg_path =
		    	"<path d='m "+coords[0]+","+coords[1]+
		    	" c "+((coords[2]-svg_sx_min)-((coords[2]-b_coords[2])*contour_percent))+
		    	","+((coords[3]-svg_sy_min)-((coords[3]-b_coords[3])*contour_percent))+
		    	" "+((coords[4]-svg_sx_min)-((coords[4]-b_coords[4])*contour_percent))+
		    	","+((coords[5]-svg_sy_min)-((coords[5]-b_coords[5])*contour_percent))+
		    	" "+((coords[6]-svg_sx_min))+
		    	","+((coords[7]-svg_sy_min))+
		    	"' id=''  style='stroke:#000000;stroke-width:"+(bzr.line_width*1.5)+";fill:none;'  />";
		    
		    SVG += svg_path;
		    context.moveTo(coords[6], coords[7]);
		    context.closePath();

		    context.lineWidth = bzr.line_width*1.5;
		    context.lineCap = 'round';
		    context.strokeStyle = '#000000';
		    context.stroke();
		}
			
		outside_path += svg_path;
		context.moveTo(coords[6], coords[7]);
		context.closePath();

	    }

	SVG += outside_path;
	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}


function draw_mask_loop(canvas_w, canvas_h, count) {
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		sides[n] = coords;

		context.beginPath();
		context.lineWidth = 0;
		context.moveTo(coords[0], coords[1]);
		context.bezierCurveTo((coords[2]), coords[3], coords[4], coords[5], coords[6], coords[7]);
		context.bezierCurveTo((coords[4]), coords[5]*-1, coords[2], coords[3]*-1, coords[0], coords[1]);
		context.lineTo(coords[0],coords[7]);
		context.closePath();

		context.fillStyle = '#ffffff'; 
		context.fill();


		context.beginPath();
		
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];


		if(n) {

		    inside_path += 
		    	" c "+ (svg_sx_max-coords[4])*-1+","+(svg_sy_max-coords[5])*-1+
		    	" "+(svg_sx_max-coords[2])*-1+","+(svg_sy_max-coords[3])*-1+
		    	" "+(svg_sx_max-coords[0])*-1+","+(svg_sy_max-coords[1])*-1;
		    inside_path += " z' style='fill:#ffffff;stroke:#000000;stroke-width:0;' id='' />";		    
		} else {
		    inside_path = "<path d='m ";
		    inside_path += coords[0]+","+coords[1]+
		    	" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
		    	" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
		    	" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min);
		}


	    }

	SVG +=inside_path;
	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}


var bzrs = {
    line_width: [1,1,1,1,1],
    sx: [0.3, 0.1,0.38,0.3,0.2,0.2],
    sy: [0.1, 0.25,0.15,0,0.1,0.125],
    c1x: [0.69, 0.3,0.5,0.38,0.3,0.3],
    c1y: [-0.3, 0.17,0.25,-0.11,0.1,0.1],
    c2x: [0.9, 0.4,0.52,0.54,0.4,0.4],
    c2y: [0.5, 0.2,-0.21,0.24,0.2,0.2],
    ex: [1, 0.5,0.6,0.6,0.5,0.5],
    ey: [0, 0,0,0,0,0],
    ox: [0, 0,0,0,0,0],
    oy: [0, 0,0,0,0,0],
    segs: [16, 5,8,8,8,8],
    flip: [1, 1,1,1,1,1],
    scale: [1, 1.9,1,1,1,0.8],
    brush: {
	on:[1, 1,1,1,1,1],
	c1y:[1.2,-3,1,0.6,-2,-3],
	c1x: [1.34,1,1,0.89,1,0.77],
	c2x:[1,1,1,1,1,0.91],
	c2y: [0.7,1.2,0.2,1,1.2,1.7],
    }
}

// var bzrs = {
//     line_width: [1],
//     sx: [0.2],
//     sy: [0.125],
//     c1x: [0.3],
//     c1y: [0.1],
//     c2x: [0.4],
//     c2y: [0.2],
//     ex: [0.5],
//     ey: [0],
//     ox: [0],
//     oy: [0],
//     segs: [2],
//     flip: [1],
//     scale: [0.8],
//     brush: {
// 	on:[1],
// 	c1y:[-3],
// 	c1x: [0.77],
// 	c2x:[0.91],
// 	c2y: [1.7],
//     }
// }


function load_bzr_params(idx) {

    bzr.line_width = bzrs.line_width[idx];
    bzr.sx = bzrs.sx[idx];
    bzr.sy = bzrs.sy[idx];

    bzr.c1x = bzrs.c1x[idx];
    bzr.c1y = bzrs.c1y[idx];

    bzr.c2x = bzrs.c2x[idx];
    bzr.c2y = bzrs.c2y[idx];

    bzr.ex = bzrs.ex[idx];
    bzr.ey = bzrs.ey[idx];

    bzr.ox = bzrs.ox[idx];
    bzr.oy = bzrs.oy[idx];

    bzr.segs = bzrs.segs[idx];
    bzr.flip = bzrs.flip[idx];
    bzr.scale = bzrs.scale[idx];

    bzr.brush.on = bzrs.brush.on[idx];
    bzr.brush.c1y = bzrs.brush.c1y[idx];
    bzr.brush.c1x = bzrs.brush.c1x[idx];
    bzr.brush.c2y = bzrs.brush.c2y[idx];
    bzr.brush.c2x = bzrs.brush.c2x[idx];

}

function draw(){
    SVG = "";
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
//    context.globalAlpha=0.1;
    context.rect(0, 0, canvas.width, canvas.height);
    context.fillStyle = '#ffffff';
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.stroke();

    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;

    for(c=0; c<6;c++) {
	load_bzr_params(c);
	draw_mask_loop(canvas_w, canvas_h,c);
	draw_loop(canvas_w, canvas_h,c);
	draw_contour_loop(canvas_w, canvas_h,c);
    }

    
}

//UI
$(function() {
    $( "#radio" ).buttonset();
    
    $( "#eq2 > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "brush-c1y" || this.id == "brush-c2y") {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: svg_draw
	    });
	} else {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: 0,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: svg_draw
	    });

	}
	    
	}});
    
    $( "#eq > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "c1y" || this.id == "c2y") {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: svg_draw
	    });
	} else if(this.id == "segs") {
	    $(this).empty().slider({
		value: bzr.segs,
		range: "min",
		animate: false,
		min: 1,
		max: 20,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr
	    });
	} else if(this.id == "flip") {
	    $(this).empty().slider({
		value: bzr.flip,
		range: "min",
		animate: false,
		min: 1,
		max: 4,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr,

	    });
	} else if(this.id == "scale") {
	    $(this).empty().slider({
		value: bzr.scale,
		range: "min",
		animate: true,
		min: 0,
		max: 5,
		step:0.1,
		orientation: "vertical",
		slide: refreshbzr,
		change: svg_draw,

	    });
	} else {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -1,
		max: 1,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: svg_draw
	    });

	}
	}
    });
    $( "#format" ).button();
    $( "#more" ).button();
    $( "#burst" ).button();
    $( "#download" ).button();
})


function downloadInnerHtml(link,filename, elId, mimeType) {
    mimeType = mimeType || 'text/plain';
    link.setAttribute('download', filename);
    link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(SVG_HEAD+SVG+SVG_TAIL));
}


function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function load_bzr() {
    for(var k in bzr) {
	if(k !== "brush") {
	    var val = bzrs[k][CUR_LAYER];
	    $('#'+k).slider('value',val);
	    $('#'+k+'_val').text(val);
//	    console.log(bzr.flip);
	} else {
	    for(var n in bzr.brush) {
		if(n == 'on') continue;
		var val = bzrs.brush[n][CUR_LAYER];
		$('#'+k+"-"+n).slider('value',val);
		$('#'+k+"-"+n+'_val').text(val);
	    }

	}
    }
    
}



function snap() {
    var inc = 1/5;
    for(var i =0; i<5; i++) {
	snap_inc = (i+1);
	draw();
	svg_draw();
	bzr.ex = bzr.ex-inc;
	bzr.sy = bzr.sy+inc;
	bzr.ey = bzr.ey+inc;
	$('#download')[0].click();
    }
    snap_inc = 0;
}

function get_rand_val(scale, off) {
    var val = ((parseInt(Math.random()*1000)/1000)*scale)+off;
    return parseInt((val*100))/100;
}

function random_gen() {
    bzr.sx = get_rand_val(2, -1);
    bzr.ex = get_rand_val(2, -1);
    bzr.c1x = get_rand_val(2, -1);
    bzr.c1y = get_rand_val(4, -2);
    bzr.c2x = get_rand_val(2, -1);
    bzr.c2y = get_rand_val(4, -2);
    bzr.segs = parseInt(get_rand_val(8, 0)+2)*2;
    bzr.flip = parseInt(get_rand_val(2, 0)+1);

    if(bzr.brush.on) {
	bzr.brush.c1x = get_rand_val(1, 0.5);
	bzr.brush.c1y = get_rand_val(1, 0.5);
	bzr.brush.c2x = get_rand_val(1, 0.5);
	bzr.brush.c2y = get_rand_val(1, 0.5);
    }
    load_bzr();
}


var isRunning = false;
var RUN = "";

$(document).ready(function(){
//    $(".controler").toggle();
//    $(".controler2").toggle();
    $("#myCanvas").click(function() {
	$(".controler").toggle();
	if($(".controler2").is(":visible")) {
	    $(".controler2").toggle();
	}else if(bzr.brush.on) {
	    $(".controler2").toggle();
	}
	draw();
	svg_draw();
    });


    $(".l_sel").click(function() {
	var tmp = this.id.split("_");
	var layer = tmp[1];
	//	console.log(layer);
	CUR_LAYER = layer;
	load_bzr();
//	console.log(CUR_LAYER);
    });

    
    $("#format").click(function() {
	var text = $("#format_lab").text();
	if(text == 'PNG') {$("#format_lab").text("SVG");export_format = "SVG";} else {$("#format_lab").text("PNG");export_format = "PNG";}
    });


    $("#burst").click(function() {
	if(!isRunning) {
	    RUN = setInterval(function () {isRunning = true; random_gen()}, 2000);
	    $("#burst").text("---");
	} else {
	    clearInterval(RUN); isRunning = false;
	    $("#burst").text(">>>");
	}
    });

    $("#more").click(function() {
	$(".controler2").toggle();
	bzr.brush.on = (bzr.brush.on+1)%2;
	draw();
	svg_draw();

    });

    if(!bzr.brush.on) $(".controler2").toggle();
    

    document.getElementById('download').addEventListener('click', function() {

    	var fname = "bzr_";
    	var ftmp = [];
    	for(var k in bzr) {
    	    if(k !== "brush") {
    		var val = bzr[k];
    		if(k == 'line_width') continue;
    		if(k == 'segs' || k == 'flip' || k == 'scale') {
    		    val = val;
    		} else {
    		    val = parseInt(val*100);
    		}
    		ftmp.push(val);
    	    } else {
    		for(var n in bzr.brush) {
    		    if(n == 'on') continue;
    		    var val = parseInt(bzr.brush[n]*100);
    		    ftmp.push(val);
    		}
    	    }
    	}

//    	fname = fname+ftmp.join('_');
	fname = "mandalala_plus";
	
    	if(snap_inc) fname = snap_inc+'_'+fname;
	    
    	if(export_format == 'SVG') {
    	    downloadInnerHtml(this, fname+'.svg','svg');
    	} else {
    	    downloadCanvas(this, 'myCanvas', fname+'.png');
    	}

    }, false);


    
    load_bzr();
    
    if(SVG_DEBUG) {
	$( "body" ).append("<div id=\"svg\"></div>");
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
	$(".container").css("float","left");
    } else {
	$(".container").css("float","none");
    }


    
});



function refreshbzr(event,ui) {
    if (event.originalEvent) {
	var id = this.id;
	var id_split = id.split("-");
	var val = $(this).slider("value");
	if(id_split[0] !== "brush") {
	    // if(id == "sy") {
	    // 	bzrs["ey"][CUR_LAYER] = val;
	    // 	$("#ey_val").text(val);
	    // }
	    bzrs[id][CUR_LAYER] = val;
	} else {
//	    console.log([id_split[0],id_split[1]]);
	    bzrs[id_split[0]][id_split[1]][CUR_LAYER] = val;
	}
	$("#"+id+"_val").text(val);
    }
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}

function refreshbzr_brush(event,ui) {
    $( "#eq2 > span" ).each(function() {
	if(this.id == 'brush-c1x') {bzr.brush.c1x = $(this).slider("value");$("#brush-c1x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c1y') {bzr.brush.c1y = $(this).slider("value");$("#brush-c1y_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2x') {bzr.brush.c2x = $(this).slider("value");$("#brush-c2x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2y') {bzr.brush.c2y = $(this).slider("value");$("#brush-c2y_val").text($(this).slider("value"));}

    });
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}


function svg_draw() {
    if(SVG_DEBUG && $('#svg').length) {
	document.getElementById('svg').innerHTML = "";
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
    }
}

  
draw();


