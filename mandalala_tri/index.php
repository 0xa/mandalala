<?php
$tmp = explode("/",$_SERVER["REQUEST_URI"]);
$name = strtoupper($tmp[count($tmp)-2]);
$bzrs = "".file_get_contents("bzrs.js");
$mandalala_width = isset($_GET["WIDTH"])?$_GET["WIDTH"]:640;
$G_ROT = isset($_GET["G_ROT"])?$_GET["G_ROT"]:"0_0_0";
$G_ROT = explode("_",$G_ROT);

?>
<!DOCTYPE HTML>
<html>
  <head>

    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
    <link rel="stylesheet" href="../js/jquery-ui.css">
    <link rel="stylesheet" href="../css/mandalala.css">

    <!-- <script src="//code.jquery.com/jquery-2.1.4.min.js"></script> -->
    <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->


    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.ui.touch-punch.min.js"></script>

    <script>var mandalala_width = " <?php echo $mandalala_width ?> ";</script>
    <script>var g_rot_x = " <?php echo $G_ROT[0] ?> ";</script>
    <script>var g_rot_y = " <?php echo $G_ROT[1] ?> ";</script>
    <script>var g_rot_z = " <?php echo $G_ROT[2] ?> ";</script>
    

  </head>
  <body>

  <?php
    echo "<div class=\"\" id=\"header\">_.:".$name.":._</div>";
    ?>

    
    <div class="mandalala_container" id="mandalala_container" style="display:none">
    <canvas id="myCanvas"></canvas>

    <?php
        if(!isset($_GET["UI"]) || $_GET["UI"] === "text") {
            echo "<div class=\"text_container\">";
            echo "<textarea spellcheck=\"false\" id=\"texted\">".$bzrs."</textarea>";
            echo "</div>";
        }
    ?>
      
      <div class="controler2">
	<div id="eq2">
	  <span class="slide" id="brush-c1x">0</span>
	  <div><span class="eq_lab"  id="brush-c1x_lab">c1x</span></div>
	  <div><span class="eq_val2" id="brush-c1x_val"></span></div>

	  <span class="slide" id="brush-c1y">0</span>
	  <div><span class="eq_lab"  id="brush-c1y_lab">c1y</span></div>
	  <div><span class="eq_val2" id="brush-c1y_val"></span></div>

	  <span class="slide" id="brush-c2x">0</span>
	  <div><span class="eq_lab"  id="brush-c2x_lab">c2x</span></div>
	  <div><span class="eq_val2" id="brush-c2x_val"></span></div>

	  <span class="slide" id="brush-c2y">0</span>
	  <div><span class="eq_lab"  id="brush-c2y_lab">c2y</span></div>
	  <div><span class="eq_val2" id="brush-c2y_val"></span></div>

	  
	</div>
      </div>
      
      <div class="controler">
	<div id="eq">

	  
	  <div id="layers_sel">
	    <input class="l_sel" type="radio" id="l_0" name="radio" checked="checked">
	    <input class="l_sel" type="radio" id="l_1" name="radio">
	    <input class="l_sel" type="radio" id="l_2" name="radio">
	    <input class="l_sel" type="radio" id="l_3" name="radio">
	    <input class="l_sel" type="radio" id="l_4" name="radio">
	    <input class="l_sel" type="radio" id="l_5" name="radio">
	  </div>
	  
	  <span class="slide" id="sx">0</span>
	  <div><span class="eq_lab"  id="sx_lab">SX</span></div>
	  <div><span class="eq_val" id="sx_val"></span></div>

	  <span class="slide" id="ex">0</span>
	  <div><span class="eq_lab" id="ex_lab">EX</span></div>
	  <div><span class="eq_val" id="ex_val"></span></div>

	  
	  <span class="slide" id="sy">0</span>
	  <div><span class="eq_lab" id="sy_lab">SY</span></div>
	  <div><span class="eq_val" id="sy_val"></span></div>
	  
	  <span class="slide" id="c1x">0</span>
	  <div><span class="eq_lab" id="c1x_lab">C1X</span></div>
	  <div><span class="eq_val" id="c1x_val"></span></div>

	  
	  <span class="slide" id="c1y">0</span>
	  <div><span class="eq_lab" id="c1y_lab">C1Y</span></div>
	  <div><span class="eq_val" id="c1y_val"></span></div>
	  
	  <span class="slide" id="c2x">0</span>
	  <div><span class="eq_lab" id="c2x_lab">C2X</span></div>
	  <div><span class="eq_val" id="c2x_val"></span></div>
	  
	  <span class="slide" id="c2y">0</span>
	  <div><span class="eq_lab" id="c2y_lab">C2Y</span></div>
	  <div><span class="eq_val" id="c2y_val"></span></div>
	  
	  <span class="slide" id="ox">0</span>
	  <div><span class="eq_lab" id="ox_lab">OX</span></div>
	  <div><span class="eq_val" id="ox_val"></span></div>

	  <span class="slide" id="segs">0</span>
	  <div><span class="eq_lab" id="segs_lab">SEG</span></div>
	  <div><span class="eq_val" id="segs_val"></span></div>
	  
	  <!-- <span class="slide" id="flip">0</span> -->
	  <!-- <div><span class="eq_lab" id="flip_lab">FP</span></div> -->
	  <!-- <div><span class="eq_val" id="flip_val"></span></div> -->

	  <span class="slide" id="scale">1</span>
	  <div><span class="eq_lab" id="scale_lab">*</span></div>
	  <div><span class="eq_val" id="scale_val"></span></div>

	  
	</div>
	<input type="checkbox" id="format"><label for="format" id="format_lab" class="toggle">PNG</label>
	<!-- <input type="checkbox" id="more"><label for="more" id="more_lab" class="toggle">BRU<br>SH.</label> -->
	<div><a id="download" class="toggle">DOW<br>NLO<br>AD.</a></div>
	<!-- <button class="toggle" id="burst">>>></button> -->
      </div>
    </div>


    <?php if(!isset($_GET["G_ROT"])) echo "<script src=\"bzrs.js\"></script>"?>
    <script src="mandalala.js"></script>

    
<?php
    $foot = "";
$foot.= "<div class=\"\" id=\"footer\">";
$foot .= "<div id=\"footer_left\">";
$foot.= "CHUN|AT|KURI|DOT|MU";
$foot .= "</div>";

$foot .= "<div id=\"footer_right\">";
$foot.= "<a href=\"../index.php\"><b><<<</b></a>";
$foot .= "</div>";

$foot.= "</div>";

echo $foot;
?>

    
  </body>
</html>      
