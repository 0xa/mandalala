//BZR stuff
//http://www.13thparallel.org/archive/bezier-curves/
function B1(t) { return t*t*t }
function B2(t) { return 3*t*t*(1-t) }
function B3(t) { return 3*t*(1-t)*(1-t) }
function B4(t) { return (1-t)*(1-t)*(1-t) }

var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var SVG_DEBUG = 0;
var WIRE_FRAME = 0;
var MAP_3D = 1;
var CLIP_Z = 1;
if (typeof mandalala_width !== 'undefined') {
    // the variable is defined
    canvas.width = mandalala_width;
    canvas.height = mandalala_width;
} else {
    canvas.width = 640;
    canvas.height = 640;

}

var RADIUS = (canvas.width/2)*0.97;

//console.log(mandalala_width);

if(SVG_DEBUG) {
    $("#mandalala_container").css("width", (canvas.width+10)*2);
    $("#mandalala_container").css("height", canvas.height+10);
} else {
    $("#mandalala_container").css("width", canvas.width+10);
    $("#mandalala_container").css("height", canvas.height+10);
}

$("#text_container").css("width", canvas.width);
$("#text_container").css("height", canvas.height);
$('#texted').css("width", canvas.width);
$('#texted').css("height", canvas.height);

$('.controler').css("width", canvas.width);
$('.controler').css("top", canvas.height-200);
$('.controler2').css("width", canvas.width);

var export_format = "PNG";
var seg_w = canvas.width/2;
var seg_h = canvas.width/2;

var CUR_LAYER = 0;
var snap_inc = 0;
//svg
var SVG_HEAD = "<svg xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' width='"+canvas.width+"' height='"+canvas.height+"' style='stroke:#000000;stroke-width:1;fill:none;'>";
SVG_HEAD +=  "<rect x='0' y='0' width='"+canvas.width+"' height='"+canvas.height+"' style='fill:none;stroke:#000000;stroke-width:1;'/>";

var SVG_TAIL = "</svg>";

var bzr = {
    line_width: 1,
    sx: 0.2,
    sy: 0.125,
    c1x: 0.3,
    c1y: 0.1,
    c2x: 0.4,
    c2y: 0.2,
    ex: 0.5,
    ey: 0,
    ox: 0,
    oy: 0,
    segs: 8,
    flip: 1,
    scale: 1,
    brush: {
	on:1,
	c1y:0.46,
	c1x: 0.77,
	c2x:0.91,
	c2y: 1.7,
    }
}

// var g_rot_y = 0;
// var g_rot_x = 0.5;
// var g_rot_z = 0;

//console.log([g_rot_x,g_rot_y,g_rot_z]);

function to_sphere2(xy) {
    var t = 0;

    var sphere = xy2sphere(xy[0], xy[1], t);

    
//    console.log(sphere);
    
    var rot = rotateY(sphere[0],sphere[1],sphere[2],g_rot_y);
    var rot2 = rotateX(rot[0],rot[1],rot[2],g_rot_x);
    var rot3 = rotateZ(rot2[0],rot2[1],rot2[2],g_rot_z);
    if (CLIP_Z && rot3[2]<-0.25) return [];
//    if(rot2[2]<0) return [];
//    var result = [rot2[0]*RADIUS,rot2[1]*RADIUS,rot2[2]*RADIUS];
//    var result = [rot3[0]*RADIUS,rot3[1]*RADIUS,rot3[2]*RADIUS];
    var result = [rot3[0],rot3[1],rot3[2]];
    
    
    // var pz = (rot2[2]-4)*-1;
    // var d = 1;
    // var perspective = [((d*rot2[0])/pz)*canvas.width*1.825,
    //   		       ((((d*(rot2[1]))/pz))+0.0) *canvas.width*1.825, 
    //   		       -1];

    
    return result;
//    return sphere;
//    return perspective;
}


function perpen(pts, d, off) {
    //		vector AB = (Bx-Ax, By-Ay) = (BAx, BAy)
    var vec = [];
    vec[0] = pts[1][0]-pts[0][0];
    vec[1] = pts[1][1]-pts[0][1];
    //		var len = Math.sqrt( Math.pow(vec[0],2) + Math.pow(vec[1]) );
    var len = Math.sqrt(Math.pow(vec[0],2)+Math.pow(vec[1],2))
    //         		(BAx, BAy)
    // unit vector AB = ------------------,  where length = sqrt(BAx^2 + BAy^2)
    //                        length
    var unit = [];
    unit[0] = vec[0]/len;
    unit[1] = vec[1]/len;

    //                                   (-BAy, BAx)
    // unit vector perpendicular to AB = -------------
    //                                     length

    var per = [];
    per[0] = (-1*vec[1])/len;
    per[1] = (vec[0])/len;

    //		coordinate at t1 = (Bx, By) + t1 * (unit vector perpendicular to AB)
    
    var per_t1 = [];
    if(d == 1) {
	per_t1[0] = pts[1][0]-off*per[0];
	per_t1[1] = pts[1][1]-off*per[1];
    } else {
	per_t1[0] = pts[1][0]+off*per[0];
	per_t1[1] = pts[1][1]+off*per[1];

    }
    return per_t1;
}

function dist(p1, p2) {
    var dist = Math.sqrt(Math.pow(p2[0]-p1[0],2)+Math.pow(p2[1]-p1[1],2));
    return dist;
}

var zseg = [];

function sortFunction(a, b) {
    if (a[0] === b[0]) {return 0;} else {return (a[0] < b[0]) ? -1 : 1;}
}

function sortFunction2(a, b) {
    if (a === b) {return 0;} else {return (a < b) ? -1 : 1;}
}



// Given a "mapping sphere" of radius R,
// the Mercator projection (x,y) of a given latitude and longitude is:
// x = R * longitude
// y = R * log( tan( (latitude + pi/2)/2 ) )

// and the inverse mapping of a given map location (x,y) is:
// longitude = x / R
// latitude = 2 * atan(exp(y/R)) - pi/2
// To get the 3D coordinates from the result of the inverse mapping:

// Given longitude and latitude on a sphere of radius S,
// the 3D coordinates P = (P.x, P.y, P.z) are:
// P.x = S * cos(latitude) * cos(longitude)
// P.y = S * cos(latitude) * sin(longitude)
// P.z = S * sin(latitude)

//Latitude measurements range from 0° to (+/–)90°. Longitude measures how far east or west of the prime meridian a place is located. The prime meridian runs through Greenwich, England. Longitude measurements range from 0° to (+/–)180°.

function bzr_draw() {


    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    var radius = 5;
    
    var coords = [1,0, 0.5,1, 0.5,1, 0,0];
    var res = 10;
    for(var i = 0; i< 10; i++) {
	t = i/(10-1);
//	console.log(t);
	bzr_x = coords[0] * B1(t) + coords[2] * B2(t) + coords[4] * B3(t) + coords[6] * B4(t);
	bzr_y = coords[1] * B1(t) + coords[3] * B2(t) + coords[5] * B3(t) + coords[7] * B4(t);

	console.log([t, bzr_x, t-(bzr_x-t)]);
	
	
    	context.beginPath();

	context.arc((bzr_x*canvas_w)+canvas_w, 
		    (bzr_y*canvas_w*-1)+canvas_w, 
		    radius, 0, 2 * Math.PI, false);

	context.arc((t*canvas_w)+canvas_w, 
		    (0*canvas_w*-1)+canvas_w, 
		    radius, 0, 2 * Math.PI, false);

	
	var col = 255;
	var col2 = 0;
	context.fillStyle = "rgba("+col+", "+col2+", "+col2+", 255)";
	context.fill();
	context.lineWidth = 5;
	context.strokeStyle = "rgba("+col+", "+col2+", "+col2+", 255)";
	context.strokeStyle = 'none';
	context.stroke();

	
    }

    
    context.restore();

}

var lats = [];
function xy2sphere(x,y,t) {
    var R = RADIUS;
    var longitude = x / R;
    var latitude = 2 * Math.atan(Math.exp(y/R)) - Math.PI/2;

//    console.log([longitude, latitude]);
    //[1.472594946113794, -1.4745212132752086]
//    var inc = (latitude+Math.PI)/(Math.PI*2);
    var lphase = (latitude+(Math.PI/2))/Math.PI;
//    lphase = Math.abs(Math.abs((lphase*2)-1)-0);
//    lphase = (lphase+0.2)%1.0;
//    lphase = Math.sin(lphase*Math.PI*1)*1;

    var t = lphase;
    var coords = [1,1, 1,1, 0,0, 0,0]
    bzr_x = coords[0] * B1(t) + coords[2] * B2(t) + coords[4] * B3(t) + coords[6] * B4(t);
    bzr_y = coords[1] * B1(t) + coords[3] * B2(t) + coords[5] * B3(t) + coords[7] * B4(t);

    bzr_d = dist([bzr_x,bzr_y],[t,0]);

    bzr_v = bzr_y;

    var shape = lphase;
    var bp = 0.5;
    if(shape < bp) {
	shape = shape/bp;
	//	shape = Math.sin(shape*Math.PI*0.5);
//	shape = Math.abs(shape - 1);
//	shape = Math.pow(shape, 3);
//	shape = Math.abs(shape - 1);

//	shape = Math.sin(shape*Math.PI*0.25)*1.41;
//	shape = Math.pow(shape, 3);

	shape = Math.sin(shape*Math.PI*0.5);
	shape = Math.pow(shape, 5);

	

    } else {
	shape = (1-shape)/(1-bp);
	shape = Math.sin(shape*Math.PI*0.5);
	shape = Math.pow(shape, 5);

    }

    var R2 =1;

    var sx = R2 *shape* Math.cos(longitude);
    var sy = R2 *shape* Math.sin(longitude);
    var sz = (((latitude+(Math.PI/2))/Math.PI)*2)-1;


    // var sx = R2 * Math.cos(latitude) * Math.cos(longitude);
    // var sy = R2 * Math.cos(latitude) * Math.sin(longitude);
    // var sz = R2 *lphase* Math.sin(latitude);

    
    return [sx, sy, sz];
    
}



function rotateY(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+0+z*Math.sin(rot);
    var roty = y;
    var rotz = -1*x*Math.sin(rot)+0+z*Math.cos(rot);
    return [rotx, roty, rotz];
}

function rotateX(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x;
    var roty = 0+y*Math.cos(rot)-z*Math.sin(rot);
//    var rotz = 0+y*Math.sin(rot)+z*Math.sin(rot);
    var rotz = 0+y*Math.sin(rot)+z*Math.cos(rot);

    return [rotx, roty, rotz];
}

function rotateZ(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+y*Math.sin(rot)*-1+0;
    var roty = x*Math.sin(rot)+y*Math.cos(rot)+0;
    var rotz = z;

    return [rotx, roty, rotz];
}


function rotate_2d(x,y,rot) {
    //rotate X
    var rotx = x*Math.cos(rot)-y*Math.sin(rot);
    var roty = x*Math.sin(rot)+y*Math.cos(rot);
    return [rotx, roty];
}


var hex_vert = [];

function draw_hex_grid() {
    hex_vert = [];
    context.save();

    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    var radius = 5;
//    var hex_radius = canvas_w/15;
    var hex_radius = canvas_w/10;
    var segs = 3;

    var line_res =20;
    var line_res2 =30;

    var hex_line = [];
    var hex_pat = [];
    

    for(var tri = 0; tri<2; tri++) {
	
	var hex = [];
	
	for(var i = 0; i<segs; i++) {
	    if(tri) {
		var pinc = (i/segs+0.5)%1.0;
	    } else {
		var pinc = (i/segs+0)%1.0;
	    }
	    var x = Math.sin( pinc   *Math.PI*2)*hex_radius;
	    var y = Math.cos( pinc    *Math.PI*2)*hex_radius;
	    hex.push([x, y]);
	}

	
	
	var side_len = dist([hex[0][0],hex[0][1]],[hex[1][0],hex[1][1]]);
	
	var off = dist([hex[0][0],hex[0][1]],[hex[1][0],hex[1][1]]);
	
	for(var i = 0; i<hex.length;i++) {
	    var x = hex[i][0];
	    var y = hex[i][1];
	    if(i == (hex.length-1)) {var next = 0;} else {var next = i+1;}
	    var x1 = hex[next][0];
	    var y1 = hex[next][1];
	    
	    for(var n = 0; n < line_res; n++) {
		var line_x = ((x1-x)*(n/line_res))+x;
		var line_y = ((y1-y)*(n/line_res))+y;
		hex_line.push([line_x, line_y, 1, tri]);
	    }


	    var gap = 0.5;
	    var seg_len = 1;
	    for(var n = 0; n < line_res2; n++) {
//		if(i!=0) continue;
		
		var line_x = ((x1-x)* ( ((n/(line_res2-1))*seg_len) + (1-seg_len)/2 )  ) + x;
		var line_y = ((y1-y)* ( ((n/(line_res2-1))*seg_len) + (1-seg_len)/2 )  ) + y;

		hex_pat.push([line_x*gap, line_y*gap, 1, tri]);
	    }

	    var stop = 0.25;
	    var stop_x = ((x1-x)*stop)+x;
	    var stop_y = ((y1-y)*stop)+y;
	    var stop2 = 1-stop;
	    var stop2_x = ((x1-x)*stop2)+x;
	    var stop2_y = ((y1-y)*stop2)+y;
	    
	    var p = perpen([[x,y],[stop_x,stop_y]], 1, 14);
	    var p2 = perpen([[x1,y1],[stop_x,stop_y]], 1, 14);

	    var line_res3 = 10;
	    for(var n = 0; n < line_res3; n++) {
		var phase =(n/(line_res3-0));
		var line_x = ((stop_x-p[0])*(phase))+stop_x;
		var line_y = ((stop_y-p[1])*(phase))+stop_y;

		var line2_x = ((stop_x-p2[0])*(phase))+stop_x;
		var line2_y = ((stop_y-p2[1])*(phase))+stop_y;
		
		hex_pat.push([line_x, line_y, 3, tri]);
		hex_pat.push([line2_x, line2_y, 3, tri]);
		
	    }

	    var center_off = 1;
	    for(var n = 0; n < line_res2; n++) {

		var x1 = 0;
		var y1 = 0;
		var line_x = x - ((x-x1) * (((n/line_res2) * center_off  ) + 0  )) ;
		var line_y = y - ((y-y1) * (((n/line_res2) * center_off  ) + 0 )) ;
		
		hex_pat.push([line_x, line_y,0.125]);
	    }
	    
	    var center_off2 = 1;

	    var line_res3 = 20;
	    
	    for(var n = 0; n < line_res3; n++) {
		var phase =n/(line_res3-0);
		var clip = (i!=0 && i!=3)? 1:1;
		var x1 = x*(1-center_off2);
		var y1 = y*(1-center_off2);
		var p = perpen([[x,y],[x1,y1]], -1, off);
 		var line_x = p[0] - ((p[0]-x1) * (phase +0  )) ;
		var line_y = p[1] - ((p[1]-y1) * (phase +0  )) ;

		hex_pat.push([line_x,line_y,0.5]);
	    }
	    
	    
	    
	}

    }
	
    var r_num = Math.ceil(canvas.width/off);
    var off_scale = (canvas.width/(r_num))/off;
//    console.log(off_scale);
    var c_off = Math.sqrt(Math.pow(off,2)-Math.pow(off/2,2));
    var c_num = Math.floor(canvas.width/(c_off));
    var c_off_scale = (canvas.width/c_num)/c_off;
    var c_off_scale = 1;

    for(var c=0;c<c_num;c++) {
	center_y = hex_radius+((c_off*c)*off_scale);
	var shift = c%2;
	for(var r=0;r<r_num;r++) {
//	    if(c!=0) continue;
	    if(shift) {
		center_x = ((off*0.5)+(off*r))*off_scale;
	    } else {
		center_x = ((off*1)+(off*r))*off_scale;
	    }

	    var data = [];
	    for(var i = 0; i<hex_line.length;i++) {

		line_x = hex_line[i][0]*off_scale;
		line_y = hex_line[i][1]*off_scale;
		which = hex_line[i][3];

		if(which) {
		    var x_coord = center_x+line_x+off*0.5*off_scale;
		    var y_off = hex_radius - Math.sqrt( Math.pow(hex_radius,2)-Math.pow(side_len*0.5,2) );
		    var y_coord = center_y+line_y+y_off*off_scale;
		} else {
		    var x_coord = center_x+line_x;
		    var y_coord = center_y+line_y;

		}

		hex_vert.push([x_coord,y_coord,hex_line[i][2]]);
		data.push([x_coord,y_coord, off_scale, c_off_scale]);
		if(y_coord < 0 || y_coord > canvas.width) continue;
		if(x_coord < 0 || x_coord > canvas.width) continue;

	    }

	    
	    for(var i = 0; i<hex_pat.length;i++) {

		line_x = hex_pat[i][0]*off_scale;
		line_y = hex_pat[i][1]*off_scale;
		which = hex_pat[i][3];

		if(which) {
		    var x_coord = center_x+line_x+off*0.5*off_scale;
		    var y_off = hex_radius - Math.sqrt( Math.pow(hex_radius,2)-Math.pow(side_len*0.5,2) );
		    var y_coord = center_y+line_y+y_off*off_scale;
		} else {
		    var x_coord = center_x+line_x;
		    var y_coord = center_y+line_y;

		}


		
		// line_x = hex_pat[i][0];
		// line_y = hex_pat[i][1];

		// which = hex_pat[i][3];

		// if(which) {
		//     var x_coord = center_x+line_x+off*0.5*off_scale;
		//     var y_coord = center_y+line_y+off*(1);
		// } else {
		//     var x_coord = center_x+line_x;
		//     var y_coord = center_y+line_y;

		// }

		
		
//		var x_coord = center_x+line_x;
//		var y_coord = center_y+line_y;

		hex_vert.push([x_coord,y_coord, hex_pat[i][2]]);
		data.push([x_coord,y_coord, off_scale, c_off_scale]);
		
		if(y_coord < 0 || y_coord > canvas.width) continue;
		if(x_coord < 0 || x_coord > canvas.width) continue;


	    }
	    if(!MAP_3D) hex_2D(data);
	}
    }

    context.restore();
}

function hex_2D(data) {
    context.save();
    for(var i = 0; i<data.length;i++) {
	var x_coord = data[i][0]*data[i][2];
	var y_coord = data[i][1]*data[i][3];

    	context.beginPath();
	context.arc(x_coord, y_coord, 1, 0, 2 * Math.PI, false);
	var col = 255;
	context.fillStyle = "rgba("+col+", 0, 0, 255)";
	context.fill();
	context.lineWidth = 1;
	context.strokeStyle = "rgba("+col+", 0, 0, 255)";
	context.stroke();
	
    }
    
    context.restore();
}

function hex_3D() {
//    console.log(hex_vert.length);

    context.save();

    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    var radius = 2;

    for(var i = 0; i< hex_vert.length; i++) {

	var vert2D = hex_vert[i];
	var vert3D = to_sphere2([(vert2D[0])*((RADIUS*2*Math.PI)/canvas.width),
				 (vert2D[1]-canvas_w)*((RADIUS*2*Math.PI)/canvas.width)
				]);
	var y_phasor = Math.abs((Math.abs((vert2D[1]/canvas.width)-0.5)*2)-1);
	var z_phasor = Math.abs(vert3D[2])+0.1;
	if(!vert3D.length) continue;
//	console.log(z_phasor);
    	context.beginPath();

	context.arc((canvas_w+vert3D[0]*RADIUS)*1, 
		    (canvas_w+vert3D[1]*RADIUS)*1, 
		    (radius*vert2D[2])*y_phasor*z_phasor, 0, 2 * Math.PI, false);

	// context.arc((canvas_w+vert3D[0]*RADIUS)*1, 
	// 	    (canvas_w+vert3D[1]*RADIUS)*1, 
	// 	    (radius), 0, 2 * Math.PI, false);

//	var col = parseInt(((vert3D[2]+(RADIUS/2))/(RADIUS))*255);
//	col = parseInt(255*0.5+col*0.75);
	var col = 0;
	context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.fill();
	context.lineWidth = 1;
	context.strokeStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.strokeStyle = 'none';
	context.stroke();
    }
    context.restore();
}

function draw(){
    SVG = "";
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    //context.globalAlpha=0.1;
    context.rect(0, 0, canvas.width, canvas.height);
    context.fillStyle = "rgba(255, 255, 255, 255)";
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.stroke();

    
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    
    draw_hex_grid();
    if(MAP_3D) hex_3D();
//    bzr_draw();
//    lats.sort(sortFunction2);
//    console.log([lats[lats.length-1], lats[0]]);
//    hex_3D();

}

//UI
$(function() {
    $( "#radio" ).buttonset();
    
    $( "#eq2 > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "brush-c1y" || this.id == "brush-c2y") {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: 0,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	    
	}});
    
    $( "#eq > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "c1y" || this.id == "c2y") {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else if(this.id == "segs") {
	    $(this).empty().slider({
		value: bzr.segs,
		range: "min",
		animate: false,
		min: 1,
		max: 20,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr
		
	    });
	} else if(this.id == "flip") {
	    $(this).empty().slider({
		value: bzr.flip,
		range: "min",
		animate: false,
		min: 1,
		max: 4,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr,

	    });
	} else if(this.id == "scale") {
	    $(this).empty().slider({
		value: bzr.scale,
		range: "min",
		animate: true,
		min: 0,
		max: 5,
		step:0.1,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw,
		change: function (event) {if (event.originalEvent) {svg_draw();}}

	    });
	} else {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -1,
		max: 1,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	}
    });
    $( "#format" ).button();
    $( "#more" ).button();
    $( "#burst" ).button();
    $( "#download" ).button();
})


function downloadInnerHtml(link,filename, elId, mimeType) {
    mimeType = mimeType || 'text/plain';
    link.setAttribute('download', filename);
    link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(SVG_HEAD+SVG+SVG_TAIL));
}

function save() {$('#download')[0].click();}

function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function load_bzr() {
}



function snap() {
    var inc = 1/5;
    for(var i =0; i<5; i++) {
	snap_inc = (i+1);
	draw();
	svg_draw();
	bzr.ex = bzr.ex-inc;
	bzr.sy = bzr.sy+inc;
	bzr.ey = bzr.ey+inc;
	$('#download')[0].click();
    }
    snap_inc = 0;
}

function get_rand_val(scale, off) {
    var val = ((parseInt(Math.random()*1000)/1000)*scale)+off;
    return parseInt((val*100))/100;
}

function random_gen() {
    bzr.sx = get_rand_val(2, -1);
    bzr.ex = get_rand_val(2, -1);
    bzr.c1x = get_rand_val(2, -1);
    bzr.c1y = get_rand_val(4, -2);
    bzr.c2x = get_rand_val(2, -1);
    bzr.c2y = get_rand_val(4, -2);
    bzr.segs = parseInt(get_rand_val(8, 0)+2)*2;
    bzr.flip = parseInt(get_rand_val(2, 0)+1);

    if(bzr.brush.on) {
	bzr.brush.c1x = get_rand_val(1, 0.5);
	bzr.brush.c1y = get_rand_val(1, 0.5);
	bzr.brush.c2x = get_rand_val(1, 0.5);
	bzr.brush.c2y = get_rand_val(1, 0.5);
    }
    load_bzr();
}


var isRunning = false;
var RUN = "";

function execute(x) {
    setTimeout("eval(" + x + ")", 0);
    // Or: $.globalEval(x);
}

var text_toggle = 0;

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
	if(elem.createTextRange) {
	    var range = elem.createTextRange();
	    range.move('character', caretPos);
	    range.select();
	}
	else {
	    if(elem.selectionStart) {
		elem.focus();
		elem.setSelectionRange(caretPos, caretPos);
	    }
	    else
		elem.focus();
	}
    }
}

$(document).ready(function(){

    if($('#texted').length) {
	
	document.getElementById('texted').onkeydown = function(){

//	    console.log(event.keyCode);
	    
	    if(event.ctrlKey===true && event.keyCode==88){
		var text = $("#texted").val()
		eval(text);
		draw();
		svg_draw();
	    }
	    if(event.ctrlKey===true && event.keyCode==69 && event.shiftKey){

		text_toggle = text_toggle?0:1;
		if(text_toggle) {
	    	    $("#texted").css("color","rgba(255,0,0,0)");
		    $("#texted").attr("readonly","readonly"); 	
		} else {
	    	    $("#texted").css("color","rgba(255,0,0,1)");
		    $("#texted").removeAttr("readonly"); 	
		}
		
	    }

	    if((event.ctrlKey===true && event.keyCode==38) || (event.ctrlKey===true && event.keyCode==40)) {
		var stopCharacters = [' ', '\n', '\r', '\t']
		var text = $('#texted').html();
		var start = $('#texted')[0].selectionStart;
		var end = $('#texted')[0].selectionEnd;
		while (start > 0) {
		    if (stopCharacters.indexOf(text[start]) == -1) {--start;} else {break;}
		};
		++start;
		while (end < text.length) {
		    if (stopCharacters.indexOf(text[end]) == -1) {++end;} else {break;}
		}
		var word = text.substr(start, end - start);

		var fsb = word.search(/\[/);
		var bsb = word.search(/\]/);

		if(fsb >= 0) word = word.substr(fsb+1,word.length);
		if(bsb >= 0) word = word.substr(0, bsb);
		
		var inc = 0.05;
		var next_val = word;
		if(event.keyCode==38) {next_val = parseFloat(word)+0.05;}
		if(event.keyCode==40) {next_val = parseFloat(word)-0.05;}

		if(next_val>0) {next_val = '+'+next_val.toFixed(2);} else {next_val = next_val.toFixed(2);}
		
		var first_half = text.substr(0, start);
		var last_half = text.substr(end, text.length);

		if(fsb >= 0) {
		    $('#texted').html(first_half+'['+next_val+','+last_half);
		} else if(bsb >= 0) {
		    $('#texted').html(first_half+next_val+'],'+last_half);
		} else {
		    $('#texted').html(first_half+next_val+','+last_half);
		}
		
		setCaretPosition("texted", start);

		var new_text = $("#texted").val()
		eval(new_text);
		draw();
		svg_draw();
		
		event.preventDefault();
		event.stopPropagation();
		
	    }
	    
	}
    }
    
    $(".controler").toggle();
    $(".controler2").toggle();
    $("#myCanvas").click(function() {
	$(".controler").toggle();
	if($(".controler2").is(":visible")) {
	    $(".controler2").toggle();
	}else if(bzr.brush.on) {
	    $(".controler2").toggle();
	}
	draw();
	svg_draw();
    });


    $(".l_sel").click(function() {
	var tmp = this.id.split("_");
	var layer = tmp[1];
	//	console.log(layer);
	CUR_LAYER = layer;
	load_bzr();
    });

    
    // $("#format").click(function() {
    // 	var text = $("#format_lab").text();
    // 	if(text == 'PNG') {$("#format_lab").text("SVG");export_format = "SVG";} else {$("#format_lab").text("PNG");export_format = "PNG";}
    // });


    $("#burst").click(function() {
	if(!isRunning) {
	    RUN = setInterval(function () {isRunning = true; random_gen()}, 2000);
	    $("#burst").text("---");
	} else {
	    clearInterval(RUN); isRunning = false;
	    $("#burst").text(">>>");
	}
    });

    $("#more").click(function() {
	$(".controler2").toggle();
	bzr.brush.on = (bzr.brush.on+1)%2;
	draw();
	svg_draw();

    });

    if(!bzr.brush.on) $(".controler2").toggle();
    

    document.getElementById('download').addEventListener('click', function() {

    	var fname = "bzr_";
    	var ftmp = [];
    	for(var k in bzr) {
    	    if(k !== "brush") {
    		var val = bzr[k];
    		if(k == 'line_width') continue;
    		if(k == 'segs' || k == 'flip' || k == 'scale') {
    		    val = val;
    		} else {
    		    val = parseInt(val*100);
    		}
    		ftmp.push(val);
    	    } else {
    		for(var n in bzr.brush) {
    		    if(n == 'on') continue;
    		    var val = parseInt(bzr.brush[n]*100);
    		    ftmp.push(val);
    		}
    	    }
    	}

//    	fname = fname+ftmp.join('_');
	fname = "mandalala_hex";
	
    	if(snap_inc) fname = snap_inc+'_'+fname;
	    
    	if(export_format == 'SVG') {
    	    downloadInnerHtml(this, fname+'.svg','svg');
    	} else {
//	    console.log(this);
    	    downloadCanvas(this, 'myCanvas', fname+'.png');
    	}

    }, false);


    
    load_bzr();
    
    if(SVG_DEBUG) {
	$( "body" ).append("<div id=\"svg\"></div>");
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
	$(".container").css("float","left");
    } else {
	$(".container").css("float","none");
    }

    $(".mandalala_container").toggle();
    
});

function refreshbzr(event,ui) {
    if (event.originalEvent) {
	var id = this.id;
	var id_split = id.split("-");
	var val = $(this).slider("value");
	if(id_split[0] !== "brush") {
	    // if(id == "sy") {
	    // 	bzrs["ey"][CUR_LAYER] = val;
	    // 	$("#ey_val").text(val);
	    // }
	    bzrs[id][CUR_LAYER] = val;
	} else {
//	    console.log([id_split[0],id_split[1]]);
	    bzrs[id_split[0]][id_split[1]][CUR_LAYER] = val;
	}
	$("#"+id+"_val").text(val);
    }
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}

function refreshbzr_brush(event,ui) {
    $( "#eq2 > span" ).each(function() {
	if(this.id == 'brush-c1x') {bzr.brush.c1x = $(this).slider("value");$("#brush-c1x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c1y') {bzr.brush.c1y = $(this).slider("value");$("#brush-c1y_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2x') {bzr.brush.c2x = $(this).slider("value");$("#brush-c2x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2y') {bzr.brush.c2y = $(this).slider("value");$("#brush-c2y_val").text($(this).slider("value"));}

    });
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}


function svg_draw() {

    if(SVG_DEBUG && $('#svg').length) {
	document.getElementById('svg').innerHTML = "";
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg'));

    }
}

  
draw();
