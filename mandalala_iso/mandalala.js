
var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var SVG_DEBUG = 0;
var WIRE_FRAME = 0;
var MAP_3D = 1;
var CLIP_Z = 1;
var DRAW_GRID = 0;
if (typeof mandalala_width !== 'undefined') {
    // the variable is defined
    canvas.width = mandalala_width;
    canvas.height = mandalala_width;
} else {
    canvas.width = 640;
    canvas.height = 640;

}

var RADIUS = (canvas.width/2)*0.97;

//console.log(mandalala_width);

if(SVG_DEBUG) {
    $("#mandalala_container").css("width", (canvas.width+10)*2);
    $("#mandalala_container").css("height", canvas.height+10);
} else {
    $("#mandalala_container").css("width", canvas.width+10);
    $("#mandalala_container").css("height", canvas.height+10);
}

$("#text_container").css("width", canvas.width);
$("#text_container").css("height", canvas.height);
$('#texted').css("width", canvas.width);
$('#texted').css("height", canvas.height);

$('.controler').css("width", canvas.width);
$('.controler').css("top", canvas.height-200);
$('.controler2').css("width", canvas.width);

var export_format = "PNG";
var seg_w = canvas.width/2;
var seg_h = canvas.width/2;

var CUR_LAYER = 0;
var snap_inc = 0;
//svg
var SVG_HEAD = "<svg xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' width='"+canvas.width+"' height='"+canvas.height+"' style='stroke:#000000;stroke-width:1;fill:none;'>";
SVG_HEAD +=  "<rect x='0' y='0' width='"+canvas.width+"' height='"+canvas.height+"' style='fill:none;stroke:#000000;stroke-width:1;'/>";

var SVG_TAIL = "</svg>";

var bzr = {
    line_width: 1,
    sx: 0.2,
    sy: 0.125,
    c1x: 0.3,
    c1y: 0.1,
    c2x: 0.4,
    c2y: 0.2,
    ex: 0.5,
    ey: 0,
    ox: 0,
    oy: 0,
    segs: 8,
    flip: 1,
    scale: 1,
    brush: {
	on:1,
	c1y:0.46,
	c1x: 0.77,
	c2x:0.91,
	c2y: 1.7,
    }
}

var g_rot_y = -0.8;
var g_rot_x = 1;
var g_rot_z = -0.5;
function to_sphere2(xy) {
    var t = 0;

    var sphere = xy2sphere(xy[0], xy[1], t);

    
//    console.log(sphere);
    
    var rot = rotateY(sphere[0],sphere[1],sphere[2],g_rot_y);
    var rot2 = rotateX(rot[0],rot[1],rot[2],g_rot_x);
    var rot3 = rotateZ(rot2[0],rot2[1],rot2[2],g_rot_z);
    if (CLIP_Z && rot2[2]<0) return [];
//   if(rot2[2]<0) return [];
//    var result = [rot2[0]*RADIUS,rot2[1]*RADIUS,rot2[2]*RADIUS];
//    var result = [rot3[0]*RADIUS,rot3[1]*RADIUS,rot3[2]*RADIUS];
    var result = [rot3[0],rot3[1],rot3[2]];
    
    
    // var pz = (rot2[2]-4)*-1;
    // var d = 1;
    // var perspective = [((d*rot2[0])/pz)*canvas.width*1.825,
    //   		       ((((d*(rot2[1]))/pz))+0.0) *canvas.width*1.825, 
    //   		       -1];

    
    return result;
//    return sphere;
//    return perspective;
}


function perpen(pts, d, off) {
    //		vector AB = (Bx-Ax, By-Ay) = (BAx, BAy)
    var vec = [];
    vec[0] = pts[1][0]-pts[0][0];
    vec[1] = pts[1][1]-pts[0][1];
    //		var len = Math.sqrt( Math.pow(vec[0],2) + Math.pow(vec[1]) );
    var len = Math.sqrt(Math.pow(vec[0],2)+Math.pow(vec[1],2))
    //         		(BAx, BAy)
    // unit vector AB = ------------------,  where length = sqrt(BAx^2 + BAy^2)
    //                        length
    var unit = [];
    unit[0] = vec[0]/len;
    unit[1] = vec[1]/len;

    //                                   (-BAy, BAx)
    // unit vector perpendicular to AB = -------------
    //                                     length

    var per = [];
    per[0] = (-1*vec[1])/len;
    per[1] = (vec[0])/len;

    //		coordinate at t1 = (Bx, By) + t1 * (unit vector perpendicular to AB)
    
    var per_t1 = [];
    if(d == 1) {
	per_t1[0] = pts[1][0]-off*per[0];
	per_t1[1] = pts[1][1]-off*per[1];
    } else {
	per_t1[0] = pts[1][0]+off*per[0];
	per_t1[1] = pts[1][1]+off*per[1];

    }
    return per_t1;
}

function dist(p1, p2) {
    var dist = Math.sqrt(Math.pow(p2[0]-p1[0],2)+Math.pow(p2[1]-p1[1],2));
    return dist;
}

var zseg = [];

function sortFunction(a, b) {
    if (a[0] === b[0]) {return 0;} else {return (a[0] < b[0]) ? -1 : 1;}
}



// Given a "mapping sphere" of radius R,
// the Mercator projection (x,y) of a given latitude and longitude is:
// x = R * longitude
// y = R * log( tan( (latitude + pi/2)/2 ) )

// and the inverse mapping of a given map location (x,y) is:
// longitude = x / R
// latitude = 2 * atan(exp(y/R)) - pi/2
// To get the 3D coordinates from the result of the inverse mapping:

// Given longitude and latitude on a sphere of radius S,
// the 3D coordinates P = (P.x, P.y, P.z) are:
// P.x = S * cos(latitude) * cos(longitude)
// P.y = S * cos(latitude) * sin(longitude)
// P.z = S * sin(latitude)

//Latitude measurements range from 0° to (+/–)90°. Longitude measures how far east or west of the prime meridian a place is located. The prime meridian runs through Greenwich, England. Longitude measurements range from 0° to (+/–)180°.

function xy2sphere(x,y,t) {
    var R = RADIUS;
    var longitude = x / R;
    var latitude = 2 * Math.atan(Math.exp(y/R)) - Math.PI/2;

//    console.log([longitude, latitude]);
    
    var inc = (latitude+Math.PI)/(Math.PI*2);

    var R2 = RADIUS;
    R2 =1;
    var sx = R2 * Math.cos(latitude) * Math.cos(longitude);
    var sy = R2 * Math.cos(latitude) * Math.sin(longitude);
    var sz = R2 * Math.sin(latitude);

    return [sx, sy, sz];
    
}


function rotateY(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+0+z*Math.sin(rot);
    var roty = y;
    var rotz = -1*x*Math.sin(rot)+0+z*Math.cos(rot);
    return [rotx, roty, rotz];
}

function rotateX(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x;
    var roty = 0+y*Math.cos(rot)-z*Math.sin(rot);
//    var rotz = 0+y*Math.sin(rot)+z*Math.sin(rot);
    var rotz = 0+y*Math.sin(rot)+z*Math.cos(rot);

    return [rotx, roty, rotz];
}

function rotateZ(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+y*Math.sin(rot)*-1+0;
    var roty = x*Math.sin(rot)+y*Math.cos(rot)+0;
    var rotz = z;

    return [rotx, roty, rotz];
}


function rotate_2d(x,y,rot) {
    //rotate X
    var rotx = x*Math.cos(rot)-y*Math.sin(rot);
    var roty = x*Math.sin(rot)+y*Math.cos(rot);
    return [rotx, roty];
}


var hex_vert = [];

function xy2iso(x, y) {
    var off = 0;
    var width = 1;
    var height = 1;
    var map_x = (x-y)*(width*0.5);
    var map_y = (x+y)*(height*1);

    
    return [map_x, map_y];
}

var grid_scale = 1.5;
function draw_hex_grid() {
    hex_vert = [];
    data = [];
    context.save();

    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    var radius = 5;
    var line_res = 20;
    var block_res = 5;
    var debug_line_res = 5;
    var grid_num = 60;
    var grid_w = (canvas.width/grid_num)*grid_scale;

    var bl = (((canvas.width)*((grid_num-1)/grid_num))*grid_scale)+grid_w;
    var last_iso = xy2iso(bl,bl);
    var iso_center_y = last_iso[1]/2;
    var iso_center_x = last_iso[0]/2;
    var grid_y_off = (canvas.height/2)-iso_center_y;
    var grid_x_off = (canvas.height/2)-iso_center_x;
    
    for(var r=0; r<grid_num;r++) {
	for(var c=0; c<grid_num;c++) {
	    x = ((canvas.width)*(r/(grid_num-0))*grid_scale);
	    y = ((canvas.width)*(c/(grid_num-0))*grid_scale);
	    
	    iso = xy2iso(x+grid_w*0.5, y+grid_w*0.5);
	    map_x = iso[0]+grid_x_off;
	    map_y = iso[1]+grid_y_off;
	    if(map_y < 0 || map_y > canvas.width) continue;
	    if(map_x < 0 || map_x > canvas.width) continue;
	    hex_vert.push([map_x,map_y]);
	    data.push([map_x,map_y]);

	    var dia = [[c, r],[r, c-1]];

	    if(DRAW_GRID) {
		for(var p = 0; p<grid_num/2; p++) {
		    for(var d=0; d<2;d++) {
			for(var l=0;l<debug_line_res;l++) {
			    if(d==0) {
				x_coord = x+((l/(debug_line_res-1))*grid_w);
				y_coord = y;
			    } else {
	     			y_coord = y+((l/(debug_line_res-1))*grid_w);
	     			x_coord = x;
			    }
			    iso = xy2iso(x_coord, y_coord);
			    map_x = iso[0]+grid_x_off;
			    map_y = iso[1]+grid_y_off;
			    if(map_y < 0 || map_y > canvas.width) continue;
			    if(map_x < 0 || map_x > canvas.width) continue;
			    hex_vert.push([map_x,map_y]);
			    data.push([map_x,map_y]);
			    
			}
		    }
		}
	    }


 	    var pat_res = 6;
	    for(var p = 0; p<grid_num/2; p++) {
		for(var d=0; d<dia.length;d++) {
		    var d1 = dia[d][0];
		    var d2 = dia[d][1];
		    if(d1 == p*2 && d2 < p*2) {
			x_coord = x+(grid_w/2);
			y_coord = y+(grid_w/2);

			iso = xy2iso(x_coord, y_coord);
			map_x = iso[0]+grid_x_off;
			map_y = iso[1]+grid_y_off;

			if(map_y < 0 || map_y > canvas.width) continue;
			if(map_x < 0 || map_x > canvas.width) continue;

			var pscale = 0.75;
			var x1 = x+(grid_w*(1-pscale)*0.5);
			var y1 = y+(grid_w*(1-pscale)*0.5);
			var x2 = x+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			var y2 = y+(grid_w*(1-pscale)*0.5);
			var x3 = x+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			var y3 = y+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			var x4 = x+(grid_w*(1-pscale)*0.5);
			var y4 = y+grid_w*pscale+(grid_w*(1-pscale)*0.5);

			var points = [[x1, y1],[x2, y2],[x3, y3],[x4, y4]];
			
			var bscale = 1.75;
			if(d==0){
			    var bx1 = x+(grid_w*(1-bscale)*0.5);
			    var by1 = y+(grid_w*(1-bscale)*0.5);
			    var bx2 = x+grid_w*bscale+(grid_w*(1-bscale)*0.5);
			    var by2 = y+(grid_w*(1-bscale)*0.5);
			    var bx3 = x+grid_w*bscale+(grid_w*(1-bscale)*0.5);
			    var by3 = y+grid_w*bscale+(grid_w*(1-bscale)*0.5);
			    var bx4 = x+(grid_w*(1-bscale)*0.5);
			    var by4 = y+grid_w*bscale+(grid_w*(1-bscale)*0.5);

			} else {

			    var bx2 = x+(grid_w*(1-bscale)*0.5);
			    var by2 = y+(grid_w*(1-bscale)*0.5);

			    var bx3 = x+grid_w*bscale+(grid_w*(1-bscale)*0.5);
			    var by3 = y+(grid_w*(1-bscale)*0.5);

			    var bx4 = x+grid_w*bscale+(grid_w*(1-bscale)*0.5);
			    var by4 = y+grid_w*bscale+(grid_w*(1-bscale)*0.5);

			    var bx1 = x+(grid_w*(1-bscale)*0.5);
			    var by1 = y+grid_w*bscale+(grid_w*(1-bscale)*0.5);

			    
			}
			
			
			var bpoints = [[bx1, by1],[bx2, by2],[bx3, by3],[bx4, by4]];
			block_to(bpoints,[grid_x_off, grid_y_off], block_res,d);

			for(var m = 0; m < points.length; m++) {
			    var p1 = points[m];
			    if(m == points.length-1) {var p2 = points[0];} else {var p2 = points[m+1];}
			    line_to([p1[0], p1[1]],[p2[0], p2[1]],[0.25, 0.75],[grid_x_off, grid_y_off], line_res);

			}

			if(d ==1 && d2 == (p*2)-1) {
			    var pscale = 1;
			    var x1 = x+(grid_w*(1-pscale)*0.5);
			    var y1 = y+(grid_w*(1-pscale)*0.5);
			    var x2 = x+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			    var y2 = y+(grid_w*(1-pscale)*0.5);
			    var x3 = x+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			    var y3 = y+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			    var x4 = x+(grid_w*(1-pscale)*0.5);
			    var y4 = y+grid_w*pscale+(grid_w*(1-pscale)*0.5);
			    
			    var points = [[x1, y1],[x2, y2],[x3, y3],[x4, y4]];

			    for(var m = 0; m < points.length; m++) {
				var p1 = points[m];
				if(m == points.length-1) {var p2 = points[0];} else {var p2 = points[m+1];}
				line_to([p1[0], p1[1]],[p2[0], p2[1]],[0.5, 0.5],[grid_x_off, grid_y_off], line_res);
			    }
			    continue;
			}
			
			for(var f = 0; f<2; f++) {
			    if(d == 1) {
				var x1 = x+grid_w*f; var y1 = y;
				var x2 = x+grid_w*f; var y2 = y+grid_w;
			    } else {
				var x1 = x; var y1 = y+grid_w*f;
				var x2 = x+grid_w; var y2 = y+grid_w*f;

			    }

			    line_to([x1, y1],[x2, y2],[0.5, 0.5],[grid_x_off, grid_y_off], line_res);
			    
			}

			
			if(d == 1) {
			    var x1 = x; var y1 = y;
			    var x2 = x+grid_w; var y2 = y;
			} else {
			    var x1 = x; var y1 = y;
			    var x2 = x; var y2 = y+grid_w;
			}

			line_to([x1, y1],[x2, y2],[0.3, 0.7],[grid_x_off, grid_y_off], line_res);
			
			for(var m=0; m<2; m++) {

			    var pscale2 = 0.5;
			    if(m==0){var para = 0.25;} else {var para = 0.75;}
			    if(d == 1) {
				var x1 = x+(grid_w*(1-pscale2)*0.5);
				var y1 = y+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
				var x2 = x+grid_w*pscale2+(grid_w*(1-pscale2)*0.5);
				var y2 = y+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
			    } else {
				var x1 = x+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
				var y1 = y+(grid_w*(1-pscale2)*0.5);
				var x2 = x+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
				var y2 = y+grid_w*pscale2+(grid_w*(1-pscale2)*0.5);
			    }
			    line_to([x1, y1],[x2, y2],[0.25, 0.75],[grid_x_off, grid_y_off], line_res);
			}
			
			for(var m=0; m<2; m++) {
			    if(m==0){var para = 0.25;} else {var para = 0.75;}
			    var pscale2 = 0.5;
			    if(d == 1) {
				var x1 = x+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
				var y1 = y+(grid_w*(1-pscale2)*0.5);
				var x2 = x1;
				var y2 = y1+(grid_w*pscale2);
			    } else {

				var x1 = x+(grid_w*(1-pscale2)*0.5);
				var y1 = y+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
				var x2 = x1+(grid_w*pscale2);
				var y2 = y1;
			    }
			    line_to([x1, y1],[x2, y2],[0.5, 0.5],[grid_x_off, grid_y_off], line_res);
			}


			var para = 0.5;
			var pscale2 = 0.75;
			if(d == 1) {
			    var x1 = x+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
			    var y1 = y+(grid_w*(1-pscale2)*0.5);
			    var x2 = x1;
			    var y2 = y1+(grid_w*pscale2);
			} else {
			    
			    var x1 = x+(grid_w*(1-pscale2)*0.5);
			    var y1 = y+(grid_w*(1-pscale2)*0.5)+(grid_w*(para))*pscale2;
			    var x2 = x1+(grid_w*pscale2);
			    var y2 = y1;
			}
			line_to([x1, y1],[x2, y2],[0.375, 0.625],[grid_x_off, grid_y_off], line_res);

			
		    }
		}
		
	    }
	    
	}
    }

    if(!MAP_3D) hex_2D(data);
    
    context.restore();
}

function block_to(pts, off,block_res,d) {
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    context.save();
    for(var n=0; n<block_res;n++) {
	var line_phase = n/(block_res-1);
	var next = n+1;
	var next_phase = next/(block_res-1);
	var x_diff = (pts[1][0]-pts[0][0]);
	var y_diff = (pts[1][1]-pts[0][1]);
	if(next == block_res) continue;
	if(d == 0) {
	    // x_coord = (pts[0][0]+(x_diff*line_phase));
	    // y_coord = (pts[0][1]+(y_diff*line_phase));

	    // x_coord2 = (pts[0][0]+(x_diff*line_phase))+(x_diff/(block_res-1));
	    // y_coord2 = (pts[0][1]+(y_diff*line_phase));

	    // x_coord3 = (pts[0][0]+(x_diff*line_phase))+(x_diff/(block_res-1));
	    // y_coord3 = (pts[2][1]+(y_diff*line_phase));

	    // x_coord4 = (pts[3][0]+(x_diff*line_phase));
	    // y_coord4 = (pts[3][1]+(y_diff*line_phase));

	    x_coord = (pts[0][0]+(x_diff*line_phase));
	    y_coord = (pts[0][1]+(y_diff*line_phase));

	    x_coord2 = pts[1][0]+(x_diff*line_phase)-((x_diff/(block_res-1))*(block_res-2));
	    y_coord2 = (pts[1][1]+(y_diff*line_phase));

	    x_coord3 = pts[2][0]+(x_diff*line_phase)-((x_diff/(block_res-1))*(block_res-2));
	    y_coord3 = (pts[2][1]+(y_diff*line_phase));

	    x_coord4 = (pts[3][0]+(x_diff*line_phase));
	    y_coord4 = (pts[3][1]+(y_diff*line_phase));

	    
	} else {

	    x_coord = (pts[0][0]+(x_diff*line_phase));
	    y_coord = (pts[0][1]+(y_diff*line_phase));

	    x_coord2 = pts[1][0]+(x_diff*line_phase);
	    y_coord2 = (pts[1][1]+(y_diff*line_phase))-((y_diff/(block_res-1))*(block_res-2));

	    x_coord3 = pts[2][0]+(x_diff*line_phase);
	    y_coord3 = (pts[2][1]+(y_diff*line_phase))-((y_diff/(block_res-1))*(block_res-2));

	    x_coord4 = (pts[3][0]+(x_diff*line_phase));
	    y_coord4 = (pts[3][1]+(y_diff*line_phase));


	}
	var iso = xy2iso(x_coord, y_coord);
	var iso2 = xy2iso(x_coord2, y_coord2);
	var iso3 = xy2iso(x_coord3, y_coord3);
	var iso4 = xy2iso(x_coord4, y_coord4);
//	var iso = xy2iso(pts[0][0],pts[0][1]);
	var map_x = iso[0]+off[0];
	var map_y = iso[1]+off[1];
	var map_x2 = iso2[0]+off[0];
	var map_y2 = iso2[1]+off[1];
	var map_x3 = iso3[0]+off[0];
	var map_y3 = iso3[1]+off[1];
	var map_x4 = iso4[0]+off[0];
	var map_y4 = iso4[1]+off[1];

	var vert3D = to_sphere2([(map_x)*((RADIUS*2*Math.PI)/canvas.width), (map_y-canvas_w)*((RADIUS*2*Math.PI)/canvas.width)]);
	var vert3D2 = to_sphere2([(map_x2)*((RADIUS*2*Math.PI)/canvas.width), (map_y2-canvas_w)*((RADIUS*2*Math.PI)/canvas.width)]);
	var vert3D3 = to_sphere2([(map_x3)*((RADIUS*2*Math.PI)/canvas.width), (map_y3-canvas_w)*((RADIUS*2*Math.PI)/canvas.width)]);
	var vert3D4 = to_sphere2([(map_x4)*((RADIUS*2*Math.PI)/canvas.width), (map_y4-canvas_w)*((RADIUS*2*Math.PI)/canvas.width)]);


	context.beginPath();

 
	
	// context.moveTo(map_x, map_y);
	// context.lineTo(map_x2, map_y2);
	// context.lineTo(map_x3, map_y3);
	// context.lineTo(map_x4, map_y4);

	// context.arc(map_x, map_y, 2, 0, 2 * Math.PI, false);
	// context.arc(map_x2, map_y2, 5, 0, 2 * Math.PI, false);
	// context.arc(map_x3, map_y3, 5, 0, 2 * Math.PI, false);
	// context.arc(map_x4, map_y4, 2, 0, 2 * Math.PI, false);
	
	context.moveTo(canvas_w+vert3D[0]*RADIUS, canvas_w+vert3D[1]*RADIUS);
	context.lineTo(canvas_w+vert3D2[0]*RADIUS, canvas_w+vert3D2[1]*RADIUS);	
	context.lineTo(canvas_w+vert3D3[0]*RADIUS, canvas_w+vert3D3[1]*RADIUS);
	context.lineTo(canvas_w+vert3D4[0]*RADIUS, canvas_w+vert3D4[1]*RADIUS);
	
	var stroke_fill = "rgba(0, 0, 0, 1)";
	var stroke_style = "rgba(0, 0, 0, 1)";

	context.fillStyle = stroke_fill;
	context.fill();
	context.lineWidth = 1;
	context.strokeStyle = stroke_style;
	context.closePath();
	context.stroke();
	
    }
    context.restore();
}

function line_to(p1, p2, bp, off,line_res) {
    for(var n=0; n<line_res;n++) {
	var line_phase = n/(line_res-1);
	if(line_phase > bp[0] && line_phase < bp[1]) continue;
	x_coord = (p1[0]+((p2[0]-p1[0])*line_phase));
	y_coord = (p1[1]+((p2[1]-p1[1])*line_phase));
	var iso = xy2iso(x_coord, y_coord);
	map_x = iso[0]+off[0];
	map_y = iso[1]+off[1];
	hex_vert.push([map_x,map_y]);
	data.push([map_x,map_y]);
    }

}


function line_to2(p1, p2, bp, off,line_res, phasor_scale) {
    for(var n=0; n<line_res;n++) {
	var line_phase = ((n/line_res)*1)+0;
	if(line_phase > bp[0] && line_phase < bp[1]) continue;
	x_coord = (p1[0]+((p2[0]-p1[0])*line_phase*phasor_scale));
	y_coord = (p1[1]+((p2[1]-p1[1])*line_phase*phasor_scale));
	var iso = xy2iso(x_coord, y_coord);
	map_x = iso[0]+off[0];
	map_y = iso[1]+off[1];
	hex_vert.push([map_x,map_y]);
	data.push([map_x,map_y]);
    }

}


function hex_2D(data) {
    context.save();
    for(var i = 0; i<data.length;i++) {
	var x_coord = data[i][0];
	var y_coord = data[i][1];

    	context.beginPath();
	context.arc(x_coord, y_coord, 1, 0, 2 * Math.PI, false);
	var col = 255;
	context.fillStyle = "rgba("+col+", 0, 0, 255)";
	context.fill();
	context.lineWidth = 1;
	context.strokeStyle = "rgba("+col+", 0, 0, 255)";
	context.stroke();
	
    }
    
    context.restore();
}

function hex_3D() {
    context.save();
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    var radius = 2;
    for(var i = 0; i< hex_vert.length; i++) {

	var vert2D = hex_vert[i];
	var vert3D = to_sphere2([(vert2D[0])*((RADIUS*2*Math.PI)/canvas.width),
				 (vert2D[1]-canvas_w)*((RADIUS*2*Math.PI)/canvas.width)
				]);
	var y_phasor = Math.abs((Math.abs((vert2D[1]/canvas.width)-0.5)*2)-1);
	var z_phasor = vert3D[2];
//	var z_phasor = 1;
	if(!vert3D.length) continue;
    	context.beginPath();
	context.arc((canvas_w+vert3D[0]*RADIUS)*1, 
		    (canvas_w+vert3D[1]*RADIUS)*1, 
		    radius*y_phasor*z_phasor, 0, 2 * Math.PI, false);
	var col = 255;
	context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.fill();
	context.lineWidth = 1;
	context.strokeStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.strokeStyle = 'none';
	context.stroke();
    }
    context.restore();
}

function draw(){
    SVG = "";
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    //context.globalAlpha=0.1;
    context.rect(0, 0, canvas.width, canvas.height);
    context.fillStyle = "rgba(255, 255, 255, 255)";
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.stroke();

    
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    
    draw_hex_grid();
    if(MAP_3D) hex_3D();
//    hex_3D();

}

//UI
$(function() {
    $( "#radio" ).buttonset();
    
    $( "#eq2 > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "brush-c1y" || this.id == "brush-c2y") {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: 0,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	    
	}});
    
    $( "#eq > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "c1y" || this.id == "c2y") {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else if(this.id == "segs") {
	    $(this).empty().slider({
		value: bzr.segs,
		range: "min",
		animate: false,
		min: 1,
		max: 20,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr
		
	    });
	} else if(this.id == "flip") {
	    $(this).empty().slider({
		value: bzr.flip,
		range: "min",
		animate: false,
		min: 1,
		max: 4,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr,

	    });
	} else if(this.id == "scale") {
	    $(this).empty().slider({
		value: bzr.scale,
		range: "min",
		animate: true,
		min: 0,
		max: 5,
		step:0.1,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw,
		change: function (event) {if (event.originalEvent) {svg_draw();}}

	    });
	} else {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -1,
		max: 1,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	}
    });
    $( "#format" ).button();
    $( "#more" ).button();
    $( "#burst" ).button();
    $( "#download" ).button();
})


function downloadInnerHtml(link,filename, elId, mimeType) {
    mimeType = mimeType || 'text/plain';
    link.setAttribute('download', filename);
    link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(SVG_HEAD+SVG+SVG_TAIL));
}

function save() {$('#download')[0].click();}

function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function load_bzr() {
}



function snap() {
    var inc = 1/5;
    for(var i =0; i<5; i++) {
	snap_inc = (i+1);
	draw();
	svg_draw();
	bzr.ex = bzr.ex-inc;
	bzr.sy = bzr.sy+inc;
	bzr.ey = bzr.ey+inc;
	$('#download')[0].click();
    }
    snap_inc = 0;
}

function get_rand_val(scale, off) {
    var val = ((parseInt(Math.random()*1000)/1000)*scale)+off;
    return parseInt((val*100))/100;
}

function random_gen() {
    bzr.sx = get_rand_val(2, -1);
    bzr.ex = get_rand_val(2, -1);
    bzr.c1x = get_rand_val(2, -1);
    bzr.c1y = get_rand_val(4, -2);
    bzr.c2x = get_rand_val(2, -1);
    bzr.c2y = get_rand_val(4, -2);
    bzr.segs = parseInt(get_rand_val(8, 0)+2)*2;
    bzr.flip = parseInt(get_rand_val(2, 0)+1);

    if(bzr.brush.on) {
	bzr.brush.c1x = get_rand_val(1, 0.5);
	bzr.brush.c1y = get_rand_val(1, 0.5);
	bzr.brush.c2x = get_rand_val(1, 0.5);
	bzr.brush.c2y = get_rand_val(1, 0.5);
    }
    load_bzr();
}


var isRunning = false;
var RUN = "";

function execute(x) {
    setTimeout("eval(" + x + ")", 0);
    // Or: $.globalEval(x);
}

var text_toggle = 0;

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
	if(elem.createTextRange) {
	    var range = elem.createTextRange();
	    range.move('character', caretPos);
	    range.select();
	}
	else {
	    if(elem.selectionStart) {
		elem.focus();
		elem.setSelectionRange(caretPos, caretPos);
	    }
	    else
		elem.focus();
	}
    }
}

$(document).ready(function(){

    if($('#texted').length) {
	
	document.getElementById('texted').onkeydown = function(){

//	    console.log(event.keyCode);
	    
	    if(event.ctrlKey===true && event.keyCode==88){
		var text = $("#texted").val()
		eval(text);
		draw();
		svg_draw();
	    }
	    if(event.ctrlKey===true && event.keyCode==69 && event.shiftKey){

		text_toggle = text_toggle?0:1;
		if(text_toggle) {
	    	    $("#texted").css("color","rgba(255,0,0,0)");
		    $("#texted").attr("readonly","readonly"); 	
		} else {
	    	    $("#texted").css("color","rgba(255,0,0,1)");
		    $("#texted").removeAttr("readonly"); 	
		}
		
	    }

	    if((event.ctrlKey===true && event.keyCode==38) || (event.ctrlKey===true && event.keyCode==40)) {
		var stopCharacters = [' ', '\n', '\r', '\t']
		var text = $('#texted').html();
		var start = $('#texted')[0].selectionStart;
		var end = $('#texted')[0].selectionEnd;
		while (start > 0) {
		    if (stopCharacters.indexOf(text[start]) == -1) {--start;} else {break;}
		};
		++start;
		while (end < text.length) {
		    if (stopCharacters.indexOf(text[end]) == -1) {++end;} else {break;}
		}
		var word = text.substr(start, end - start);

		var fsb = word.search(/\[/);
		var bsb = word.search(/\]/);

		if(fsb >= 0) word = word.substr(fsb+1,word.length);
		if(bsb >= 0) word = word.substr(0, bsb);
		
		var inc = 0.05;
		var next_val = word;
		if(event.keyCode==38) {next_val = parseFloat(word)+0.05;}
		if(event.keyCode==40) {next_val = parseFloat(word)-0.05;}

		if(next_val>0) {next_val = '+'+next_val.toFixed(2);} else {next_val = next_val.toFixed(2);}
		
		var first_half = text.substr(0, start);
		var last_half = text.substr(end, text.length);

		if(fsb >= 0) {
		    $('#texted').html(first_half+'['+next_val+','+last_half);
		} else if(bsb >= 0) {
		    $('#texted').html(first_half+next_val+'],'+last_half);
		} else {
		    $('#texted').html(first_half+next_val+','+last_half);
		}
		
		setCaretPosition("texted", start);

		var new_text = $("#texted").val()
		eval(new_text);
		draw();
		svg_draw();
		
		event.preventDefault();
		event.stopPropagation();
		
	    }
	    
	}
    }
    
    $(".controler").toggle();
    $(".controler2").toggle();
    $("#myCanvas").click(function() {
	$(".controler").toggle();
	if($(".controler2").is(":visible")) {
	    $(".controler2").toggle();
	}else if(bzr.brush.on) {
	    $(".controler2").toggle();
	}
	draw();
	svg_draw();
    });


    $(".l_sel").click(function() {
	var tmp = this.id.split("_");
	var layer = tmp[1];
	//	console.log(layer);
	CUR_LAYER = layer;
	load_bzr();
    });

    
    // $("#format").click(function() {
    // 	var text = $("#format_lab").text();
    // 	if(text == 'PNG') {$("#format_lab").text("SVG");export_format = "SVG";} else {$("#format_lab").text("PNG");export_format = "PNG";}
    // });


    $("#burst").click(function() {
	if(!isRunning) {
	    RUN = setInterval(function () {isRunning = true; random_gen()}, 2000);
	    $("#burst").text("---");
	} else {
	    clearInterval(RUN); isRunning = false;
	    $("#burst").text(">>>");
	}
    });

    $("#more").click(function() {
	$(".controler2").toggle();
	bzr.brush.on = (bzr.brush.on+1)%2;
	draw();
	svg_draw();

    });

    if(!bzr.brush.on) $(".controler2").toggle();
    

    document.getElementById('download').addEventListener('click', function() {

    	var fname = "bzr_";
    	var ftmp = [];
    	for(var k in bzr) {
    	    if(k !== "brush") {
    		var val = bzr[k];
    		if(k == 'line_width') continue;
    		if(k == 'segs' || k == 'flip' || k == 'scale') {
    		    val = val;
    		} else {
    		    val = parseInt(val*100);
    		}
    		ftmp.push(val);
    	    } else {
    		for(var n in bzr.brush) {
    		    if(n == 'on') continue;
    		    var val = parseInt(bzr.brush[n]*100);
    		    ftmp.push(val);
    		}
    	    }
    	}

//    	fname = fname+ftmp.join('_');
	fname = "mandalala_hex";
	
    	if(snap_inc) fname = snap_inc+'_'+fname;
	    
    	if(export_format == 'SVG') {
    	    downloadInnerHtml(this, fname+'.svg','svg');
    	} else {
//	    console.log(this);
    	    downloadCanvas(this, 'myCanvas', fname+'.png');
    	}

    }, false);


    
    load_bzr();
    
    if(SVG_DEBUG) {
	$( "body" ).append("<div id=\"svg\"></div>");
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
	$(".container").css("float","left");
    } else {
	$(".container").css("float","none");
    }

    $(".mandalala_container").toggle();
    
});

function refreshbzr(event,ui) {
    if (event.originalEvent) {
	var id = this.id;
	var id_split = id.split("-");
	var val = $(this).slider("value");
	if(id_split[0] !== "brush") {
	    // if(id == "sy") {
	    // 	bzrs["ey"][CUR_LAYER] = val;
	    // 	$("#ey_val").text(val);
	    // }
	    bzrs[id][CUR_LAYER] = val;
	} else {
//	    console.log([id_split[0],id_split[1]]);
	    bzrs[id_split[0]][id_split[1]][CUR_LAYER] = val;
	}
	$("#"+id+"_val").text(val);
    }
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}

function refreshbzr_brush(event,ui) {
    $( "#eq2 > span" ).each(function() {
	if(this.id == 'brush-c1x') {bzr.brush.c1x = $(this).slider("value");$("#brush-c1x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c1y') {bzr.brush.c1y = $(this).slider("value");$("#brush-c1y_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2x') {bzr.brush.c2x = $(this).slider("value");$("#brush-c2x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2y') {bzr.brush.c2y = $(this).slider("value");$("#brush-c2y_val").text($(this).slider("value"));}

    });
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}


function svg_draw() {

    if(SVG_DEBUG && $('#svg').length) {
	document.getElementById('svg').innerHTML = "";
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg'));

    }
}

  
draw();
