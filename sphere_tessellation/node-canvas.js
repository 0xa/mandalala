function tri(sig) {return Math.abs((Math.abs(sig-0.5)*2)-1);}
function trap(sig, amount) {
    var val = Math.pow(Math.min(tri(sig)*amount,1) ,2);
    return val;
}

var sys = require('sys')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { sys.puts(stderr) }

require('node-import');
imports('./tessellation.js');
var http=require('http');

if(typeof process.argv[2] != 'undefined') {
    var canvas_w = parseInt(process.argv[2]);
} else {
    var canvas_w = 600;
}

//var frame_w = canvas_w*(1280/720);
//var frame_w = canvas_w*2.39;
var frame_w = canvas_w;
var frame_h = canvas_w;

var MAP_3D = 1;
var SPHERE_SIZE = 0.9;

var Canvas = require('canvas')
, Image = Canvas.Image
, canvas = new Canvas(frame_w, frame_h)
, context = canvas.getContext('2d');


var ver_count = 0;
function hex_2D(vertices) {
    var canvas_w_2d = frame_w/1;
    var canvas_h_2d = frame_h/1;
    
    context.save();
    for(var i = 0; i<vertices.length;i++) {
	var dat = vertices[i];
	var what = dat["WHAT"];
	var vert2D = dat["VERTEX_2D"];
	var vert3D = dat["VERTEX_3D"];;
	var col = dat["COL"];
	
    	context.beginPath();

	if(what === "DOT") {
	    context.arc((canvas_w_2d*vert2D[0][0])*1, 
	  		(canvas_h_2d*vert2D[0][1])*1, 
 	 		1, 0, 2 * Math.PI, false);
	}

	if(what === "LINE") {
	    context.moveTo((canvas_w_2d*vert2D[0][0]), (canvas_h_2d*vert2D[0][1]));
	    context.lineTo((canvas_w_2d*vert2D[1][0]), (canvas_h_2d*vert2D[1][1]));
	}
	if(what === "FACE") {
	    context.moveTo((canvas_w_2d*vert2D[0][0]), (canvas_h_2d*vert2D[0][1]));
	    context.lineTo((canvas_w_2d*vert2D[1][0]), (canvas_h_2d*vert2D[1][1]));
	    context.lineTo((canvas_w_2d*vert2D[2][0]), (canvas_h_2d*vert2D[2][1]));
	}
	
	context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.fill();
	context.lineWidth = 1;
	context.strokeStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.closePath();
	context.stroke();
	ver_count++;
    }
    
    context.restore();
}

function hex_3D(vertices) {
    var canvas_w_3d = frame_w/2;
    var canvas_h_3d = frame_h/2;
    var radius = RADIUS;
    for(var i = 0; i< vertices.length; i++) {
	context.save();
	var dat = vertices[i];
	var what = dat["WHAT"];
	var vert3D = dat["VERTEX_3D"];
	var y_phasor = Math.abs((Math.abs((dat["COORDS"][0][1]/1)-0.5)*2)-1);
	var z_phasor = Math.abs(vert3D[0][2])+0.1;
	var size = dat["SIZE"];
	var col = dat["COL"];

	if(col == 255) {col = SIGS.COMP_BG_PHASOR;}
	
    	context.beginPath();

	if(what === "DOT") {
	    context.arc((canvas_w_3d+vert3D[0][0]*RADIUS)*1, 
	  		(canvas_h_3d+vert3D[0][1]*RADIUS)*1, 
 	 		(radius*size)*y_phasor*z_phasor, 0, 2 * Math.PI, false);
	    context.lineWidth = 0.1
	}

	if(what === "LINE") {
	    context.moveTo((canvas_w_3d+vert3D[0][0]*RADIUS), (canvas_h_3d+vert3D[0][1]*RADIUS));
	    context.lineTo((canvas_w_3d+vert3D[1][0]*RADIUS), (canvas_h_3d+vert3D[1][1]*RADIUS));
	    context.lineWidth =(radius*size)*y_phasor*z_phasor;
	}


	if(what === "FACE") {
	    context.moveTo((canvas_w_3d+vert3D[0][0]*RADIUS), (canvas_h_3d+vert3D[0][1]*RADIUS));
	    context.lineTo((canvas_w_3d+vert3D[1][0]*RADIUS), (canvas_h_3d+vert3D[1][1]*RADIUS));
	    context.lineTo((canvas_w_3d+vert3D[2][0]*RADIUS), (canvas_h_3d+vert3D[2][1]*RADIUS));
	    context.lineWidth = 0;
	}

	context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.fill();

	context.lineCap="round";
	context.strokeStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.stroke();
	ver_count++;
	context.restore();
    }

}

// t: current time, b: begInnIng value, c: change In value, d: duration
function easeInOutBack (t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
}

function easeInOutBack2 (t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
    s = 1;
    return c/2*((t-=2)*t*(((s*=(1))+1)*t + s) + 2) + b;
}


function easeInBack(t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    return c*(t/=d)*t*((s+1)*t - s) + b;
}

function easeOutBack (t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
}

function easeInOutQuad (t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t + b;
    return -c/2 * ((--t)*(t-2) - 1) + b;
}

function easeOutQuad (t, b, c, d) {
    return -c *(t/=d)*(t-2) + b;
}

function easeInOutSine (t, b, c, d) {
    return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
}

function easeOutExpo (t, b, c, d) {
    return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
}

function easeInOutExpo(t, b, c, d) {
    if (t==0) return b;
    if (t==d) return b+c;
    if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
    return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
}

function draw(rotx, roty, rotz, SIGS){
    if(DATA_ONLY==1) return;
    context.clearRect(0, 0, frame_w, frame_h);

    context.beginPath();
    context.rect(0, 0, frame_w, frame_h);
    var col = SIGS.COMP_BG_PHASOR;
    context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.closePath();
    context.stroke();

    ver_count = 0;
    var vertices = tess_calc(rotx,roty, rotz, SIGS);
    if(MAP_3D) {hex_3D(vertices);} else {hex_2D(vertices);}
}

var FPS = 30;
var BPM = 120;
var BEATS_PER_BAR = 4;
//var BAR_NUM = 16;
var BAR_NUM = 32;
var BEAT_FRAMES = parseInt(60/BPM*FPS);
var BAR_FRAMES = BEAT_FRAMES*BEATS_PER_BAR;
var frames =BAR_FRAMES*BAR_NUM;
var render_frames = frames;
//var render_frames = BEAT_FRAMES*4;


var BAR_INC = 1.0/BAR_FRAMES;
var BAR_PHASOR = 0;
var BAR_PHASOR_SNAP = 0;

var TWO_BAR_PHASOR = 0;
var TWO_BAR_PHASOR_SNAP = 0;


var SIGS = {
    "PHASOR_TOP":0,
    "PHASOR_BAR":0,
    "PHASOR_BEAT":0,
    "COUNT_BEAT":0,
    "COUNT_BAR":0,
    "COMP_WAB_PHASOR":0,
    "COMP_WAB_TRAP_PHASOR":0,
    "COMP_PAT_SHIFT_PHASOR":0,
    "SIGS.COMP_BG_PHASOR":1,
    "SIGS.COMP_RSCALE":2,
    "SIGS.FRAME_W":0,
    "SIGS.FRAME_H":0
}

var render_frames = 1;
var RENDER_VIDEO = 0;
var DATA_ONLY = 0;
var DATA_DUMP = FPS+" "+BPM+" "+render_frames+";\n";
//var f_count = 0;
var f_count_start = 0;
var render_bar_offset = 0;
var f_count_start = BAR_FRAMES*render_bar_offset;
var f_count = BAR_FRAMES*render_bar_offset;

//var render_frames = BAR_FRAMES*render_bar_offset+1;

var idle = 1;
var g_rot_x = 0.325;
//var g_rot_x = 0;

var g_rot_y = 0.5;
//var g_rot_y = 0;
var g_rot_z = 0.5;

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
var fs = require('fs');

var back_amount = 2;
var expand_size = 0.1;

function render() {
    if(!idle) return;
    set_idle(0);    
    var phasor = (f_count/(frames-0))*1;
    var fname = "/bitmaps/"+pad(f_count-f_count_start, 5)+".png";

    var beat_phasor = (phasor*BEATS_PER_BAR*BAR_NUM)%1.0;
    var bar_phasor = (phasor*BAR_NUM)%1.0;
    var two_bar_phasor = (phasor*(BAR_NUM*0.5))%1.0;
    var four_bar_phasor = (phasor*(BAR_NUM*0.25))%1.0;
    var eight_bar_phasor = (phasor*(BAR_NUM*0.125))%1.0;
    var beat_count = Math.floor(phasor*BAR_NUM*BEATS_PER_BAR);
    var bar_count = Math.floor(phasor*BAR_NUM);
    SIGS.PHASOR_TOP = phasor;
    SIGS.PHASOR_BAR = bar_phasor;
    SIGS.PHASOR_BEAT = (bar_phasor*BEATS_PER_BAR)%1.0;
    SIGS.COUNT_BEAT = beat_count;
    SIGS.COUNT_BAR = bar_count;

    SIGS.COMP_RSCALE = 1;
    SIGS.FRAME_W = frame_w/frame_h;
    SIGS.FRAME_H = frame_h/frame_h;

    if(bar_count < 8) {

	SIGS.COMP_PAT_SHIFT_PHASOR = 0;
	SIGS.COMP_WAB_AMP = 1/24.0;
	SIGS.COMP_WAB_FREQ = 9;
	SIGS.COMP_WAB_TRAP_PHASOR =2;
	var BG_BREAK = 0.25;

	if(bar_count%8 < 4) {
	    SIGS.COMP_WAB_PHASOR =(bar_phasor*3)%1.0;
	    var tmp_bg = bar_phasor;
	    var bg_step = (((bar_count%4)/3)*0.75)+0.25;
	    var rot_snap = (bar_count/(BAR_NUM-1))*1;
	    
	    if(bar_count%4==0) {
		SIGS.COMP_WAB_FREQ = 9;
		 rot_snap = 0;
	    } else if(bar_count%4==1) {
		SIGS.COMP_WAB_FREQ = 3;
		rot_snap = 0.25;
	    } else if(bar_count%4==2) {
		SIGS.COMP_WAB_FREQ = 4;
		rot_snap = -0.15;
	    } else {
		SIGS.COMP_WAB_FREQ = 9;
		rot_snap = 1;
	    }
	    
	} else {
	    var tmp_bg = two_bar_phasor;
	    var bg_step = 1;
	    if(bar_count < 6) {var rot_snap = 0.325;} else {var rot_snap = 0;SIGS.COMP_WAB_FREQ = 3}

	    SIGS.COMP_WAB_AMP = 1/24.0;
	    SIGS.COMP_WAB_PHASOR = (SIGS.COMP_WAB_PHASOR+(
		(BAR_INC*1.5)*((easeInOutQuad(tri(two_bar_phasor),0,1,1)*2)+0.5)
	    ))%1.0;
	}

	if(bar_count == 7) {SIGS.COMP_WAB_TRAP_PHASOR = 2;}
	
	if(tmp_bg < BG_BREAK) {
		var bg_phasor = easeInOutExpo(tmp_bg/BG_BREAK,0,1,1);
	} else {
	    var bg_phasor = easeInOutQuad(Math.abs(((tmp_bg-BG_BREAK)/(1-BG_BREAK))-1),0,1,1);
	}

	SIGS.COMP_BG_PHASOR = parseInt(bg_phasor*255* bg_step);

	SIGS.COMP_WAB_AMP = 0;
	SIGS.COMP_WAB_PHASOR = 0;
	SIGS.COMP_BG_PHASOR = 255;	
	
	RADIUS = ((canvas_w/2)*SPHERE_SIZE);
	draw(g_rot_x+rot_snap, g_rot_y, g_rot_z,SIGS);
	
    } else if(bar_count < 16) {

	var rot_snap = 1;
	SIGS.COMP_BG_PHASOR = 255;
	var wab_amp_env = 3;
	if(bar_count%4==0 && BAR_PHASOR_SNAP==0) {BAR_PHASOR = 0; BAR_PHASOR_SNAP=1;}
	if(bar_count%4==1 && BAR_PHASOR_SNAP==1) {BAR_PHASOR_SNAP=0;}

	if(bar_count%4 < 2 ) {
    	    SIGS.COMP_WAB_PHASOR = easeOutExpo(BAR_PHASOR, 0, 1, 1);
	    
    	    if(bar_count%2==0){
		SIGS.COMP_WAB_AMP = (1/8.0)*trap(bar_phasor, wab_amp_env);
	    } else {
		SIGS.COMP_WAB_AMP = 1/24.0
	    };
    	    if(bar_count%2==0){
		if(bar_count%8<4) {SIGS.COMP_WAB_FREQ = 3;} else {SIGS.COMP_WAB_FREQ = 3;}
	    } else {SIGS.COMP_WAB_FREQ = 9};
	} else {
	    var fade3 =Math.min(Math.abs(two_bar_phasor-1)*6,1);
    	    SIGS.COMP_WAB_PHASOR = easeOutQuad(BAR_PHASOR, 0, 1, 1)*fade3;
    	    if(bar_count%8<4) {
		SIGS.COMP_WAB_AMP = 1/6.0;
	    } else {
		SIGS.COMP_WAB_AMP = 1/2.0;
	    }
    	    if(bar_count%8<4) {SIGS.COMP_WAB_FREQ = 6;} else {SIGS.COMP_WAB_FREQ = 3;}
	}
	SIGS.COMP_WAB_TRAP_PHASOR = SIGS.COMP_WAB_PHASOR;
	
	SIGS.COMP_PAT_SHIFT_PHASOR = easeInOutBack(two_bar_phasor, 0, 1, 1, 1.5);
	if(bar_count%2==1) {
	    back_amount = 6;
	    if(bar_count%4==3){expand_size = canvas_w/8;} else{expand_size = canvas_w/16;}
	    
	    if(Math.floor(bar_phasor*2) == 0) {
		var ease= easeInOutBack2((bar_phasor*2)%1.0, 0, 1, 1, back_amount);
	    } else {
		var ease = easeInOutQuad(Math.abs(((bar_phasor*2)%1.0)-1), 0, 1, 1); 
	    }
	    RADIUS = ((canvas_w/2)*SPHERE_SIZE)+(ease*expand_size);
	} else {
	    RADIUS = ((canvas_w/2)*SPHERE_SIZE);
	}
	if(bar_count%8 == 7) {SIGS.COMP_BG_PHASOR = parseInt(Math.min(Math.abs(bar_phasor-1)*1.5,1)*255);}
	if(bar_count%8 == 0) {SIGS.COMP_BG_PHASOR = parseInt(easeInOutQuad(Math.min(bar_phasor*4,1),0,1,1)*255);}
	draw(g_rot_x+(((((phasor*2)%1.0)+rot_snap)%1.0)*2), g_rot_y, g_rot_z, SIGS);
	
    } else {
	SIGS.COMP_WAB_AMP = 0.25;
	SIGS.COMP_WAB_FREQ = 9;
	SIGS.COMP_WAB_PHASOR = bar_phasor;
	SIGS.COMP_BG_PHASOR = 255;
	SIGS.COMP_WAB_TRAP_PHASOR =2;
	
//	if(bar_count%16 == 15) {SIGS.COMP_BG_PHASOR = parseInt(Math.min(Math.abs(bar_phasor-1)*1,1)*255);}
	if(bar_count%16 >= 14) {SIGS.COMP_BG_PHASOR = parseInt(Math.min(
	    easeInOutQuad(Math.abs(two_bar_phasor-1),0,1,1)
	    ,1)*255);}
	if(bar_count%16 == 0) {SIGS.COMP_BG_PHASOR = parseInt(easeInOutQuad(Math.min(bar_phasor*3,1),0,1,1)*255);}
	
	if(bar_count%16<8) {
	    if(bar_count%8<4) {
		scale_sig = easeInOutQuad(four_bar_phasor, 0, 1, 1);
		SIGS.COMP_RSCALE = 6.5-(scale_sig*4);
	    } else {SIGS.COMP_RSCALE = 2.5;}
	} else {
	    var scale_amp = 0.5;
	    if(Math.floor(eight_bar_phasor*4)==1) {var scale_amp = 1}
	    if(Math.floor(eight_bar_phasor*4)==3) {var scale_amp = 2}

	    var scale_phasor = (Math.sin(two_bar_phasor*2*Math.PI)+1)*scale_amp*
	    easeInOutQuad(tri(two_bar_phasor),0,1,1);
	    SIGS.COMP_RSCALE = 2.5+scale_phasor;
	}
	RADIUS = ((canvas_w/2)*SPHERE_SIZE);

	draw(g_rot_x+0.325, g_rot_y, g_rot_z+eight_bar_phasor*2, SIGS);

    }
	
    if(DATA_ONLY==1) {

	DATA_DUMP = DATA_DUMP+(f_count+0)+" "+phasor.toFixed(4)+" "+(SIGS.COMP_BG_PHASOR/255).toFixed(4)+" "+SIGS.COMP_WAB_PHASOR.toFixed(4)+";\n";
	
     	f_count++;
	if(bar_count%4<2) {
	    BAR_PHASOR = (BAR_PHASOR+BAR_INC)%1.0;
	} else {
	    var trap_invert = (Math.pow(Math.abs(trap(two_bar_phasor, 4)-1),2)*0.9)+0.1;
	    var tmp_phasor = trap_invert;
	    BAR_PHASOR = (BAR_PHASOR+(BAR_INC*tmp_phasor))%1.0;
	}
	set_idle(1);    
    } else {
	canvas.toBuffer(function(err, buf){
    	    if (err) throw err;
    	    fs.writeFile(__dirname +fname, buf,function (err,data) {
		if (err) {return console.log(err);}
		console.log(
		    pad(ver_count,7)+' '+(phasor*100).toFixed(1)+'% '+pad(f_count+1,5)+' of '+render_frames+' | '+bar_count+'.'+beat_count%4
		);
     		f_count++;

		if(bar_count%4<2) {
		    BAR_PHASOR = (BAR_PHASOR+BAR_INC)%1.0;
		} else {
		    var trap_invert = (Math.pow(Math.abs(trap(two_bar_phasor, 4)-1),2)*0.9)+0.1;
		    var tmp_phasor = trap_invert;
		    BAR_PHASOR = (BAR_PHASOR+(BAR_INC*tmp_phasor))%1.0;
		}
		set_idle(1);    
	    });
	});
    }
    
}

function set_idle(val) {idle = val;}

var CLEANED = 0;

function keep_trying()
{
    if(f_count < render_frames) {
	if(f_count == f_count_start && !CLEANED && !DATA_ONLY) {
	    var cmd = "rm ./bitmaps/*.png";
	    exec(cmd, puts);
	    CLEANED = 1;
	    console.log("removing old pngs.");
	}
	render();
	setImmediate(keep_trying);
    } else if(f_count == render_frames && RENDER_VIDEO) {
	if(!DATA_ONLY) {
	    var now = new Date();
	    var v_fname = now.getFullYear()+"-"+pad(now.getMonth()+1,2)+"-"+pad(now.getDate(),2)+"-"+pad(now.getHours(),2)+
		"-"+pad(now.getMinutes(),2)
		+"_"+frames+"-"+FPS+".mpg";
	    //-qscale:v 1?
	    var cmd = "yes | ffmpeg -r "+FPS+" -i bitmaps/%5d.png -vb 20M -vf scale=-1:720 "+"render/"+v_fname;
	    exec(cmd, puts);
	} else {
	    console.log(DATA_DUMP);
	    fs.writeFile('data_dump.txt', DATA_DUMP, function (err) {
		if (err)
		    return console.log(err);
	    });
	    

	    
	}
    }

}

keep_trying();



