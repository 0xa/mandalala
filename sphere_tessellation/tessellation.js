//BZR stuff
//http://www.13thparallel.org/archive/bezier-curves/
var CLIP_Z = 1;

function B1(t) { return t*t*t }
function B2(t) { return 3*t*t*(1-t) }
function B3(t) { return 3*t*(1-t)*(1-t) }
function B4(t) { return (1-t)*(1-t)*(1-t) }

function tes_foo() {console.log("foo");}

function xy2iso(x, y) {
    var off = 0;
    var width = 1;
    var height = 1;
    var map_x = (x-y)*(width*1);
    var map_y = (x+y)*(height*1.5);
    return [map_x, map_y];
}


function rotateY(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+0+z*Math.sin(rot);
    var roty = y;
    var rotz = -1*x*Math.sin(rot)+0+z*Math.cos(rot);
    return [rotx, roty, rotz];
}

function rotateX(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x;
    var roty = 0+y*Math.cos(rot)-z*Math.sin(rot);
    var rotz = 0+y*Math.sin(rot)+z*Math.cos(rot);
    return [rotx, roty, rotz];
}

function rotateZ(x,y,z,deg) {
    var rot = 3.14*deg;
    var rotx = x*Math.cos(rot)+y*Math.sin(rot)*-1+0;
    var roty = x*Math.sin(rot)+y*Math.cos(rot)+0;
    var rotz = z;
    return [rotx, roty, rotz];
}


function rotate_2d(x,y,rot) {
    //rotate X
    var rotx = x*Math.cos(rot)-y*Math.sin(rot);
    var roty = x*Math.sin(rot)+y*Math.cos(rot);
    return [rotx, roty];
}

var g_rot_x = 0;
var g_rot_y = 0;
var g_rot_z = 0;

var frame_w  = 1;
var frame_h  = 1;


function to_sphere2(xy, sphere_r) {
    var sphere = xy2sphere(xy[0], xy[1], sphere_r);
    var rot = rotateY(sphere[0],sphere[1],sphere[2],g_rot_y);
    var rot2 = rotateX(rot[0],rot[1],rot[2],g_rot_x);
    var rot3 = rotateZ(rot2[0],rot2[1],rot2[2],g_rot_z);

    var bound_scale = 1.5;
    
    if(rot3[1]<(frame_h*0.5)*-2*bound_scale || rot3[1] > (frame_h*0.5)*2*bound_scale) return [];
    if(rot3[0]<(frame_w*0.5)*-2*bound_scale || rot3[0] > (frame_w*0.5)*2*bound_scale) return [];

    
    //    if (CLIP_Z && rot3[2]<-0.25) return [];
    if (CLIP_Z && rot3[2]<0) return [];
//    if (CLIP_Z && rot3[2]>0) return [];
    var result = [rot3[0],rot3[1],rot3[2]];
    
    return result;
}

function perpen(pts, d, off) {
    var vec = [];
    vec[0] = pts[1][0]-pts[0][0];
    vec[1] = pts[1][1]-pts[0][1];
    var len = Math.sqrt(Math.pow(vec[0],2)+Math.pow(vec[1],2))
    var unit = [];
    unit[0] = vec[0]/len;
    unit[1] = vec[1]/len;
    var per = [];
    per[0] = (-1*vec[1])/len;
    per[1] = (vec[0])/len;
    
    var per_t1 = [];
    if(d == 1) {
	per_t1[0] = pts[1][0]-off*per[0];
	per_t1[1] = pts[1][1]-off*per[1];
    } else {
	per_t1[0] = pts[1][0]+off*per[0];
	per_t1[1] = pts[1][1]+off*per[1];

    }
    return per_t1;
}

function dist(p1, p2) {
    var dist = Math.sqrt(Math.pow(p2[0]-p1[0],2)+Math.pow(p2[1]-p1[1],2));
    return dist;
}

function xy2sphere(x,y,sphere_r) {
    var R = 1;
    var longitude = x / R;
    var latitude = 2 * Math.atan(Math.exp(y/R)) - Math.PI/2;

//    var R2 =1;
    var R2 = sphere_r;

    var sx = R2 * Math.cos(latitude) * Math.cos(longitude);
    var sy = R2 * Math.cos(latitude) * Math.sin(longitude);
    var sz = R2 * Math.sin(latitude);
    
    return [sx, sy, sz];
    
}

function lattice() {
    var map_data = [];
    var normal_w = 1;
    var unit = lattice_struct["unit"];
    var center_x = lattice_struct["center_x"];
    var center_y = lattice_struct["center_y"];
    var side_len = lattice_struct["side_len"];
    var poly_radius = lattice_struct["poly_radius"];
    var off_scale = lattice_struct["off_scale"];
    var c_off_scale = lattice_struct["c_off_scale"];
    var segs = lattice_struct["segs"];
    var x_flip = lattice_struct["x_flip"];
    var y_flip = lattice_struct["y_flip"];
    var sphere_r = lattice_struct["sphere_r"];
    
    for(var i = 0; i<unit.length;i++) {
	var line_x = [];
	var line_y = [];

	var coords = [];
	var verts3D = [];
	var verts2D = [];

	var points = unit[i]["COORDS"];

	for(var n=0;n<points.length;n++) {
	    var point = points[n];
	    if(segs == 3) {
		line_x.push(point[0]*off_scale);
		line_y.push(point[1]*off_scale);
	    } else if(segs == 6) {
		line_x.push(point[0]*off_scale);
		line_y.push(point[1]*c_off_scale);
	    } else if(segs == 4) {
		line_x.push(point[0]*off_scale);
		line_y.push(point[1]*off_scale);
	    }
	    
	}
	which = unit[i]["WHICH"];
	var has_clip = 0;
	for(var n=0;n<points.length;n++) {
	    if(which) {
		var x_coord = center_x+line_x[n]+side_len*0.5*off_scale;
		var y_off = poly_radius - Math.sqrt( Math.pow(poly_radius,2)-Math.pow(side_len*0.5,2));
		var y_coord = center_y+line_y[n]+y_off*off_scale;
	    } else {
		var x_coord = center_x+line_x[n]*x_flip;
		var y_coord = center_y+line_y[n]*y_flip;
	    }

	    var vert3D = to_sphere2([x_coord*((1*2*Math.PI)/normal_w),
	 			     (y_coord-(normal_w/2))*((1*2*Math.PI)/normal_w)],
				   sphere_r);
	    if(!vert3D.length) has_clip = 1;
	    coords.push([x_coord, y_coord]);
	    verts2D.push([x_coord*off_scale, y_coord*off_scale]);
	    verts3D.push(vert3D);
	}

//	if(!verts3D[0].length) continue;

	if(has_clip) continue;
	
//	if(!verts3D[verts3D.length] === "undefined") continue;
	
	map_data.push({COORDS:coords,
		       SIZE:unit[i]["SIZE"]*(poly_radius*0.1), 
		       VERTEX_3D:verts3D,
		       VERTEX_2D:verts2D,
		       WHAT:unit[i]["WHAT"],
		       COL:unit[i]["COL"]});

    }
    return map_data;
}


function make_patterns(poly, tri, poly_radius, side_len) {
    var patterns = [[],[],[],[]];
    var LINE_SIZE = 6;
    var line_res =3;
	for(var i = 0; i<poly.length;i++) {
	    var x = poly[i][0];
	    var y = poly[i][1];
	    if(i == (poly.length-1)) {var next = 0;} else {var next = i+1;}
	    var x1 = poly[next][0];
	    var y1 = poly[next][1];

	    for(var n = 0; n < line_res; n++) {
		var phase = n/(line_res);
		var next_n = Math.min(n+1,line_res-1);
		var next_phase = next_n/line_res;
		
		var line_x = ((x1-x)*(phase))+x;
		var line_y = ((y1-y)*(phase))+y;
		var next_line_x = ((x1-x)*(next_phase))+x;
		var next_line_y = ((y1-y)*(next_phase))+y;
		
		patterns[0].push({COORDS:[[line_x,line_y]],SIZE:2,WHICH:tri,WHAT:"DOT",COL:0});
	    }


	    var scale2 = 1;
	    var gap =0.125*(1/64); var seg_len = 1;
	    var line_res3 = 2;

	    for(var n = 0; n < line_res3; n++) {
		if(i!= 0 ) continue;
		var flip = -1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);

		var xoff_flip = i<2?-1:1;
		var yoff_flip = i<2?1:-1;
		var off_amount = 0.5;
		var iso_off = xy2iso(side_len*off_amount*xoff_flip, side_len*0);

		var xoff = iso_off[0];
		var yoff = iso_off[1];
		
	    	var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	patterns[1].push({COORDS:[
		    [(line_x+xoff)*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
		    [(next_line_x+xoff)*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]
		], SIZE:LINE_SIZE,WHICH:tri,WHAT:"DOT",COL:255});
	    }


	    
	    var scale2 = 1;
	    var gap =0.75; var seg_len = 1;
	    var line_res3 = 10;

	    for(var n = 0; n < line_res3; n++) {
		var flip = -1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);

//		if(phase < 0.5) continue;
		var xoff_flip = i<2?-1:1;
		var yoff_flip = i<2?1:-1;
		//		var off_amount = 0.25;
		var off_amount = 0;
		if(i%2 == 0) {
		    var iso_off = xy2iso(side_len*off_amount*xoff_flip, side_len*0);
		} else {
		    var iso_off = xy2iso(side_len*0, side_len*off_amount*yoff_flip);
		}

		var xoff = iso_off[0];
		var yoff = iso_off[1];
		
	    	var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	patterns[1].push({COORDS:[
		    [(line_x+xoff)*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
		    [(next_line_x+xoff)*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]
		], SIZE:LINE_SIZE,WHICH:tri,WHAT:"LINE",COL:255});
	    }

	    var nest_level = 4;
	    for(var nest=0; nest<nest_level; nest++) {
		if(nest == 2) continue;
		var scale2 = 1;
		var gap =(1/nest_level/2)*(nest+1)*1;
		var seg_len = 1;
		var line_res3 = 5;

		for(var n = 0; n < line_res3; n++) {
		    var flip = -1;
	    	    var phase = n/(line_res3-1);
	    	    var next_n = Math.min(n+1,line_res3-1);
	    	    var next_phase = next_n/(line_res3-1);
		    var off_amount1 = 0.5;
		    if(i==0) {
			var off_amount = 1*off_amount1;
			var iso_off = xy2iso(side_len*off_amount, side_len*0);
		    } else if(i==2) {
			var off_amount = -1*off_amount1;
			var iso_off = xy2iso(side_len*off_amount, side_len*0);
		    } else {continue;}
		    var xoff = iso_off[0];
		    var yoff = iso_off[1];
		    
	    	    var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	    var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	    var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	    var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	    patterns[1].push({COORDS:[
		    	[(line_x+xoff)*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
		    	[(next_line_x+xoff)*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]
		    ], SIZE:LINE_SIZE,WHICH:tri,WHAT:"LINE",COL:255});
		}
	    }

	    var nest_level = 4;
	    for(var nest=0; nest<nest_level; nest++) {
		if(nest >=2) continue;
		var scale2 = 1;
		var gap =(1/nest_level/2)*(nest+1)*1;
		var seg_len = 1;
		var line_res3 = 5;

		for(var n = 0; n < line_res3; n++) {
		    var flip = -1;
	    	    var phase = n/(line_res3-1);
	    	    var next_n = Math.min(n+1,line_res3-1);
	    	    var next_phase = next_n/(line_res3-1);
		    var off_amount1 = 0.5;
		    if(i==0) {
		    	var off_amount = 1*off_amount1;
		    	var iso_off = xy2iso(side_len*off_amount, side_len*0);
		    } else if(i==2) {
		    	var off_amount = -1*off_amount1;
		    	var iso_off = xy2iso(side_len*off_amount, side_len*0);
		    } else if(i==3) {
			var off_amount = 1*off_amount1;
		    	var iso_off = xy2iso(side_len*0,side_len*off_amount );
		    }
		    else{
			var off_amount = -1*off_amount1;
		    	var iso_off = xy2iso(side_len*0,side_len*off_amount );
		    }
//		    var iso_off = [0,0];
		    var xoff = iso_off[0];
		    var yoff = iso_off[1];
		    
	    	    var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	    var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	    var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	    var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	    patterns[3].push({COORDS:[
		    	[(line_x+xoff)*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
		    	[(next_line_x+xoff)*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]
		    ], SIZE:LINE_SIZE,WHICH:tri,WHAT:"LINE",COL:255});
		}
	    }

	    var scale2 = 1;
	    var gap =0.75; var seg_len = 1;
	    var line_res3 = 10;

	    for(var n = 0; n < line_res3; n++) {
		var flip = -1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);

//		if(phase < 0.5) continue;
		var xoff_flip = i<2?-1:1;
		var yoff_flip = i<2?1:-1;
		//		var off_amount = 0.25;
		var off_amount = 0;
		if(i%2 == 0) {
		    var iso_off = xy2iso(side_len*off_amount*xoff_flip, side_len*0);
		} else {
		    var iso_off = xy2iso(side_len*0, side_len*off_amount*yoff_flip);
		}

		var xoff = iso_off[0];
		var yoff = iso_off[1];
		
	    	var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	patterns[3].push({COORDS:[
		    [(line_x+xoff)*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
		    [(next_line_x+xoff)*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]
		], SIZE:LINE_SIZE,WHICH:tri,WHAT:"LINE",COL:255});
	    }

	    
	    
	    var scale2 = 1;
	    var gap =1.6; var seg_len = 1;
//	    var gap =1.25; var seg_len = 1;
	    var line_res3 = 10;
	    for(var n = 0; n < line_res3; n++) {
		var flip = 1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);
		
		var yoff = 0;
	    	var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	patterns[2].push({COORDS:[
		    [line_x*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
		    [next_line_x*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip],
		    [0,0]
		],
				  SIZE:4,WHICH:tri,WHAT:"FACE",COL:0});
	    }


	}
	// ends here...
    return patterns;
}


function make_patterns_hex(poly, tri, poly_radius, side_len) {
    var patterns = [[],[],[],[]];
    var line_res =5;
	for(var i = 0; i<poly.length;i++) {
	    var x = poly[i][0];
	    var y = poly[i][1];
	    if(i == (poly.length-1)) {var next = 0;} else {var next = i+1;}
	    var x1 = poly[next][0];
	    var y1 = poly[next][1];

	    for(var n = 0; n < line_res; n++) {
		var phase = n/(line_res);
		var next_n = Math.min(n+1,line_res-1);
		var next_phase = next_n/line_res;
		
		var line_x = ((x1-x)*(phase))+x;
		var line_y = ((y1-y)*(phase))+y;
		var next_line_x = ((x1-x)*(next_phase))+x;
		var next_line_y = ((y1-y)*(next_phase))+y;
		
		patterns[0].push({COORDS:[[line_x,line_y]],SIZE:1,WHICH:tri,WHAT:"DOT",COL:0});
	    }

	    var scale2 = 1+0.125;
	    var gap = 0.75; var seg_len = 1;
	    var line_res3 = 10;
	    for(var n = 0; n < line_res3; n++) {
		var flip = 1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);
		
	    	if(i==2 && phase > 0.75) continue;
		if(i==3 && phase < 0.2) continue;
		
		var yoff = 0;
	    	var line_x = ((x1-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	var line_y = ((y1-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	var next_line_x = ((x1-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	var next_line_y = ((y1-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	patterns[1].push({COORDS:[[line_x*gap*scale2*flip,(line_y+yoff)*gap*scale2*flip],
					  [next_line_x*gap*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]],SIZE:6,WHICH:tri,WHAT:"LINE",COL:0});
	    }



	    var gap = 0.5; var seg_len = 1;
	    var line_res3 = 5;
	    for(var n = 0; n < line_res3; n++) {
		var flip = 1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);

		var n_x1 = x1; var n_y1 = y1;
		var n_x = x; var n_y = y;

		var yoff = 0;
		var xoff = 0;
	    	var line_x = ((n_x1-n_x)* ((phase*seg_len) + (1-seg_len)/2)) + n_x;
	    	var line_y = ((n_y1-n_y)* ((phase*seg_len) + (1-seg_len)/2)) + n_y;
	    	var next_line_x = ((n_x1-n_x)* ((next_phase*seg_len) + (1-seg_len)/2)) + n_x;
	    	var next_line_y = ((n_y1-n_y)* ((next_phase*seg_len) + (1-seg_len)/2)) + n_y;

	    	patterns[1].push({COORDS:[[(line_x*gap+xoff)*scale2*flip,(line_y+yoff)*gap*scale2*flip],
					  [(next_line_x*gap+xoff)*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]],SIZE:6,WHICH:tri,WHAT:"LINE",COL:0});
	    }

	    var gap = 0.25; var seg_len = 1;
	    var line_res3 = 5;
	    for(var n = 0; n < line_res3; n++) {
		var flip = 1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);

		var n_x1 = x1; var n_y1 = y1;
		var n_x = x; var n_y = y;
		
		var yoff = 0;
		var xoff = 0;
	    	var line_x = ((n_x1-n_x)* ((phase*seg_len) + (1-seg_len)/2)) + n_x;
	    	var line_y = ((n_y1-n_y)* ((phase*seg_len) + (1-seg_len)/2)) + n_y;
	    	var next_line_x = ((n_x1-n_x)* ((next_phase*seg_len) + (1-seg_len)/2)) + n_x;
	    	var next_line_y = ((n_y1-n_y)* ((next_phase*seg_len) + (1-seg_len)/2)) + n_y;

	    	patterns[1].push({COORDS:[[(line_x*gap+xoff)*scale2*flip,(line_y+yoff)*gap*scale2*flip],
					  [(next_line_x*gap+xoff)*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]],SIZE:6,WHICH:tri,WHAT:"LINE",COL:0});
	    }



	    var gap = 0.5; var seg_len = 1;
	    var line_res3 = 20;
	    for(var n = 0; n < line_res3; n++) {
		var flip = 1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);

		if(i==0 || i==4 || i==2) {if(phase > 0.5) continue;} else {if(phase < 0.5) continue;}
		
		var n_x = x;
		var n_y = y;
		var n_x1 = 0;
		var n_y1 = 0;
		
		var yoff = 0;
		var xoff = 0;
	    	var line_x = ((n_x1-n_x)* ((phase*seg_len) + (1-seg_len)/2)) + n_x;
	    	var line_y = ((n_y1-n_y)* ((phase*seg_len) + (1-seg_len)/2)) + n_y;
	    	var next_line_x = ((n_x1-n_x)* ((next_phase*seg_len) + (1-seg_len)/2)) + n_x;
	    	var next_line_y = ((n_y1-n_y)* ((next_phase*seg_len) + (1-seg_len)/2)) + n_y;

	    	patterns[1].push({COORDS:[[(line_x*gap+xoff)*scale2*flip,(line_y+yoff)*gap*scale2*flip],
					  [(next_line_x*gap+xoff)*scale2*flip,(next_line_y+yoff)*gap*scale2*flip]],SIZE:6,WHICH:tri,WHAT:"LINE",COL:0});
	    }
	    
	    var gap = 0.25; var seg_len = 1;
	    var line_res3 = 3;
	    for(var n = 0; n < line_res3; n++) {
		var flip = 1;
	    	var phase = n/(line_res3-1);
	    	var next_n = Math.min(n+1,line_res3-1);
	    	var next_phase = next_n/(line_res3-1);
		var scale = 1;
		if(i != 0 && i !=3) continue;
		var yoff = 0;
		
	    	var line_x = ((0-x)* ((phase*seg_len) + (1-seg_len)/2)) + x;
	    	var line_y = ((0-y)* ((phase*seg_len) + (1-seg_len)/2)) + y;
	    	var next_line_x = ((0-x)* ((next_phase*seg_len) + (1-seg_len)/2)) + x;
	    	var next_line_y = ((0-y)* ((next_phase*seg_len) + (1-seg_len)/2)) + y;

	    	patterns[2].push({COORDS:[[line_x*gap*scale*flip,(line_y+yoff)*gap*scale*flip],
					  [next_line_x*gap*scale*flip,(next_line_y+yoff)*gap*scale*flip]],SIZE:6,WHICH:tri,WHAT:"LINE",COL:0});
	    }

	    var spirals = [0.75, 0.5, 0.25];
	    var connects = [1.75, 2,3];
	    
	    for(var s=0; s<spirals.length;s++) {
		var mod = s%2;
		var flip = mod?1:-1;
		var gap = spirals[s]; 
		var seg_len = 1;
		var line_res2 =10;
		var scale = 1.125;
		var n_x = x;
		var n_y = y;

		if(i%3==1) {n_y = y*connects[s];}
		
		for(var n = 0; n < line_res2; n++) {
	    	    var phase = n/(line_res2-1);
	    	    var next_n = Math.min(n+1,line_res2-1);
	    	    var next_phase = next_n/(line_res2-1);
		    var cut = 0; 
		    var cut_max = 1;
		    if(i%3 == 0 && s == spirals.length-1) continue;
		    if(i%3 == 0 && s < spirals.length-1) {cut_max = gap-0.125;}
		    if(i%3 == 1) {seg_len = 1;cut_max= 1;}

	    	    if(phase < cut  || phase > cut_max) {
	    	    } else {
	    		var line_x = ((x1-n_x)* ( (phase*seg_len) + (1-seg_len)/2 )  ) + n_x;
	    		var line_y = ((y1-n_y)* ( (phase*seg_len) + (1-seg_len)/2 )  ) + n_y;
	    		var next_line_x = ((x1-n_x)* ( (next_phase*seg_len) + (1-seg_len)/2 )  ) + n_x;
	    		var next_line_y = ((y1-n_y)* ( (next_phase*seg_len) + (1-seg_len)/2 )  ) + n_y;
	    		patterns[2].push({COORDS:[[line_x*gap*flip*scale,(line_y)*gap*flip*scale],
						  [next_line_x*gap*flip*scale,(next_line_y)*gap*flip*scale]],
					  SIZE:6,WHICH:tri,WHAT:"LINE",COL:0});
		    }		    
		}
		
	    }
	    
	    
	    var lnum = 6;
	    var lrad = 0.125;
	    for(var n = 0; n < lnum; n++) {
	    	var linc = n/lnum;
	    	var next_n = Math.min(n+1,lnum-1);
	    	var next_linc = next_n/(lnum);
		
	    	var line_x = Math.sin(linc*Math.PI*2)*poly_radius*lrad;
	    	var line_y = (Math.cos(linc*Math.PI*2)+0)*poly_radius*lrad;
	    	var next_line_x = Math.sin(linc*Math.PI*2)*poly_radius*lrad;
	    	var next_line_y = (Math.cos(linc*Math.PI*2)+0)*poly_radius*lrad;
	    	patterns[3].push({COORDS:[[line_x,line_y]],SIZE:0.75,WHICH:tri,WHAT:"DOT",COL:0});

	    }
	    
	}
	// ends here...
    return patterns;
}

var lattice_struct = {};

function tri(sig) {
    return Math.abs((Math.abs(sig-0.5)*2)-1);
}

function trap(sig, amount) {
    var val = Math.pow(Math.min(tri(sig)*amount,1) ,2);
    return val;
}


function wabble2(static_phasor, moving_phasor, wab_freq, wab_exp, sin_cos,wab_amp, wab_trap_phasor) {
//    wab_freq = Math.floor(moving_phasor*4)+1;
    if(sin_cos == 1) {
	return  1+((Math.pow( Math.sin(
	    ((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0*Math.PI*2
	),wab_exp) *wab_amp));
    } else {
	return  1+((Math.pow( Math.cos(
	    ((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0*Math.PI*2
	),wab_exp) *wab_amp));
    }

}

function wabble3(static_phasor, moving_phasor, wab_freq, wab_exp, sin_cos,wab_amp, wab_trap_phasor) {
    if(sin_cos == 1) {
	return  1+(
	    (Math.pow( 
		Math.sin(((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0*Math.PI*2)
		,wab_exp)
	     * wab_amp) * trap(wab_trap_phasor,1.75) );
    } else {
	return  1+((Math.pow( Math.cos(
	    ((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0*Math.PI*2
	),wab_exp) *wab_amp) * trap(wab_trap_phasor,1.75) );
    }

}

function wabble(static_phasor, moving_phasor, wab_freq,rscale, sin_cos,wab_amp, wab_trap_phasor) {
    if(sin_cos == 1) {
	return  (1*rscale)+
	    ((Math.sin(((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0*Math.PI*2)*wab_amp) 
	     *trap(wab_trap_phasor,1.75));
    } else {
	return  (1*rscale)+
	    ((Math.cos(((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0*Math.PI*2)*wab_amp) 
	     *trap(wab_trap_phasor,1.75));
    }

}


function wabble_old(static_phasor, moving_phasor, wab_freq, wab_exp, sin_cos,wab_amp, wab_trap_phasor) {
//    wab_freq = Math.floor(moving_phasor*4)+1;
    if(sin_cos == 1) {
	return  1+((Math.pow( Math.sin(
	    ((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0   *Math.PI*2
	),wab_exp) *wab_amp) * trap(moving_phasor,1.75) );
    } else {
	return  1+((Math.pow( Math.cos(
	    ((((static_phasor)*wab_freq)%1.0)+((moving_phasor*1)%1.0))%1.0   *Math.PI*2
	),wab_exp) *wab_amp) * trap(moving_phasor,1.75) );
    }

}



//function tess_calc(rotx, roty, rotz, phasor,phasor2) {
function tess_calc(rotx, roty, rotz, SIGS) {
    // global rotation
    g_rot_x = rotx;
    g_rot_y = roty;
    g_rot_z = rotz;

    var wab_phasor = 0;
    var wab_trap_phasor = 2;
    var pat_shift_phasor = 0;
    var wab_amp = 0;
    var wab_freq = 0;
    var rscale  = 1;

    
    if(SIGS.COMP_PAT_SHIFT_PHASOR){pat_shift_phasor = SIGS.COMP_PAT_SHIFT_PHASOR;}
    if(SIGS.COMP_WAB_PHASOR){wab_phasor = SIGS.COMP_WAB_PHASOR;}
    if(SIGS.COMP_WAB_TRAP_PHASOR){wab_trap_phasor = SIGS.COMP_WAB_TRAP_PHASOR;}
    if(SIGS.COMP_WAB_AMP){wab_amp = SIGS.COMP_WAB_AMP;}
    if(SIGS.COMP_WAB_FREQ){wab_freq = SIGS.COMP_WAB_FREQ;}
    if(SIGS.COMP_RSCALE){rscale = SIGS.COMP_RSCALE;}
    if(SIGS.FRAME_W){frame_w = SIGS.FRAME_W;}
    if(SIGS.FRAME_H){frame_h = SIGS.FRAME_H;}

//    wab_phasor = 0.5; wab_amp = 1/1.25; wab_freq = 3;
    
//    var wab_exp = 1;

    
    var vertices = [];
    var vertices_js = [];

    var normal_w = 1;
    var canvas_w = normal_w/2;
    //42,24,63,25,86,106,172
    var grid_resolution= 63;
    var poly_radius = canvas_w/grid_resolution;

    var segs = 4;
    var repeats = (segs == 3)?2:1;

    for(var tri = 0; tri<repeats; tri++) {
	var poly = [];
	for(var i = 0; i<segs; i++) {
	    if(tri) {
		var pinc = (i/segs+0.5)%1.0;
	    } else {
		if(segs == 4) {var pinc = (i/segs+0.125)%1.0;} else {var pinc = (i/segs+0)%1.0;}
	    }
	    var x = Math.sin(pinc*Math.PI*2)*poly_radius;
	    var y = Math.cos(pinc*Math.PI*2)*poly_radius;

	    poly.push([x, y]);
	}

	if(segs !== 4) {
	    var side_len = dist([poly[0][0],poly[0][1]],[poly[1][0],poly[1][1]]);
	    var hex_off = Math.sqrt(Math.pow(poly_radius,2)-Math.pow(side_len*0.5,2));
	} else {
	    var side_len = dist(poly[0],poly[1]);
	    var iso_p1 = xy2iso(poly[0][0], poly[0][1]);
	    var iso_p2 = xy2iso(poly[1][0], poly[1][1]);
	    var iso_p3 = xy2iso(poly[2][0], poly[2][1]);
	    var iso_p4 = xy2iso(poly[3][0], poly[3][1]);
	    var diag_len = dist(iso_p1,iso_p2);
	    var diag_len2 = dist(iso_p1,iso_p3);
	    poly = [iso_p1,iso_p2,iso_p3,iso_p4];
	}

	var patterns = make_patterns(poly,tri,poly_radius, side_len);
    }

    if(segs == 3) {
	var r_num = Math.ceil(normal_w/side_len);
	var off_scale = (normal_w/(r_num))/side_len;
	var c_off = Math.sqrt(Math.pow(side_len,2)-Math.pow(side_len/2,2));
	var c_num = Math.floor(normal_w/(c_off));
	var c_off_scale = 1;
    } else if(segs == 6) {
	var r_num = Math.ceil(normal_w/(hex_off*2));
	var off_scale = (normal_w/(r_num*2))/hex_off;
	var c_off = (poly_radius*2)-(side_len/2);
	var c_num = Math.floor(normal_w/(c_off));
	var c_off_scale = 1;
    } else if(segs == 4) {
	var r_num = Math.ceil(normal_w/side_len);
	var total_width = side_len*r_num;
	var total_diag = diag_len*r_num;

	var off_scale = normal_w/total_width;
	var c_num = r_num;

	// console.log({'r_num':r_num, 
	// 	     'side_len':side_len,
	// 	     'diag_len':diag_len, 
	// 	     'off_scale':off_scale, 
	// 	     'total_diag':total_diag});
    }

//    var r_low = 1+ ((Math.sin((phasor2*8)%1.0*Math.PI*2)*(1/32.0))*(Math.abs((Math.abs(Math.pow(phasor2,2)-0.5)*2)-1)));
//    var r_high = 1+ ((Math.cos((phasor2*8)%1.0*Math.PI*2)*(1/32.0))*(Math.abs((Math.abs(Math.pow(phasor2,2)-0.5)*2)-1)) );




    //faces first..

    var pat_count = 0;
    for(var face=0; face<2;face++) {
//	
	for(var c=0;c<c_num;c++) {
	    var shift = c%2;
	    
	    for(var r=0;r<r_num;r++) {
		if(segs == 3) {
		    var center_y = poly_radius+((c_off*c)*off_scale);
		} else if(segs == 6) {
		    var center_y = poly_radius+((c_off*c)*c_off_scale);
		}
		
		if(shift) {
		    if(segs == 3) {
			var center_x = ((side_len*0.5)+(side_len*r))*off_scale;
		    } else if(segs == 6) {
    			var center_x = (hex_off+(hex_off+(hex_off*2*r)))*off_scale;
		    }
		} else {
		    if(segs == 3) {
			var center_x = ((side_len*1)+(side_len*r))*off_scale;
		    } else if(segs == 6) {
			var center_x = (hex_off+(hex_off*2*r))*off_scale;
		    }
		}
		
		if(segs == 4) {
		    var center_x = (r*side_len*off_scale) + (side_len*off_scale)*0.5;
		    var center_y = (c*side_len*off_scale) + (side_len*off_scale)*0.5;
		    var iso_cen = xy2iso(center_x, center_y);
		    center_x = iso_cen[0]+0.5;
//		    center_y = iso_cen[1]-0.5-(pat_shift_phasor*diag_len2*2*off_scale);
		    center_y = iso_cen[1]-0.5-(pat_shift_phasor*diag_len2*6*off_scale);
		}

		if(center_y<0 || center_y > 1) continue;
		if(center_x<0 || center_x > 1) continue;

		var x_flip = r%2?1:-1;
		if(shift == 0) {var y_flip = r%2?-1:1;} else {var y_flip = 1;}
		var bend = r_num/3;
//		var bend = r_num/1;

		lattice_struct["center_x"] = center_x;
		lattice_struct["center_y"] = center_y;
		lattice_struct["side_len"] = side_len;
		lattice_struct["poly_radius"] = poly_radius;
		lattice_struct["off_scale"] = off_scale;
		lattice_struct["c_off_scale"] = c_off_scale;
		lattice_struct["segs"] = segs;

		lattice_struct["x_flip"] = 1;
		lattice_struct["y_flip"] = 1;

//		lattice_struct["x_flip"] = x_flip;
//		lattice_struct["y_flip"] = y_flip;

		lattice_struct["sphere_r"] = 1;

		
		if(face == 0) {
		    
		    lattice_struct["unit"] = patterns[2];


		    if(r%2 && (c<r&&c>(r-bend))) {
			var r_mod = wabble(c/c_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
		    	lattice_struct["y_flip"] = -1;
		    	vertices = vertices.concat(lattice());
		    }

		    if(c%2 && r>(c+(bend-0))) {
			var r_mod = wabble(r/r_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
		    	vertices = vertices.concat(lattice());
		    }

		    if(r%2 && c>(r+(bend-0))) {
			var r_mod = wabble(c/c_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
		    	lattice_struct["y_flip"] = -1;
		    	vertices = vertices.concat(lattice());
		    }

		    
		    if((c%2 && (r<c&&r>(c-bend)))) {
			var r_mod = wabble(r/r_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
    			vertices = vertices.concat(lattice());
		    }

		    
		    

		    if((r%2 && c==r) || (r%2 && r==(c+(bend-0))) || (c%2 && c==(r+(bend-0))) ) {
			if((c%2 && c==(r+(bend-0)))) {
			    var r_mod = wabble(c/c_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
			} else {
			    var r_mod = wabble(r/r_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
			}

		    	lattice_struct["sphere_r"] = r_mod;
		    	lattice_struct["y_flip"] = -1;
		    	vertices = vertices.concat(lattice());
		    }
		    
		} else {

		    lattice_struct["unit"] = patterns[1];
		    if(r%2 && (c<r&&c>(r-bend))) {
			var r_mod = wabble(c/c_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
		    	lattice_struct["y_flip"] = -1;
		    	vertices = vertices.concat(lattice());
		    }

		    if((c%2 && (r<c&&r>(c-bend)))) {
			var r_mod = wabble(r/r_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
			
		    	lattice_struct["sphere_r"] = r_mod;
		    	vertices = vertices.concat(lattice());
		    }

		    

		    if(r%2 && c>(r+(bend-0))) {
			var r_mod = wabble(c/c_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
		    	lattice_struct["y_flip"] = -1;
		    	vertices = vertices.concat(lattice());
		    }

		    if(c%2 && r>(c+(bend-0))) {
			var r_mod = wabble(r/r_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
		    	lattice_struct["sphere_r"] = r_mod;
		    	vertices = vertices.concat(lattice());
		    }

		    

		    if((r%2 && c==r) || (r%2 && r==(c+(bend-0))) || (c%2 && c==(r+(bend-0))) ) {
		    	lattice_struct["unit"] = patterns[3];
			if((c%2 && c==(r+(bend-0)))) {
			    var r_mod = wabble(c/c_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
			} else {
			    var r_mod = wabble(r/r_num, wab_phasor, wab_freq, rscale, 1, wab_amp, wab_trap_phasor);
			}

		    	lattice_struct["sphere_r"] = r_mod;
		    	lattice_struct["y_flip"] = -1;
		    	vertices = vertices.concat(lattice());
		    }

		    
		    // var r_mod = wabble(c/c_num, wab_phasor, wab_freq, wab_exp, 1, wab_amp);
		    // lattice_struct["sphere_r"] = r_mod;
		    // lattice_struct["unit"] = patterns[shift+1];
		    // vertices = vertices.concat(lattice());
		    // if(!shift) {
		    // 	lattice_struct["unit"] = patterns[3];
		    // 	vertices = vertices.concat(lattice());
		    // }
		    
		}
		
	    }
	}
	
    }
	    
    return vertices;

}




