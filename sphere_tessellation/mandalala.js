
var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var SVG_DEBUG = 0;
var WIRE_FRAME = 0;
var MAP_3D = 1;
if (typeof mandalala_width !== 'undefined') {
    // the variable is defined
    canvas.width = mandalala_width;
    canvas.height = mandalala_width;
} else {
    canvas.width = 640;
    canvas.height = 640;

}

var RADIUS = (canvas.width/2)*0.97;

//console.log(mandalala_width);

if(SVG_DEBUG) {
    $("#mandalala_container").css("width", (canvas.width+10)*2);
    $("#mandalala_container").css("height", canvas.height+10);
} else {
    $("#mandalala_container").css("width", canvas.width+10);
    $("#mandalala_container").css("height", canvas.height+10);
}

$("#text_container").css("width", canvas.width);
$("#text_container").css("height", canvas.height);
$('#texted').css("width", canvas.width);
$('#texted').css("height", canvas.height);

$('.controler').css("width", canvas.width);
$('.controler').css("top", canvas.height-200);
$('.controler2').css("width", canvas.width);

var export_format = "PNG";
var seg_w = canvas.width/2;
var seg_h = canvas.width/2;

var CUR_LAYER = 0;
var snap_inc = 0;
//svg
var SVG_HEAD = "<svg xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' width='"+canvas.width+"' height='"+canvas.height+"' style='stroke:#000000;stroke-width:1;fill:none;'>";
SVG_HEAD +=  "<rect x='0' y='0' width='"+canvas.width+"' height='"+canvas.height+"' style='fill:none;stroke:#000000;stroke-width:1;'/>";

var SVG_TAIL = "</svg>";

var bzr = {
    line_width: 1,
    sx: 0.2,
    sy: 0.125,
    c1x: 0.3,
    c1y: 0.1,
    c2x: 0.4,
    c2y: 0.2,
    ex: 0.5,
    ey: 0,
    ox: 0,
    oy: 0,
    segs: 8,
    flip: 1,
    scale: 1,
    brush: {
	on:1,
	c1y:0.46,
	c1x: 0.77,
	c2x:0.91,
	c2y: 1.7,
    }
}

// var rotx = 0;
// var roty = 0;
// var rotz = 0;


var zseg = [];

function sortFunction(a, b) {
    if (a[0] === b[0]) {return 0;} else {return (a[0] < b[0]) ? -1 : 1;}
}

function sortFunction2(a, b) {
    if (a === b) {return 0;} else {return (a < b) ? -1 : 1;}
}

var ver_count = 0;
function hex_2D(vertices) {
    context.save();
    for(var i = 0; i<vertices.length;i++) {
	var dat = vertices[i];
	var what = dat["WHAT"];

	var vert2D = dat["VERTEX_2D"];

	var vert3D = dat["VERTEX_3D"];;
	var canvas_w = canvas.width;
	var col = dat["COL"];
    	context.beginPath();

	if(what === "DOT") {
	    context.arc((canvas_w*vert2D[0][0])*1, 
	  		(canvas_w*vert2D[0][1])*1, 
 	 		1, 0, 2 * Math.PI, false);
	}

	if(what === "LINE") {
	    context.moveTo((canvas_w*vert2D[0][0]), (canvas_w*vert2D[0][1]));
	    context.lineTo((canvas_w*vert2D[1][0]), (canvas_w*vert2D[1][1]));
	}
	if(what === "FACE") {
	    context.moveTo((canvas_w*vert2D[0][0]), (canvas_w*vert2D[0][1]));
	    context.lineTo((canvas_w*vert2D[1][0]), (canvas_w*vert2D[1][1]));
	    context.lineTo((canvas_w*vert2D[2][0]), (canvas_w*vert2D[2][1]));
	}


	context.fill();


	
	
//	var col = 255;
//	context.fillStyle = "rgba("+col+", 0, 0, 255)";
	context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.fill();
	context.lineWidth = 1;
//	context.strokeStyle = "rgba("+col+", 0, 0, 255)";
	context.lineCap="round";
	context.strokeStyle = "rgba("+col+", "+col+", "+col+", 255)";

//	context.closePath();
	context.stroke();
	ver_count++;
    }
    
    context.restore();
}

function hex_3D(vertices) {
    context.save();
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
//    var radius = 2;
//    var radius = canvas.width*0.0025;
    var radius = canvas_w;
    for(var i = 0; i< vertices.length; i++) {
	var dat = vertices[i];
	var what = dat["WHAT"];
	var vert3D = dat["VERTEX_3D"];
	var y_phasor = Math.abs((Math.abs((dat["COORDS"][0][1]/1)-0.5)*2)-1);
	var z_phasor = Math.abs(vert3D[0][2])+0.1;
	var size = dat["SIZE"];
	var col = dat["COL"];

	
    	context.beginPath();

	if(what === "DOT") {
	    context.arc((canvas_w+vert3D[0][0]*RADIUS)*1, 
	  		(canvas_w+vert3D[0][1]*RADIUS)*1, 
 	 		(radius*size)*y_phasor*z_phasor, 0, 2 * Math.PI, false);
	    context.lineWidth = 0.1
	}

	if(what === "LINE") {
	    context.moveTo((canvas_w+vert3D[0][0]*RADIUS), (canvas_w+vert3D[0][1]*RADIUS));
	    context.lineTo((canvas_w+vert3D[1][0]*RADIUS), (canvas_w+vert3D[1][1]*RADIUS));
	    context.lineWidth =(radius*size)*y_phasor*z_phasor;
	}


	if(what === "FACE") {
	    context.moveTo((canvas_w+vert3D[0][0]*RADIUS), (canvas_w+vert3D[0][1]*RADIUS));
	    context.lineTo((canvas_w+vert3D[1][0]*RADIUS), (canvas_w+vert3D[1][1]*RADIUS));
	    context.lineTo((canvas_w+vert3D[2][0]*RADIUS), (canvas_w+vert3D[2][1]*RADIUS));
	    //	    context.lineWidth =(radius*size)*y_phasor*z_phasor;
	    context.lineWidth = 0;
	}

	

	context.fillStyle = "rgba("+col+", "+col+", "+col+", 255)";
	context.fill();

	context.lineCap="round";
	context.strokeStyle = "rgba("+col+", "+col+", "+col+", 255)";
//	context.strokeStyle = 'none';
//	context.closePath();
	context.stroke();
	ver_count++;
    }
    context.restore();
}


function draw(){
    SVG = "";
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    //context.globalAlpha=0.1;
    context.rect(0, 0, canvas.width, canvas.height);
    context.fillStyle = "rgba(255, 255, 255, 255)";
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.stroke();

    ver_count = 0;
    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
    
//    var vertices = tess_calc(rotx, roty, rotz, 0, 0);
    var vertices = tess_calc(rotx, roty, rotz, {});


    if(MAP_3D) {hex_3D(vertices);} else {hex_2D(vertices);}
    console.log([vertices.length,ver_count]);
//    bzr_draw();
}

//UI
$(function() {
    $( "#radio" ).buttonset();
    
    $( "#eq2 > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "brush-c1y" || this.id == "brush-c2y") {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: 0,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	    
	}});
    
    $( "#eq > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "c1y" || this.id == "c2y") {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else if(this.id == "segs") {
	    $(this).empty().slider({
		value: bzr.segs,
		range: "min",
		animate: false,
		min: 1,
		max: 20,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr
		
	    });
	} else if(this.id == "flip") {
	    $(this).empty().slider({
		value: bzr.flip,
		range: "min",
		animate: false,
		min: 1,
		max: 4,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr,

	    });
	} else if(this.id == "scale") {
	    $(this).empty().slider({
		value: bzr.scale,
		range: "min",
		animate: true,
		min: 0,
		max: 5,
		step:0.1,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw,
		change: function (event) {if (event.originalEvent) {svg_draw();}}

	    });
	} else {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -1,
		max: 1,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	}
    });
    $( "#format" ).button();
    $( "#more" ).button();
    $( "#burst" ).button();
    $( "#download" ).button();
})


function downloadInnerHtml(link,filename, elId, mimeType) {
    mimeType = mimeType || 'text/plain';
    link.setAttribute('download', filename);
    link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(SVG_HEAD+SVG+SVG_TAIL));
}

function save() {$('#download')[0].click();}

function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function load_bzr() {
}



function snap() {
    var inc = 1/5;
    for(var i =0; i<5; i++) {
	snap_inc = (i+1);
	draw();
	svg_draw();
	bzr.ex = bzr.ex-inc;
	bzr.sy = bzr.sy+inc;
	bzr.ey = bzr.ey+inc;
	$('#download')[0].click();
    }
    snap_inc = 0;
}

var text_toggle = 0;

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
	if(elem.createTextRange) {
	    var range = elem.createTextRange();
	    range.move('character', caretPos);
	    range.select();
	}
	else {
	    if(elem.selectionStart) {
		elem.focus();
		elem.setSelectionRange(caretPos, caretPos);
	    }
	    else
		elem.focus();
	}
    }
}

$(document).ready(function(){

    if($('#texted').length) {
	
	document.getElementById('texted').onkeydown = function(){

//	    console.log(event.keyCode);
	    
	    if(event.ctrlKey===true && event.keyCode==88){
		var text = $("#texted").val()
		eval(text);
		draw();
		svg_draw();
	    }
	    if(event.ctrlKey===true && event.keyCode==69 && event.shiftKey){

		text_toggle = text_toggle?0:1;
		if(text_toggle) {
	    	    $("#texted").css("color","rgba(255,0,0,0)");
		    $("#texted").attr("readonly","readonly"); 	
		} else {
	    	    $("#texted").css("color","rgba(255,0,0,1)");
		    $("#texted").removeAttr("readonly"); 	
		}
		
	    }

	    if((event.ctrlKey===true && event.keyCode==38) || (event.ctrlKey===true && event.keyCode==40)) {
		var stopCharacters = [' ', '\n', '\r', '\t']
		var text = $('#texted').html();
		var start = $('#texted')[0].selectionStart;
		var end = $('#texted')[0].selectionEnd;
		while (start > 0) {
		    if (stopCharacters.indexOf(text[start]) == -1) {--start;} else {break;}
		};
		++start;
		while (end < text.length) {
		    if (stopCharacters.indexOf(text[end]) == -1) {++end;} else {break;}
		}
		var word = text.substr(start, end - start);

		var fsb = word.search(/\[/);
		var bsb = word.search(/\]/);

		if(fsb >= 0) word = word.substr(fsb+1,word.length);
		if(bsb >= 0) word = word.substr(0, bsb);
		
		var inc = 0.05;
		var next_val = word;
		if(event.keyCode==38) {next_val = parseFloat(word)+0.05;}
		if(event.keyCode==40) {next_val = parseFloat(word)-0.05;}

		if(next_val>0) {next_val = '+'+next_val.toFixed(2);} else {next_val = next_val.toFixed(2);}
		
		var first_half = text.substr(0, start);
		var last_half = text.substr(end, text.length);

		if(fsb >= 0) {
		    $('#texted').html(first_half+'['+next_val+','+last_half);
		} else if(bsb >= 0) {
		    $('#texted').html(first_half+next_val+'],'+last_half);
		} else {
		    $('#texted').html(first_half+next_val+','+last_half);
		}
		
		setCaretPosition("texted", start);

		var new_text = $("#texted").val()
		eval(new_text);
		draw();
		svg_draw();
		
		event.preventDefault();
		event.stopPropagation();
		
	    }
	    
	}
    }
    
    $(".controler").toggle();
    $(".controler2").toggle();
    $("#myCanvas").click(function() {
	$(".controler").toggle();
	if($(".controler2").is(":visible")) {
	    $(".controler2").toggle();
	}else if(bzr.brush.on) {
	    $(".controler2").toggle();
	}
	draw();
	svg_draw();
    });


    $(".l_sel").click(function() {
	var tmp = this.id.split("_");
	var layer = tmp[1];
	//	console.log(layer);
	CUR_LAYER = layer;
	load_bzr();
    });

    
    // $("#format").click(function() {
    // 	var text = $("#format_lab").text();
    // 	if(text == 'PNG') {$("#format_lab").text("SVG");export_format = "SVG";} else {$("#format_lab").text("PNG");export_format = "PNG";}
    // });


    $("#more").click(function() {
	$(".controler2").toggle();
	bzr.brush.on = (bzr.brush.on+1)%2;
	draw();
	svg_draw();

    });

    if(!bzr.brush.on) $(".controler2").toggle();
    

    document.getElementById('download').addEventListener('click', function() {

    	var fname = "bzr_";
    	var ftmp = [];
    	for(var k in bzr) {
    	    if(k !== "brush") {
    		var val = bzr[k];
    		if(k == 'line_width') continue;
    		if(k == 'segs' || k == 'flip' || k == 'scale') {
    		    val = val;
    		} else {
    		    val = parseInt(val*100);
    		}
    		ftmp.push(val);
    	    } else {
    		for(var n in bzr.brush) {
    		    if(n == 'on') continue;
    		    var val = parseInt(bzr.brush[n]*100);
    		    ftmp.push(val);
    		}
    	    }
    	}

//    	fname = fname+ftmp.join('_');
	fname = "mandalala_hex";
	
    	if(snap_inc) fname = snap_inc+'_'+fname;
	    
    	if(export_format == 'SVG') {
    	    downloadInnerHtml(this, fname+'.svg','svg');
    	} else {
//	    console.log(this);
    	    downloadCanvas(this, 'myCanvas', fname+'.png');
    	}

    }, false);


    
    load_bzr();
    
    if(SVG_DEBUG) {
	$( "body" ).append("<div id=\"svg\"></div>");
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
	$(".container").css("float","left");
    } else {
	$(".container").css("float","none");
    }

    $(".mandalala_container").toggle();
    
});

function refreshbzr(event,ui) {
    if (event.originalEvent) {
	var id = this.id;
	var id_split = id.split("-");
	var val = $(this).slider("value");
	if(id_split[0] !== "brush") {
	    // if(id == "sy") {
	    // 	bzrs["ey"][CUR_LAYER] = val;
	    // 	$("#ey_val").text(val);
	    // }
	    bzrs[id][CUR_LAYER] = val;
	} else {
//	    console.log([id_split[0],id_split[1]]);
	    bzrs[id_split[0]][id_split[1]][CUR_LAYER] = val;
	}
	$("#"+id+"_val").text(val);
    }
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}

function refreshbzr_brush(event,ui) {
    $( "#eq2 > span" ).each(function() {
	if(this.id == 'brush-c1x') {bzr.brush.c1x = $(this).slider("value");$("#brush-c1x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c1y') {bzr.brush.c1y = $(this).slider("value");$("#brush-c1y_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2x') {bzr.brush.c2x = $(this).slider("value");$("#brush-c2x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2y') {bzr.brush.c2y = $(this).slider("value");$("#brush-c2y_val").text($(this).slider("value"));}

    });
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}


function svg_draw() {

    if(SVG_DEBUG && $('#svg').length) {
	document.getElementById('svg').innerHTML = "";
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg'));

    }
}

  
draw();
