#!/bin/bash

BASENAME=$(basename $1 ".png")
OUTFNAME=$BASENAME"_CROP.png"
FLOPFNAME=$BASENAME"_CROP_flop.png"
MIRRORFNAME=$BASENAME"_MIRROR.png"

ROTATE=$(echo "scale=2; 360/($2*2)" | bc)

OSIZE=$(identify $1|cut -d" " -f3)
OHEIGHT=$(echo $OSIZE|cut -d"x" -f2)
OWIDTH=$(echo $OSIZE|cut -d"x" -f1)

T_HEIGHT=$(echo "scale=0; $OHEIGHT*0.5" | bc | cut -d"." -f1)
T_WIDTH=$(echo "scale=0; $OWIDTH*0.5" | bc | cut -d"." -f1)

echo $T_HEIGHT" "$T_WIDTH

for i in `seq 1 $2`;
do
    if [ $i -eq 1 ]
    then
	SIZE=$(identify $1|cut -d" " -f3)
	HEIGHT=$(echo $SIZE|cut -d"x" -f2)
	WIDTH=$(echo $SIZE|cut -d"x" -f1)
	CROP_WIDTH=$(($HEIGHT/2))
	CROP_HEIGHT=$HEIGHT
	CROP_X=$((($WIDTH/2)-$CROP_WIDTH))
	CROP_Y=0
	printf "\n" 
	echo $CROP_WIDTH"x"$CROP_HEIGHT"+"$CROP_X"+"$CROP_Y $OUTFNAME
	
	convert +repage $1 -crop $CROP_WIDTH"x"$CROP_HEIGHT"+"$CROP_X"+"$CROP_Y $OUTFNAME
	convert +repage $OUTFNAME -flop $FLOPFNAME
	convert +repage $OUTFNAME $FLOPFNAME +append $MIRRORFNAME
    else

	SIZE=$(identify $MIRRORFNAME|cut -d" " -f3)
	HEIGHT=$(echo $SIZE|cut -d"x" -f2)
	WIDTH=$(echo $SIZE|cut -d"x" -f1)
	CROP_WIDTH=$(($HEIGHT/2))
	CROP_HEIGHT=$HEIGHT
	CROP_X=$((($WIDTH/2)-$CROP_WIDTH))
	CROP_Y=0

	#	echo $i" -> "$CROP_WIDTH"x"$CROP_HEIGHT"+"$CROP_X"+"$CROP_Y $OUTFNAME
	printf "%d" $i
	convert +repage -background white  -virtual-pixel background -resize 100% $MIRRORFNAME -distort SRT -0 +repage -distort SRT -$ROTATE -resize 100% +repage -crop $CROP_WIDTH"x"$CROP_HEIGHT"+"$CROP_X"+"$CROP_Y  $OUTFNAME
	convert +repage $OUTFNAME -flop $FLOPFNAME
	convert +repage $OUTFNAME $FLOPFNAME +append $MIRRORFNAME

    fi

done

#convert $MIRRORFNAME -gravity center -extent  $OSIZE "kaleidoscope/"$MIRRORFNAME
convert $MIRRORFNAME -gravity center -extent  $T_WIDTH"x"$T_HEIGHT "kaleidoscope/"$MIRRORFNAME

#convert kaleidoscope/00000_MIRROR.png -resize 250% -gravity center -extent 1280x720 kaleidoscope/00000_MIRROR.png -transparent white -composite -background white -alpha remove foo.png
