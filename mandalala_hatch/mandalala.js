//BZR stuff
//http://www.13thparallel.org/archive/bezier-curves/
function B1(t) { return t*t*t }
function B2(t) { return 3*t*t*(1-t) }
function B3(t) { return 3*t*(1-t)*(1-t) }
function B4(t) { return (1-t)*(1-t)*(1-t) }


var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var SVG_DEBUG = 0;


if (typeof mandalala_width !== 'undefined') {
    // the variable is defined
    canvas.width = mandalala_width;
    canvas.height = mandalala_width;
} else {
    canvas.width = 640;
    canvas.height = 640;

}

console.log(mandalala_width);

if(SVG_DEBUG) {
    $("#mandalala_container").css("width", (canvas.width+10)*2);
    $("#mandalala_container").css("height", canvas.height+10);
} else {
    $("#mandalala_container").css("width", canvas.width+10);
    $("#mandalala_container").css("height", canvas.height+10);
}

$("#text_container").css("width", canvas.width);
$("#text_container").css("height", canvas.height);
$('#texted').css("width", canvas.width);
$('#texted').css("height", canvas.height);

$('.controler').css("width", canvas.width);
$('.controler').css("top", canvas.height-200);
$('.controler2').css("width", canvas.width);


var export_format = "PNG";
var seg_w = canvas.width/2;
var seg_h = canvas.width/2;
var CUR_LAYER = 0;
var snap_inc = 0;
//svg
var SVG_HEAD = "<svg xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' width='"+canvas.width+"' height='"+canvas.height+"' style='stroke:#000000;stroke-width:1;fill:none;'>";
SVG_HEAD +=  "<rect x='0' y='0' width='"+canvas.width+"' height='"+canvas.height+"' style='fill:none;stroke:#000000;stroke-width:1;'/>";

var SVG_TAIL = "</svg>";

function get_bzr_coords(cw,ch,swap, brush, reflect) {
    var x_off = seg_w*bzr.ox;
    var y_off = seg_h*bzr.oy;
    var start_x = (cw*bzr.sx)+x_off;
    var SY = bzr.sy*reflect;
//    var start_y = (ch*bzr.sy)+y_off;
    var start_y = (ch*SY)+y_off;

    if(!brush) {
	if(!swap) {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-bzr.c1x):Math.abs(swap-bzr.c1x)*-1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-bzr.c2x):Math.abs(swap-bzr.c2x)*-1;
	} else {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-bzr.c1x):swap+Math.abs(bzr.c1x)*1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-bzr.c2x):swap+Math.abs(bzr.c2x)*1;
	}
	var c1_x = (cw*swap_c1x)+x_off;
	var c1_y = (ch*(SY+(bzr.c1y*reflect)))+y_off;
	var c2_x = (cw*swap_c2x)+x_off;
	var c2_y = (ch*(SY+(bzr.c2y*reflect)))+y_off;
    } else {
	if(!swap) {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-(bzr.c1x*bzr.brush.c1x)):Math.abs(swap-(bzr.c1x*bzr.brush.c1x))*-1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-(bzr.c2x*bzr.brush.c2x)):Math.abs(swap-(bzr.c2x*bzr.brush.c2x))*-1;
	} else {
	    var swap_c1x = bzr.c1x>0?Math.abs(swap-(bzr.c1x*bzr.brush.c1x)):swap+Math.abs(bzr.c1x*bzr.brush.c1x)*1;
	    var swap_c2x = bzr.c2x>0?Math.abs(swap-(bzr.c2x*bzr.brush.c2x)):swap+Math.abs(bzr.c2x*bzr.brush.c2x)*1;
	}
	var c1_x = (cw*swap_c1x)+x_off;
	var c1_y = (ch*(SY+((bzr.c1y*bzr.brush.c1y)*reflect)))+y_off;
	var c2_x = (cw*swap_c2x)+x_off;
	var c2_y = (ch*(SY+((bzr.c2y*bzr.brush.c2y)*reflect)))+y_off;

    }

    
    var end_x = (cw*bzr.ex)+x_off;
    var end_y = ((ch*bzr.ey)+y_off);
    return [start_x, start_y, c1_x, c1_y, c2_x, c2_y, end_x, end_y];
}

var bzr = {
    line_width: 1,
    sx: 0.2,
    sy: 0.125,
    c1x: 0.3,
    c1y: 0.1,
    c2x: 0.4,
    c2y: 0.2,
    ex: 0.5,
    ey: 0,
    ox: 0,
    oy: 0,
    segs: 8,
    flip: 1,
    scale: 1,
    brush: {
	on:1,
	c1y:0.46,
	c1x: 0.77,
	c2x:0.91,
	c2y: 1.7,
    }
}

function draw_loop(canvas_w, canvas_h, count) {
    var svg_outline = "";
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		sides[n] = coords;

		context.beginPath();
		
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];


		if(n) {

		    inside_path += 
		    	" c "+ (svg_sx_max-coords[4])*-1+","+(svg_sy_max-coords[5])*-1+
		    	" "+(svg_sx_max-coords[2])*-1+","+(svg_sy_max-coords[3])*-1+
		    	" "+(svg_sx_max-coords[0])*-1+","+(svg_sy_max-coords[1])*-1;
		    inside_path += " z' style='fill:#ffffff;stroke:#000000;stroke-width:0;' id='' />";		    
		} else {
		    inside_path = "<path d='m ";
		    inside_path += coords[0]+","+coords[1]+
		    	" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
		    	" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
		    	" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min);
		}


		if(bzr.brush.on) {
		    context.moveTo(coords[0], coords[1]);
		    context.bezierCurveTo((coords[2])*1, coords[3],coords[4],coords[5],  coords[6], coords[7]);
		
		    if(i%bzr.flip) {
			var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,1,1):get_bzr_coords(canvas_w, canvas_h, 1,1,-1)
			b_coords = [b_coords[0],b_coords[1],b_coords[4],b_coords[5],b_coords[2],b_coords[3],b_coords[6],b_coords[7]];
		    } else {
			var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,1,1):get_bzr_coords(canvas_w, canvas_h, 0,1,-1)
		    }
		    
		    context.moveTo(b_coords[6], b_coords[7]);
		    context.bezierCurveTo(b_coords[4], b_coords[5], b_coords[2], b_coords[3],  b_coords[0], b_coords[1]);
		    
		    // var svg_path =
		    // 	"<path d='m "+coords[0]+","+coords[1]+
		    // 	" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
		    // 	" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
		    // 	" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min)+
		    // 	" c "+(svg_sx_max-b_coords[4])*-1+","+(svg_sy_max-b_coords[5])*-1+
		    // 	" "+(svg_sx_max-b_coords[2])*-1+","+((svg_sy_max-b_coords[3])*-1)+
		    // 	" "+(svg_sx_max-b_coords[0])*-1+","+(svg_sy_max-b_coords[1])*-1+
		    // 	" ' stroke-width="+((1.0/bzr.scale)*bzr.line_width)+"  id='' />";


		    if(n==0) {
			svg_outline +=
			    "<path d='m "+coords[0]+","+coords[1]+
			    " c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
			    " "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
			    " "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min)+
			    " c "+(svg_sx_max-b_coords[4])*-1+","+(svg_sy_max-b_coords[5])*-1+
			    " "+(svg_sx_max-b_coords[2])*-1+","+((svg_sy_max-b_coords[3])*-1)+
			    " "+(svg_sx_max-b_coords[0])*-1+","+(svg_sy_max-b_coords[1])*-1+
			    " ' stroke-width='"+((1.0/bzr.scale)*bzr.line_width)+"'  id='' />";
			
		    }

		    
		    
		} else {
		    // context.moveTo(coords[0], coords[1]);
		    // context.bezierCurveTo((coords[2]), coords[3], coords[4], coords[5], coords[6], coords[7]);

		    // var svg_path =
		    // 	"<path d='m "+coords[0]+","+coords[1]+
		    // 	" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
		    // 	" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
		    // 	" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min)+"' id='' />";
		}
		
//		outside_path += svg_path;
		context.moveTo(coords[6], coords[7]);
		context.closePath();

		context.lineWidth = (1.0/bzr.scale)*bzr.line_width;
		context.lineCap = 'round';
		context.strokeStyle = 'black';
		context.stroke();

	    }

//	SVG += outside_path;


	if(i == 0 ) {
	    var id = "outline_layer_"+count;
	    var id_double = "outline_layer_double_"+count;
	    SVG += "<defs>";

	    SVG += "<g id='"+id+"'>";
	    SVG += svg_outline;
	    SVG += "</g>";
	    
	    SVG += "<g id='"+id_double+"'>";
	    SVG += "<use xlink:href='#"+id+"'/>";
	    SVG += "<use transform=\"scale(1,-1)\" xlink:href='#"+id+"'/>";
	    SVG += "</g>";

	    SVG += "</defs>";
	}
	
	SVG += "<use xlink:href='#outline_layer_double_"+count+"'/>";

	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}

//var bzr_res = 50;
var BZR_RES = 50;
function draw_bzr_loop(canvas_w, canvas_h, count) {
//    console.log("here!!");
    bzr_res = BZR_RES*bzr.scale;

    SVG+="<defs>";
    SVG+="<linearGradient id='grade1' x1='0%' y1='0%' x2='100%' y2='0%'>";
//    SVG+="<linearGradient id='grade1'>";
    SVG+="<stop offset='0%' style='stop-color:rgb(255,255,255);stop-opacity:1' />";
    SVG+="<stop offset='65%' style='stop-color:rgb(0,0,0);stop-opacity:1' />";
    SVG+="<stop offset='90%' style='stop-color:rgb(0,0,0);stop-opacity:1' />";
    SVG+="<stop offset='100%' style='stop-color:rgb(255,255,255);stop-opacity:1' />";
    SVG+="</linearGradient>";
    SVG+="</defs>";

    var svg_contour = "";
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
//	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		if(i%bzr.flip) {
		    var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,1,1):get_bzr_coords(canvas_w, canvas_h, 1,1,-1)
		    b_coords = [b_coords[0],b_coords[1],b_coords[4],b_coords[5],b_coords[2],b_coords[3],b_coords[6],b_coords[7]];
		} else {
		    var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,1,1):get_bzr_coords(canvas_w, canvas_h, 0,1,-1)
		}

		var contour_percent = 0.5;
		var contour_c1x = coords[2]-((coords[2]-b_coords[2])*contour_percent);
		var contour_c1y = coords[3]-((coords[3]-b_coords[3])*contour_percent);
		var contour_c2x = coords[4]-((coords[4]-b_coords[4])*contour_percent);
		var contour_c2y = coords[5]-((coords[5]-b_coords[5])*contour_percent);

		
		sides[n] = coords;

		context.beginPath();
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];

		if(bzr.brush.on) {

		    for(var inc=0; inc<bzr_res; inc++) {

			var t = inc/(bzr_res-1);
			bzr_x = coords[0] * B1(t) + coords[2] * B2(t) + coords[4] * B3(t) + coords[6] * B4(t);
			bzr_y = coords[1] * B1(t) + coords[3] * B2(t) + coords[5] * B3(t) + coords[7] * B4(t);

			b_bzr_x = b_coords[0] * B1(t) + b_coords[2] * B2(t) + b_coords[4] * B3(t) + b_coords[6] * B4(t);
			b_bzr_y = b_coords[1] * B1(t) + b_coords[3] * B2(t) + b_coords[5] * B3(t) + b_coords[7] * B4(t);

//			var off_t = Math.min((t+0.1),1)%1.0;
			var off_t = Math.max(t-0.1, 0);
			c_bzr_x = coords[0] * B1(off_t) + contour_c1x * B2(off_t) + contour_c2x * B3(off_t) + coords[6] * B4(off_t);
			c_bzr_y = coords[1] * B1(off_t) + contour_c1y * B2(off_t) + contour_c2y * B3(off_t) + coords[7] * B4(off_t);

			context.beginPath();
			context.moveTo(bzr_x,bzr_y);
			context.quadraticCurveTo(c_bzr_x,c_bzr_y,b_bzr_x,b_bzr_y);
			context.lineWidth = (1.0/bzr.scale)*bzr.line_width;
			context.lineCap = 'round';
			
			var inv_t = Math.abs(((t+0.25)%1.0)-1);
			var tri = Math.pow(Math.abs((Math.abs(inv_t-0.5)*2)-1),1);
			tri = Math.min(tri*1,1);
			var tri2 = Math.abs(tri-1);
			var tone = parseInt(tri2*255);

			var tone_str = 'rgba('+tone+','+tone+','+tone+',255)';
			
			var grd=context.createLinearGradient(bzr_x,bzr_y,b_bzr_x,b_bzr_y);
			grd.addColorStop(0,'rgba(255,255,255,255)');
			grd.addColorStop(0.1,tone_str);
			grd.addColorStop(0.35,tone_str);
			grd.addColorStop(1,'rgba(255,255,255,255)');
			context.strokeStyle = grd;

			context.stroke();

			
			// SVG +=
		    	//     "<path d='m "+bzr_x+","+bzr_y+
		    	//     " Q "+(c_bzr_x)+
		    	//     ","+(c_bzr_y)+
		    	//     " "+(b_bzr_x)+
		    	//     ","+(b_bzr_y)+
			//     "' id=''  style='stroke:url(#grade1);stroke-width:"+((1.0/bzr.scale)*bzr.line_width)+";fill:none;' stroke-opacity="+(Math.pow(tri,2))+"  />";

			if(n==0) {
			    svg_contour +=
		    		"<path d='m "+bzr_x+","+bzr_y+
		    		" Q "+(c_bzr_x)+
		    		","+(c_bzr_y)+
		    		" "+(b_bzr_x)+
		    		","+(b_bzr_y)+
				"' id=''  style='stroke:url(#grade1);stroke-width:"+((1.0/bzr.scale)*bzr.line_width)+";fill:none;stroke-opacity:"+(Math.pow(tri,2))+";'  />";
//			    "' id=''  style='stroke:url(#grade1);stroke-width:"+((1.0/bzr.scale)*bzr.line_width)+";fill:none;' stroke-opacity="+(Math.pow(tri,2))+"  />"; 
			}
		    }
		    
		}

		
	    }

	if(i == 0 ) {
	    var id = "layer_"+count;
	    SVG += "<defs>";
	    SVG += "<g id='"+id+"'>";
	    SVG += svg_contour;
	    SVG += "</g>";
	    
	    var id_double = "layer_double_"+count;
	    SVG += "<g id='"+id_double+"'>";
	    SVG += "<use xlink:href='#"+id+"'/>";
	    SVG += "<use transform=\"scale(1,-1)\" xlink:href='#"+id+"'/>";
	    SVG += "</g>";
	    
	    SVG += "</defs>";
	}
	
	SVG += "<use xlink:href='#layer_double_"+count+"'/>";
	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}

var CONTOUR = 4;
function draw_contour_loop(canvas_w, canvas_h, count) {

    //    contour = contour*bzr.scale;
    contour = parseInt(CONTOUR*bzr.scale);
//    console.log(contour);
    SVG+="<defs>";
    SVG+="<linearGradient id='gra_contour' x1='0%' y1='0%' x2='100%' y2='0%'>";
    SVG+="<stop offset='50%' style='stop-color:rgb(255,255,255);stop-opacity:1' />";
    SVG+="<stop offset='75%' style='stop-color:rgb(200,200,200);stop-opacity:1' />";
    SVG+="<stop offset='90%' style='stop-color:rgb(255,255,255);stop-opacity:1' />";
    SVG+="</linearGradient>";
    SVG+="</defs>";

    var svg_contour = "";
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		sides[n] = coords;

		context.beginPath();
		
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];

		if(i%bzr.flip) {
		    var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,1,1):get_bzr_coords(canvas_w, canvas_h, 1,1,-1)
		    b_coords = [b_coords[0],b_coords[1],b_coords[4],b_coords[5],b_coords[2],b_coords[3],b_coords[6],b_coords[7]];
		} else {
		    var b_coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,1,1):get_bzr_coords(canvas_w, canvas_h, 0,1,-1)
		}

		var svg_path = "";
		for(cc=0;cc<contour;cc++ ) {
//		    console.log(cc);
		    var contour_percent = cc/(contour-1);
		    if(contour_percent == 0 || contour_percent == 1) continue;

		    context.beginPath();
		    
		    context.moveTo(coords[0], coords[1]);
		    context.bezierCurveTo(
		    	coords[2]-((coords[2]-b_coords[2])*contour_percent), 
		    	coords[3]-((coords[3]-b_coords[3])*contour_percent),
		    	coords[4]-((coords[4]-b_coords[4])*contour_percent),
		    	coords[5]-((coords[5]-b_coords[5])*contour_percent), 
		    	coords[6], coords[7]);

		    var svg_path =
		    	"<path d='m "+coords[0]+","+coords[1]+
		    	" c "+((coords[2]-svg_sx_min)-((coords[2]-b_coords[2])*contour_percent))+
		    	","+((coords[3]-svg_sy_min)-((coords[3]-b_coords[3])*contour_percent))+
		    	" "+((coords[4]-svg_sx_min)-((coords[4]-b_coords[4])*contour_percent))+
		    	","+((coords[5]-svg_sy_min)-((coords[5]-b_coords[5])*contour_percent))+
		    	" "+((coords[6]-svg_sx_min))+
		    	","+((coords[7]-svg_sy_min))+
//		    	"' id=''  style='stroke:#000000;stroke-width:"+(bzr.line_width*(1/bzr.line_width))+";fill:none;'  />";
			"' id=''  style='stroke:url(#gra_contour);stroke-width:"+(bzr.line_width)+";fill:none;'  />";


		    if(n==0) {

			svg_contour +=
		    	    "<path d='m "+coords[0]+","+coords[1]+
		    	    " c "+((coords[2]-svg_sx_min)-((coords[2]-b_coords[2])*contour_percent))+
		    	    ","+((coords[3]-svg_sy_min)-((coords[3]-b_coords[3])*contour_percent))+
		    	    " "+((coords[4]-svg_sx_min)-((coords[4]-b_coords[4])*contour_percent))+
		    	    ","+((coords[5]-svg_sy_min)-((coords[5]-b_coords[5])*contour_percent))+
		    	    " "+((coords[6]-svg_sx_min))+
		    	    ","+((coords[7]-svg_sy_min))+
			    //		    	"' id=''  style='stroke:#000000;stroke-width:"+(bzr.line_width*(1/bzr.line_width))+";fill:none;'  />";
			    "' id=''  style='stroke:url(#gra_contour);stroke-width:"+(bzr.line_width)+";fill:none;'  />";
		    }


		    
//		    SVG += svg_path;
		    context.moveTo(coords[6], coords[7]);
		    context.closePath();

//		    context.lineWidth = bzr.line_width*(1/bzr.line_width);
		    context.lineWidth = bzr.line_width;
		    context.lineCap = 'round';
//		    context.strokeStyle = '#000000';

		    var grd=context.createLinearGradient(coords[0],coords[1],coords[6],coords[7]);
		    grd.addColorStop(0.5,"#ffffff");
		    grd.addColorStop(0.7,"#aaaaaa");
		    grd.addColorStop(0.9,"#ffffff");
		    context.strokeStyle = grd;
		    
		    context.stroke();
		}
			
//		outside_path += svg_path;
		context.moveTo(coords[6], coords[7]);
		context.closePath();

	    }

//	SVG += outside_path;


	if(i == 0 ) {
	    var id = "contour_layer_"+count;
	    SVG += "<defs>";
	    SVG += "<g id='"+id+"'>";
	    SVG += svg_contour;
	    SVG += "</g>";
	    
	    var id_double = "contour_layer_double_"+count;
	    SVG += "<g id='"+id_double+"'>";
	    SVG += "<use xlink:href='#"+id+"'/>";
	    SVG += "<use transform=\"scale(1,-1)\" xlink:href='#"+id+"'/>";
	    SVG += "</g>";
	    
	    SVG += "</defs>";
	}
	
	SVG += "<use xlink:href='#contour_layer_double_"+count+"'/>";

	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}


function draw_mask_loop(canvas_w, canvas_h, count) {
    
    for(i=0; i<bzr.segs; i++) {

 	SVG += "<g transform='translate("+(canvas.width/2)+","+(canvas.height/2)+")'>";
	if(count%2) {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90)+")'>";
	} else {
	    SVG += "<g transform='rotate("+(((i/bzr.segs)*360)-90-((1/bzr.segs)*180)-180)+")'>";
	}
	SVG += "<g transform='scale("+(bzr.scale)+")'>";	
	context.save();
	
	context.translate(canvas.width/2,canvas.height/2);
	if(count%2) {
	    context.rotate((((i/bzr.segs)*360)-90)*Math.PI/180);
	} else {
	    context.rotate((((i/bzr.segs)*360)-90-(((1/bzr.segs)*180)-180))*Math.PI/180);
	}
	context.scale(bzr.scale,bzr.scale);
	var inside_path = "";
	var outside_path = "";

	var sides = [];
	
	    for(n=0;n<2;n++) {

		if(i%bzr.flip) {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 1,0,1):get_bzr_coords(canvas_w, canvas_h, 1,0,-1)
		    coords = [coords[0],coords[1],coords[4],coords[5],coords[2],coords[3],coords[6],coords[7]]
		} else {
		    var coords = n%2? get_bzr_coords(canvas_w, canvas_h, 0,0,1):get_bzr_coords(canvas_w, canvas_h, 0,0,-1)
		}

		sides[n] = coords;

		context.beginPath();
		context.lineWidth = 0;
		context.moveTo(coords[0], coords[1]);
		context.bezierCurveTo((coords[2]), coords[3], coords[4], coords[5], coords[6], coords[7]);
		context.bezierCurveTo((coords[4]), coords[5]*-1, coords[2], coords[3]*-1, coords[0], coords[1]);
		context.lineTo(coords[0],coords[7]);
		context.closePath();

		context.fillStyle = '#ffffff'; 
		context.fill();


		context.beginPath();
		
		
		var svg_sx_min = coords[0];
		var svg_sy_min = coords[1];
		var svg_sx_max = coords[6];
		var svg_sy_max = coords[7];


		if(n) {

		    inside_path += 
		    	" c "+ (svg_sx_max-coords[4])*-1+","+(svg_sy_max-coords[5])*-1+
		    	" "+(svg_sx_max-coords[2])*-1+","+(svg_sy_max-coords[3])*-1+
		    	" "+(svg_sx_max-coords[0])*-1+","+(svg_sy_max-coords[1])*-1;
		    inside_path += " z' style='fill:#ffffff;stroke:#000000;stroke-width:0;' id='' />";		    
		} else {
		    inside_path = "<path d='m ";
		    inside_path += coords[0]+","+coords[1]+
		    	" c "+ (coords[2]-svg_sx_min)+","+(coords[3]-svg_sy_min)+
		    	" "+(coords[4]-svg_sx_min)+","+(coords[5]-svg_sy_min)+
		    	" "+(coords[6]-svg_sx_min)+","+(coords[7]-svg_sy_min);
		}


	    }

	SVG +=inside_path;
	
	context.restore();
	SVG += "</g>";
	SVG += "</g>";
	SVG += "</g>";

    }

}

// var bzrs = {
//     line_width: [1,1,1,1,1],
//     sx: [0.3, 0.1,0.38,0.3,0.15,0.1],
//     sy: [-0.4, 0.26,0.15,0,0.1,0.07],
//     c1x: [0.03, 0.18,0.5,0.58,0.89,0.22],
//     c1y: [-0.03, -1.63,0.05,0.28,-0.74,-0.1],
//     c2x: [0.79, 0.48,0.52,0.54,0.79,0.4],
//     c2y: [1.5, 0.72,-0.29,0.24,1.4,0.53],
//     ex: [1, 0.5,0.6,0.6,0.99,0.5],
//     ey: [0, 0,0,0,0,0],
//     ox: [0.04, 0.01,0,0,0,0],
//     oy: [0, 0,0,0,0,0],
//     segs: [8, 8,16,16,8,8],
//     flip: [1, 1,1,1,1,1],
//     scale: [0.1,1.9,1.25,1.2,0.6,1],
//     brush: {
// 	on:[1,1,1,1,1,1],
// 	c1x: [1.34,1,1.18,0.89,1.03,1.48],
// 	c1y:[-0.01,0.46,0.96,0.6,0.81,1.5],
// 	c2x:[0.48,1,1.09,1,1.09,0.91],
// 	c2y: [0.56,1.01,-0.02,1,0.56,0.52],
//     }
// }

// var bzrs = {
//     line_width: [1],
//     sx: [0.2],
//     sy: [0.125],
//     c1x: [0.3],
//     c1y: [0.1],
//     c2x: [0.4],
//     c2y: [0.2],
//     ex: [0.5],
//     ey: [0],
//     ox: [0],
//     oy: [0],
//     segs: [2],
//     flip: [1],
//     scale: [0.8],
//     brush: {
// 	on:[1],
// 	c1y:[-3],
// 	c1x: [0.77],
// 	c2x:[0.91],
// 	c2y: [1.7],
//     }
// }


function load_bzr_params(idx) {

    bzr.line_width = bzrs.line_width[idx];
//    console.log(bzr.line_width);
    bzr.sx = bzrs.sx[idx];
    bzr.sy = bzrs.sy[idx];

    bzr.c1x = bzrs.c1x[idx];
    bzr.c1y = bzrs.c1y[idx];

    bzr.c2x = bzrs.c2x[idx];
    bzr.c2y = bzrs.c2y[idx];

    bzr.ex = bzrs.ex[idx];
    bzr.ey = bzrs.ey[idx];

    bzr.ox = bzrs.ox[idx];
    bzr.oy = bzrs.oy[idx];

    bzr.segs = bzrs.segs[idx];
    bzr.flip = bzrs.flip[idx];
    bzr.scale = bzrs.scale[idx];

    bzr.brush.on = bzrs.brush.on[idx];
    bzr.brush.c1y = bzrs.brush.c1y[idx];
    bzr.brush.c1x = bzrs.brush.c1x[idx];

    //weird hack to get svg render correctly!?
    if(bzrs.brush.c1x[idx] == 1) {bzr.brush.c1x = 0.98;}
    
    bzr.brush.c2y = bzrs.brush.c2y[idx];
    bzr.brush.c2x = bzrs.brush.c2x[idx];

    if(bzrs.brush.c2x[idx] == 1) {bzr.brush.c2x = 0.98;}

}

function draw(){
    SVG = "";
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
//    context.globalAlpha=0.1;
    context.rect(0, 0, canvas.width, canvas.height);
    context.fillStyle = '#ffffff';
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.stroke();

    var canvas_w = canvas.width/2;
    var canvas_h = canvas.height/4;
//    console.log("draw!!!");
    for(c=0; c<6;c++) {
	load_bzr_params(c);
	draw_mask_loop(canvas_w, canvas_h,c);

	draw_contour_loop(canvas_w, canvas_h,c);
	draw_bzr_loop(canvas_w, canvas_h,c);
	draw_loop(canvas_w, canvas_h,c);
    }

    
}

//UI
$(function() {
    $( "#radio" ).buttonset();
    
    $( "#eq2 > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "brush-c1y" || this.id == "brush-c2y") {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else {
	    var name_tmp = this.id.split('-');
	    $(this).empty().slider({
		value: bzr.brush[name_tmp[1]],
		range: "min",
		animate: true,
		min: 0,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	    
	}});
    
    $( "#eq > span" ).each(function() {
	// read initial values from markup and remove that
	var value = parseFloat( $( this ).text(), 10 );
	var id_arr = this.id.split('_');
	if(id_arr[1] == 'lab' || id_arr[1] == 'val') {
	} else {
	if(this.id == "c1y" || this.id == "c2y") {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -2,
		max: 2,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });
	} else if(this.id == "segs") {
	    $(this).empty().slider({
		value: bzr.segs,
		range: "min",
		animate: false,
		min: 1,
		max: 20,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr
		
	    });
	} else if(this.id == "flip") {
	    $(this).empty().slider({
		value: bzr.flip,
		range: "min",
		animate: false,
		min: 1,
		max: 4,
		step:1,
		orientation: "vertical",
		slide: refreshbzr,
		change: refreshbzr,

	    });
	} else if(this.id == "scale") {
	    $(this).empty().slider({
		value: bzr.scale,
		range: "min",
		animate: true,
		min: 0,
		max: 5,
		step:0.1,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw,
		change: function (event) {if (event.originalEvent) {svg_draw();}}

	    });
	} else {
	    $(this).empty().slider({
		value: value,
		range: "min",
		animate: true,
		min: -1,
		max: 1,
		step:0.01,
		orientation: "vertical",
		slide: refreshbzr,
//		change: svg_draw
		change: function (event) {if (event.originalEvent) {svg_draw();}}
	    });

	}
	}
    });
    $( "#format" ).button();
    $( "#more" ).button();
    $( "#burst" ).button();
    $( "#download" ).button();
})


function downloadInnerHtml(link,filename, elId, mimeType) {
    mimeType = mimeType || 'text/plain';
    link.setAttribute('download', filename);
    link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(SVG_HEAD+SVG+SVG_TAIL));
}

function save() {$('#download')[0].click();}

function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function load_bzr() {
    for(var k in bzr) {
	if(k !== "brush") {
	    var val = bzrs[k][CUR_LAYER];
	    $('#'+k).slider('value',val);
	    $('#'+k+'_val').text(val);
//	    console.log(bzr.flip);
	} else {
	    for(var n in bzr.brush) {
		if(n == 'on') continue;
		var val = bzrs.brush[n][CUR_LAYER];
		$('#'+k+"-"+n).slider('value',val);
		$('#'+k+"-"+n+'_val').text(val);
	    }

	}
    }
    
}



function snap() {
    var inc = 1/5;
    for(var i =0; i<5; i++) {
	snap_inc = (i+1);
	draw();
	svg_draw();
	bzr.ex = bzr.ex-inc;
	bzr.sy = bzr.sy+inc;
	bzr.ey = bzr.ey+inc;
	$('#download')[0].click();
    }
    snap_inc = 0;
}

function get_rand_val(scale, off) {
    var val = ((parseInt(Math.random()*1000)/1000)*scale)+off;
    return parseInt((val*100))/100;
}

function random_gen() {
    bzr.sx = get_rand_val(2, -1);
    bzr.ex = get_rand_val(2, -1);
    bzr.c1x = get_rand_val(2, -1);
    bzr.c1y = get_rand_val(4, -2);
    bzr.c2x = get_rand_val(2, -1);
    bzr.c2y = get_rand_val(4, -2);
    bzr.segs = parseInt(get_rand_val(8, 0)+2)*2;
    bzr.flip = parseInt(get_rand_val(2, 0)+1);

    if(bzr.brush.on) {
	bzr.brush.c1x = get_rand_val(1, 0.5);
	bzr.brush.c1y = get_rand_val(1, 0.5);
	bzr.brush.c2x = get_rand_val(1, 0.5);
	bzr.brush.c2y = get_rand_val(1, 0.5);
    }
    load_bzr();
}


var isRunning = false;
var RUN = "";

function execute(x) {
    setTimeout("eval(" + x + ")", 0);
    // Or: $.globalEval(x);
}

var text_toggle = 0;

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
	if(elem.createTextRange) {
	    var range = elem.createTextRange();
	    range.move('character', caretPos);
	    range.select();
	}
	else {
	    if(elem.selectionStart) {
		elem.focus();
		elem.setSelectionRange(caretPos, caretPos);
	    }
	    else
		elem.focus();
	}
    }
}

$(document).ready(function(){

    if($('#texted').length) {
	
	document.getElementById('texted').onkeydown = function(){

//	    console.log(event.keyCode);
	    
	    if(event.ctrlKey===true && event.keyCode==88){
		var text = $("#texted").val()
		eval(text);
		draw();
		svg_draw();
	    }
	    if(event.ctrlKey===true && event.keyCode==69 && event.shiftKey){
		text_toggle = text_toggle?0:1;
		if(text_toggle) {
	    	    $("#texted").css("color","rgba(255,0,0,0)");
		    $("#texted").attr("readonly","readonly"); 	
		} else {
	    	    $("#texted").css("color","rgba(255,0,0,1)");
		    $("#texted").removeAttr("readonly"); 	
		}
		
	    }

	    if((event.ctrlKey===true && event.keyCode==38) || (event.ctrlKey===true && event.keyCode==40)) {
		var stopCharacters = [' ', '\n', '\r', '\t']
		var text = $('#texted').html();
		var start = $('#texted')[0].selectionStart;
		var end = $('#texted')[0].selectionEnd;
		while (start > 0) {
		    if (stopCharacters.indexOf(text[start]) == -1) {--start;} else {break;}
		};
		++start;
		while (end < text.length) {
		    if (stopCharacters.indexOf(text[end]) == -1) {++end;} else {break;}
		}
		var word = text.substr(start, end - start);

		var fsb = word.search(/\[/);
		var bsb = word.search(/\]/);

		if(fsb >= 0) word = word.substr(fsb+1,word.length);
		if(bsb >= 0) word = word.substr(0, bsb);
		
		var inc = 0.05;
		var next_val = word;
		if(event.keyCode==38) {next_val = parseFloat(word)+0.05;}
		if(event.keyCode==40) {next_val = parseFloat(word)-0.05;}

		if(next_val>0) {next_val = '+'+next_val.toFixed(2);} else {next_val = next_val.toFixed(2);}
		
		var first_half = text.substr(0, start);
		var last_half = text.substr(end, text.length);

		if(fsb >= 0) {
		    $('#texted').html(first_half+'['+next_val+','+last_half);
		} else if(bsb >= 0) {
		    $('#texted').html(first_half+next_val+'],'+last_half);
		} else {
		    $('#texted').html(first_half+next_val+','+last_half);
		}
		
		setCaretPosition("texted", start);

		var new_text = $("#texted").val()
		 eval(new_text);
		 draw();
		 svg_draw();
		
		event.preventDefault();
		event.stopPropagation();
		
	    }
	    
	}
    }
    
    $(".controler").toggle();
    $(".controler2").toggle();
    $("#myCanvas").click(function() {
	$(".controler").toggle();
	if($(".controler2").is(":visible")) {
	    $(".controler2").toggle();
	}else if(bzr.brush.on) {
	    $(".controler2").toggle();
	}
	draw();
	svg_draw();
    });


    $(".l_sel").click(function() {
	var tmp = this.id.split("_");
	var layer = tmp[1];
	//	console.log(layer);
	CUR_LAYER = layer;
	load_bzr();
//	console.log(CUR_LAYER);
    });

    
    $("#format").click(function() {
	var text = $("#format_lab").text();
	if(text == 'PNG') {$("#format_lab").text("SVG");export_format = "SVG";} else {$("#format_lab").text("PNG");export_format = "PNG";}
    });


    $("#burst").click(function() {
	if(!isRunning) {
	    RUN = setInterval(function () {isRunning = true; random_gen()}, 2000);
	    $("#burst").text("---");
	} else {
	    clearInterval(RUN); isRunning = false;
	    $("#burst").text(">>>");
	}
    });

    $("#more").click(function() {
	$(".controler2").toggle();
	bzr.brush.on = (bzr.brush.on+1)%2;
	draw();
	svg_draw();

    });

    if(!bzr.brush.on) $(".controler2").toggle();
    

    document.getElementById('download').addEventListener('click', function() {

    	var fname = "bzr_";
    	var ftmp = [];
    	for(var k in bzr) {
    	    if(k !== "brush") {
    		var val = bzr[k];
    		if(k == 'line_width') continue;
    		if(k == 'segs' || k == 'flip' || k == 'scale') {
    		    val = val;
    		} else {
    		    val = parseInt(val*100);
    		}
    		ftmp.push(val);
    	    } else {
    		for(var n in bzr.brush) {
    		    if(n == 'on') continue;
    		    var val = parseInt(bzr.brush[n]*100);
    		    ftmp.push(val);
    		}
    	    }
    	}

//    	fname = fname+ftmp.join('_');
	fname = "mandalala_plus";
	
    	if(snap_inc) fname = snap_inc+'_'+fname;
	    
    	if(export_format == 'SVG') {
    	    downloadInnerHtml(this, fname+'.svg','svg');
    	} else {
    	    downloadCanvas(this, 'myCanvas', fname+'.png');
    	}

    }, false);


    
    load_bzr();
    
    if(SVG_DEBUG) {
	$( "body" ).append("<div id=\"svg\"></div>");
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg')); 
	$(".container").css("float","left");
    } else {
	$(".container").css("float","none");
    }

    $(".mandalala_container").toggle();
    
});



function refreshbzr(event,ui) {
    if (event.originalEvent) {
	var id = this.id;
	var id_split = id.split("-");
	var val = $(this).slider("value");
	if(id_split[0] !== "brush") {
	    // if(id == "sy") {
	    // 	bzrs["ey"][CUR_LAYER] = val;
	    // 	$("#ey_val").text(val);
	    // }
	    bzrs[id][CUR_LAYER] = val;
	} else {
//	    console.log([id_split[0],id_split[1]]);
	    bzrs[id_split[0]][id_split[1]][CUR_LAYER] = val;
	}
	$("#"+id+"_val").text(val);
    }
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}

function refreshbzr_brush(event,ui) {
    $( "#eq2 > span" ).each(function() {
	if(this.id == 'brush-c1x') {bzr.brush.c1x = $(this).slider("value");$("#brush-c1x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c1y') {bzr.brush.c1y = $(this).slider("value");$("#brush-c1y_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2x') {bzr.brush.c2x = $(this).slider("value");$("#brush-c2x_val").text($(this).slider("value"));}
	if(this.id == 'brush-c2y') {bzr.brush.c2y = $(this).slider("value");$("#brush-c2y_val").text($(this).slider("value"));}

    });
    draw();
    if(this.id == 'flip' || this.id == 'segs') {svg_draw();}
}


function svg_draw() {

    if(SVG_DEBUG && $('#svg').length) {
	document.getElementById('svg').innerHTML = "";
	document.getElementById("svg").innerHTML = SVG_HEAD+SVG+SVG_TAIL;
	document.getElementById("mandalala_container").appendChild(document.getElementById('svg'));

    }
}

  
draw();


